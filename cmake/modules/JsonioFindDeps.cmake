
if(${CMAKE_CXX_COMPILER_ID} STREQUAL MSVC)
find_library(CURL_LIB libcurl)
else()
find_library(CURL_LIB curl)
endif()
if(NOT CURL_LIB)
  message(FATAL_ERROR "curl library not found")
endif()

#find_package(velocypack REQUIRED)
find_library(VELOCYPACK_LIB velocypack)
if(NOT VELOCYPACK_LIB)
  message(FATAL_ERROR "velocypack library not found")
endif()

