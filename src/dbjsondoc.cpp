//  This is JSONIO library+API (https://bitbucket.org/gems4/jsonio)
//
/// \file dbjsondoc.cpp
/// Implementation of class TDBJsonDocument - working with database collections records
/// Used  JsonDomFree class - API for direct code access to internal JSON data.
//
// JSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2018 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include <iostream>
#include "jsonio/dbjsondoc.h"

using namespace std;

namespace jsonio {



TDBJsonDocument* TDBJsonDocument::newJsonDocument( const TDataBase* dbconnect,
          const string& collcName, const vector<std::string>& keyTemplateSelect   )
{
    if( collcName.empty() )
        return nullptr;

    TDBJsonDocument* newDoc =  new TDBJsonDocument( dbconnect, collcName, keyTemplateSelect );
    return newDoc;
}

TDBJsonDocument* TDBJsonDocument::newJsonDocumentQuery( const TDataBase* dbconnect, const std::string& collcName,
          const std::vector<std::string>& keyTemplateSelect, const DBQueryData& query  )
{
    if( collcName.empty()  )
        return nullptr;

    TDBJsonDocument* newDoc =  new TDBJsonDocument( dbconnect, collcName, keyTemplateSelect );

    // init internal selection block
    newDoc->SetQuery(query);
    return newDoc;
}

void TDBJsonDocument::setData()
{
    _currentData.reset( JsonDomFree::newObject() );
}

//  Constructor
TDBJsonDocument::TDBJsonDocument( TDBCollection* collection, const vector<std::string>& keyTemplateSelect  ):
    TDBDocumentBase( collection ),
    _keyTemplateFields(keyTemplateSelect), _currentData(nullptr)
{
    setData();
}

TDBJsonDocument::TDBJsonDocument( const TDataBase* dbconnect,
                                  const string& coltype, const string& colname,
                                  const vector<string>& keyTemplateSelect ):
    TDBDocumentBase( dbconnect, coltype, colname ),
    _keyTemplateFields(keyTemplateSelect), _currentData(nullptr)
{
    setData();
}


/// Constructor from dom data
TDBJsonDocument::TDBJsonDocument( TDBCollection* collection, const JsonDom *object ):
    TDBDocumentBase( collection, object ), _currentData(nullptr)
{
    // set query data
    setData();
}


JsonDomFree* TDBJsonDocument::recToSave(  time_t /*crtt*/, char* pars )
{
    // test oid
    JsonDomFree* oidNode = _currentData.get()->field("_id");
    string oidval;
    if(oidNode!=nullptr)
        oidNode->getValue( oidval );

    if( oidNode==nullptr || oidval.empty() ) // no "_id" into schema definition
    {
        string _keyval;
        if( !getValue( "_key", _keyval ) || _keyval.empty() ) // generate if no exist
            _keyval = makeTemplateKey(_currentData.get(), _keyTemplateFields);

        if( pars )
            _currentData->setOid_( string(pars) );
        else
            _currentData->setOid_( genOid(_keyval) ); // generate id from key
    }
    else
    {
        setOid( oidval ); // set _key from _id (must be the same value)
        if( pars )
        {
            if( oidval != string(pars) )
                jsonioErr("TGRDB0003", "Changed record oid " + string(pars) );
        }
    }
    return _currentData.get();
}


// Load data from bson structure (return read record key)
string TDBJsonDocument::recFromJson( const string& jsondata )
{
    SetJson( jsondata );
    // Get key of record
    return getKeyFromCurrent( );
}

// Return curent record in json format string
string TDBJsonDocument::GetJson(bool dense ) const
{
    string jsonstr;
    printNodeToJson( jsonstr, _currentData.get(), dense );
    return  jsonstr;
}

// Set json format string to curent record
void TDBJsonDocument::SetJson( const string& sjson )
{
    _currentData.get()->clearField();
    parseJsonToNode( sjson, _currentData.get() );
}

// Get current record key from internal data
string TDBJsonDocument::getKeyFromCurrent() const
{
    string keyStr;
    if( !getValue( "_id", keyStr  ) )
        keyStr = "";

    strip( keyStr );
    return keyStr;
}

FieldSetMap TDBJsonDocument::extractFields( const vector<string> queryFields, const JsonDom* domobj ) const
{
    string valbuf;
    FieldSetMap res;
    for( auto& ent: queryFields )
    {
        if( domobj->findValue( ent, valbuf ) )
            res[ ent] = valbuf;
        else
            res[ ent] = "";
    }
    return res;
}


FieldSetMap TDBJsonDocument::extractFields( const vector<string> queryFields, const string& jsondata ) const
{
    shared_ptr<JsonDomFree> domnode(JsonDomFree::newObject());
    parseJsonToNode( jsondata, domnode.get());
    FieldSetMap res = extractFields( queryFields, domnode.get() );
    return res;
}

// Working with collections
void TDBJsonDocument::updateQuery()
{
    if( _queryResult.get() == nullptr )
        return;

    shared_ptr<JsonDomFree> nodedata(JsonDomFree::newObject());
    _queryResult->clearResults();

    SetReadedFunction setfnc = [&]( const string& jsondata )
    {
        parseJsonToNode( jsondata, nodedata.get() );
        string keyStr = getKeyFromDom( nodedata.get() );
        _queryResult->addLine( keyStr,  nodedata.get(), false );
    };

    _collection->selectQuery( _queryResult->getQuery().getQueryCondition(), setfnc );
    // Create a thread using member function
    //std::thread th(&TDBCollection::selectQuery, _collection, _queryResult->getQuery().getQueryCondition(), setfnc );
    //th.detach();
}

} // namespace jsonio
