#include <ctime>
#include "jsonio/io_settings.h"
#include "jsonio/tf_json.h"
#include "jsonio/jsondomschema.h"
#include "jsonio/jsondomfree.h"
using namespace std;

namespace jsonio {

ThriftFieldDef JsonDomSchema::topfield = { -1, "", {Th_STRUCT}, 0, "", "", "", "", DOUBLE_EMPTY, DOUBLE_EMPTY };
static shared_ptr<JsonDomFree> interalread;

shared_ptr<JsonDomSchema> unpackJson( const string& jsondata, const string& schemaName  )
{
    shared_ptr<JsonDomSchema> domdata(JsonDomSchema::newSchemaObject( schemaName ));
    parseJsonToNode( jsondata, domdata.get());
    return domdata;
}

// Create new Node for struct name
JsonDomSchema* JsonDomSchema::newSchemaObject( const string& className )
{
  ThriftStructDef* strDef = ioSettings().Schema()->getStruct( className );
  if( strDef == nullptr )
    return nullptr;
  else
   {
     topfield.className = className;
     JsonDomSchema *newStruct = new JsonDomSchema( strDef );
     return newStruct;
  }
}


// Constructor for empty struct (up level )
JsonDomSchema::JsonDomSchema( ThriftStructDef* aStrDef ):
  _strDef(aStrDef),  _fldDef( &topfield ), _isSetUp( false ),
  _fldKey(aStrDef->getName()), _parent(nullptr),  _level(0),  _ndx(0)
{
    _fldValue = "";
    _isSetUp = true;
    // add levels for objects and arrays
    addChildren( "" );
}


// Constructor for empty field
JsonDomSchema::JsonDomSchema( ThriftFieldDef* afldDef, JsonDomSchema* aparent ):
  _strDef(aparent->_strDef),  _fldDef( afldDef), _isSetUp(afldDef->fRequired == fld_required ),
  _fldKey(afldDef->fName), _parent(aparent),  _level(0)
{
    if(_parent)
    {
       _ndx = static_cast<int>(_parent->_children.size());
       _parent->_children.push_back(shared_ptr<JsonDomSchema>(this));
    }
    // set default value
    setDefault("", 0 ); // switch off
    if( _fldDef->className == "TimeStamp" )
          setCurrentTime();
}


/// Constructor for array fields
JsonDomSchema::JsonDomSchema( const string& key, int /*atype*/, const string& value, JsonDomSchema* aparent ):
  _strDef( aparent->_strDef),  _fldDef( aparent->_fldDef),
  _isSetUp( aparent->_isSetUp ), _parent(aparent),  _level(0)
{
    if(_parent)
    {
       _ndx = static_cast<int>(_parent->_children.size());
       _parent->_children.push_back(shared_ptr<JsonDomSchema>(this));
       switch(_parent->getFieldType())
       { case Th_MAP:
             _level = _parent->_level+2;
            break;
         case Th_SET:
         case Th_LIST: // !! next level
             _level = _parent->_level+1;
       }
    }
    //if( atype != getType() )
    //  ; // error?
    _fldKey = key;
    updateField( value );
}

JsonDom* JsonDomSchema::appendNode( const string& akey, int atype, const string& avalue )
{
  if( isStruct() )
  {
      auto child = field( akey );
      if( child == nullptr )
      {
        if( atype==JSON_OBJECT )
          interalread.reset( JsonDomFree::newObject() );
        if( atype==JSON_ARRAY )
          interalread.reset( JsonDomFree::newObject() );
        return dynamic_cast<JsonDom*>(interalread.get());
      }
      // if( atype != child->getType() )
      //   ; // error?
      if( ( child->isMap() || child->isArray() ) && avalue.empty()  )
          child->_children.clear(); // clear default
      else
          child->updateField( avalue );
      return child;
  }
   else if( isArray() )
     {
      JsonDomSchema* newNode = new  JsonDomSchema( akey, atype, avalue, this );
      return newNode;
     }
    else  // not childrens type
       jsonioErr( "JsonDomSchema002", "not childrens base type" );
    //return nullptr;
}

void JsonDomSchema::setDefault( const string& newdef, int deflevel )
{
    string def = newdef;
    if( !_level && def.empty() )  // define only for fields
       def = _fldDef->fDefault;

    _fldValue = getDefValue( _fldDef->fTypeId[_level], def );

    switch( deflevel )
    {
      case 0: _isSetUp = _fldDef->fRequired == fld_required;
              break;
      case 1: _isSetUp = _fldDef->fRequired <= fld_default;
              break;
      default: _isSetUp = (_parent != nullptr ? _parent->_isSetUp :true );
    }
    // add levels for objects and arrays
    addChildren( def );
}

void JsonDomSchema::addChildren( const string& jsondata )
{
    // add levels for objects and arrays
    switch ( getFieldType() )
    {
       // main constructions
       case Th_STRUCT:
             {
               // structure
                ThriftStructDef* strDef2;
                  if( isTop() )
                     strDef2  = _strDef;
                   else
                     strDef2  = ioSettings().Schema()->getStruct( _fldDef->className );
                if( strDef2 == nullptr )
                     jsonioErr( _fldDef->className , "Undefined struct definition" );
                struct2model( strDef2, jsondata );
             }
             break;
      case Th_MAP:
      case Th_SET:
      case Th_LIST: // !! next level
             list2model( jsondata);
           break;
       default:     break;
    }
}


void JsonDomSchema::struct2model( ThriftStructDef* strDef, const string& jsondata )
{
    _strDef = strDef;
    _children.clear();

    // init empty/default structure
    auto it = strDef->fields.begin();
    while( it != strDef->fields.end() )
    {
       new JsonDomSchema( &(*it), this );
       it++;
    }
    // read data from json
    if( !jsondata.empty() )
      parseJsonToNode( jsondata, this );
}


// Define Array ( LIST, SET, MAP )
void JsonDomSchema::list2model( const string& jsondata )
{
    // init empty array
    _children.clear();
    // read data from json
    if( !jsondata.empty() )
       parseJsonToNode( jsondata, this );
}


// Get field by fieldpath ("name1.name2.name3")
JsonDomSchema *JsonDomSchema::field( const string& fieldpath) const
{
  auto pos = fieldpath.find_first_not_of("0123456789.");
  if( pos != string::npos || isArray() ) // names
  {
      queue<string> names = split(fieldpath, ".");
      return field(names);
  } else   // ids
    {
        queue<int> ids = string2intarray( fieldpath, ".");
        return field(ids);
    }
}

// Get field by fieldpath
JsonDomSchema *JsonDomSchema::field( queue<string> names ) const
{
  if( names.empty() )
    return const_cast<JsonDomSchema *>(this);

  string fname = names.front();
  names.pop();

  for(uint ii=0; ii< _children.size(); ii++ )
   if( _children[ii]->getKey() == fname )
       return _children[ii]->field( names );

  return nullptr;  // not found
}


// Get field by idspath
JsonDomSchema *JsonDomSchema::field( queue<int> ids ) const
{
    if( ids.empty() )
      return const_cast<JsonDomSchema *>(this);
    int id_ = ids.front();
    ids.pop();

    for(uint ii=0; ii< _children.size(); ii++ )
     if( _children[ii]->_fldDef->fId == id_ )
         return _children[ii]->field( ids );

    return nullptr;  // not found
}

string JsonDomSchema::getFullDescription() const
{
    // get doc from thrift schema
    if( _level == 0 )
      return  _fldDef->fDoc;

    string desc = _parent->getDescription();
    desc += " ";
    desc += _fldKey;
    return desc;
}

// if top level map with class - new edit table
ThriftEnumDef* JsonDomSchema::getMapEnumdef() const
{
   if( !isTop() && _parent->isMap() )
   {
     std::string enumName = _parent->getEnumMapName();
     if( !enumName.empty() )
         return ioSettings().Schema()->getEnum( enumName );
   }
   return nullptr;
}

// Set up current time as default value
// only for fild
void JsonDomSchema::setCurrentTime()
{
    if(_fldDef->className != "TimeStamp")
     return;

    struct tm *time_now;
    time_t secs_now;

#ifndef _MSC_VER
    tzset();
#endif
    time(&secs_now);
    time_now = localtime(&secs_now);

    JsonDomSchema *fld = field(  "year" );
    fld->setValue(1900+time_now->tm_year);
    fld = field(  "month" );
    fld->setValue(time_now->tm_mon+1);
    fld = field(  "day" );
    fld->setValue(time_now->tm_mday);
    fld = field(  "hour" );
    fld->setValue(time_now->tm_hour);
    fld = field(  "minute" );
    fld->setValue(time_now->tm_min);
    fld = field(  "second" );
    fld->setValue(time_now->tm_sec);
}

//-----------------------------Arrays

// get table sizes
vector<size_t> JsonDomSchema::getArraySizes()
{
   if( !isArray() )
        jsonioErr("getArraySize" , _fldKey+" is not a array" );

   vector<size_t>  sizes;
   JsonDomSchema *itline = this;
   auto it = _fldDef->fTypeId.begin();
   while(  (*it == Th_SET || *it == Th_LIST || *it == Th_MAP ) &&
            it< _fldDef->fTypeId.end() )
   {
      if( itline != nullptr )
      {
         auto sizef = itline->_children.size();
         sizes.push_back(sizef);

         if(sizef>0)
           itline = itline->_children[0].get();
         else
           itline = nullptr;
      }
      else
         sizes.push_back(0);
      it++;
   }
   return sizes;
}

/// Resize top level Array
void JsonDomSchema::resizeArray( const vector<std::size_t>& sizes, const string& defval )
{
  if( !isArray() || _level != 0 )
     jsonioErr("resizeArray" , _fldKey+" is not a top level array" );
  addField();
  resizeArray( 0, sizes, defval  );
}

/// Reset top level Array
void JsonDomSchema::resetArray( const vector<string> vals )
{
  if( !isArray() || _level != 0 )
   jsonioErr("resetArray" , _fldKey+" is not a top level array" );

  if( vals.size()<1 &&  _children.size()<1 )
     return;  // no add empty array

  vector<std::size_t> sizes;
  sizes.push_back(vals.size());
  resizeArray( sizes );
  for(uint ii=0; ii< _children.size(); ii++ )
    _children[ii]->setDefault(vals[ii], 2 );
}


void JsonDomSchema::resizeArray( std::size_t  size, const string& defval  )
{
    if( !isArray() )
        jsonioErr("resizeArray" , _fldKey+" is not a array" );

    // resizeline item
    if( size == _children.size() )
       ;
    else // delete if smaler
      if( size < _children.size() )
       _children.resize(size);
      else
      {
          auto level = _level;
          if( _fldDef->fTypeId[_level] == Th_MAP )
            level++;
          level++;

         // add new elements
         string value = defval;
          if( value.empty() && _children.size()>0)
              value = _children[0]->toString();
          for( auto ii=_children.size(); ii<size; ii++)
             appendNode( to_string(ii),  jsonTypeFromThrift( _fldDef->fTypeId[level] ), value );
      }
}


void JsonDomSchema::resizeArray(uint level, const vector<std::size_t>& sizes, const string& defval  )
{
    if( sizes.size() <= level )
      return;
    auto size = sizes[level];

    // resize current level
    resizeArray( size, defval  );

    // try to resize next level
    for( uint ii=0; ii< _children.size(); ii++)
      _children[ii]->resizeArray( level+1, sizes, defval );
}


// list of not defined fields
vector<string> JsonDomSchema::getNoUsedKeys() const
{
    vector<string> nokeys;
    for(uint ii=0; ii< _children.size(); ii++ )
     if( !_children[ii]->_isSetUp )
         nokeys.push_back(_children[ii]->getKey());
    return nokeys;
}

// list of defined fields
vector<string> JsonDomSchema::getUsedKeys() const
{
    vector<string> usekeys;
    for(uint ii=0; ii< _children.size(); ii++ )
     if( _children[ii]->_isSetUp )
         usekeys.push_back(_children[ii]->getKey());
    return usekeys;
}

//------------------------------------------------------------

int jsonTypeFromThrift( int thriftType )
{
    int btype = JSON_UNDEFINED;
    switch( thriftType )
    {
      case Th_BOOL:  btype = JSON_BOOL;
                    break;
      //case T_BYTE:
      case Th_I08:
      case Th_I16:
      case Th_I32:    btype = JSON_INT;
                     break;
      case Th_U64:
      case Th_I64:
      case Th_DOUBLE:  btype = JSON_DOUBLE;
                      break;
      case Th_STRING:
      //case T_UTF7:
      case Th_UTF8:   btype = JSON_STRING;
                     break;
      case Th_MAP:
      case Th_STRUCT:  btype = JSON_OBJECT;
                      break;
      case Th_SET:
      case Th_LIST:  btype = JSON_ARRAY;
                   break;
      default:
      case Th_UTF16:
      case Th_STOP:
      case Th_VOID: // may be error
                  btype =  JSON_UNDEFINED;
    }
    return btype;
}

string getDefValue( int thriftType, const string& defval )
{
    string value = defval;
    switch( thriftType )
    {
      case Th_BOOL:
                    if( value != "true" )
                       value = "false";
                    break;
      //case T_BYTE:
      case Th_I08:
      case Th_I16:
      case Th_I32:
      case Th_I64:
                   if( value.empty() )
                     value = "0";
                    else
                    { int ival;
                      if( is<int>( ival, value.c_str()) )
                           value = TArray2Base::value2string( ival );
                      else
                          value = "0";
                    }
                    break;
       case Th_U64:
                  if( value.empty() )
                    value = "0";
                  //value = to_string( stoul( value ));
                  break;
      case Th_DOUBLE:
                  if( value.empty() || value == "null" )
                        value = "0.0";
                  else
                  { double dval;
                    if( is<double>( dval, value.c_str()) )
                        value = TArray2Base::value2string( dval );
                    else
                       value = "0.0";
                  }
                  break;
      case Th_STRING:
      //case T_UTF7:
      case Th_UTF8:   if( value.empty() )
                       value = "";//emptiness;
                     break;
      case Th_MAP:
      case Th_STRUCT: value = "";
                      break;
      case Th_SET:
      case Th_LIST:   value = "";
                   break;
      default:
      case Th_UTF16:
      case Th_STOP:
      case Th_VOID: // may be error
                  value = "";
                  break;
    }
    return value;
}



} // namespace jsonio
