//  This is JSONIO library+API (https://bitbucket.org/gems4/jsonio)
//
/// \file gdatastream.h
/// Declarations of GemDataStream class - binary data stream class
/// (ByteOrder : BigEndian or LittleEndian )
//
// JSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef _gemdatastream_h_
#define _gemdatastream_h_

#include <fstream>
#include <string>
#include <vector>

namespace jsonio {

/// Binary data stream class (ByteOrder : BigEndian or LittleEndian )
class GemDataStream
{

    std::ios::openmode mod;
    std::string Path;

    int swap;
    int	byteorder;
    std::fstream ff;

public:

    enum ByteOrder { BigEndian, LittleEndian };

    GemDataStream( )
    {  setByteOrder(LittleEndian); }
    GemDataStream( std::string aPath, std::ios::openmode aMod  );
    ~GemDataStream()
    { }

    const std::string& GetPath() const
    {  return Path;   }
    int	 byteOrder() const
    { return byteorder; }
    void setByteOrder( int );

    std::filebuf* rdbuf() { return ff.rdbuf(); }
    std::streamsize gcount() { return ff.gcount(); }
    std::istream& getline(char* s, std::streamsize n, char delim) { return ff.getline(s, n, delim); }
    void close() { ff.close(); }
    void put(char ch) { ff.put(ch); }
    std::istream& get(char& ch) { return ff.get(ch); }
    void sync() { ff.sync(); }
    bool good() { return ff.good(); }
    void clear() { ff.clear(); }
    void flush() { ff.flush(); }
    int tellg() { return ff.tellg(); }
    void open(const char* filename, std::ios::openmode mode)
    {
        mod = mode;
        Path =  filename;
        ff.open(filename, mode);
    }
    std::ostream& seekp(long pos, std::ios_base::seekdir dir) { return ff.seekp(pos, dir); }
    std::istream& seekg(long pos, std::ios_base::seekdir dir) { return ff.seekg(pos, dir); }

    GemDataStream &operator>>( char &i );
    GemDataStream &operator>>( unsigned char &i ) { return operator>>((char&)i); }
    GemDataStream &operator>>( signed char &i ) { return operator>>((char&)i); }
    GemDataStream &operator>>( short &i );
    GemDataStream &operator>>( unsigned short &i ) { return operator>>((short&)i); }
    GemDataStream &operator>>( int &i );
    GemDataStream &operator>>( unsigned int &i ) { return operator>>((int&)i); }
    GemDataStream &operator>>( long &i );
    GemDataStream &operator>>( unsigned long &i ) { return operator>>((long&)i); }
    GemDataStream &operator>>( float &f );
    GemDataStream &operator>>( double &f );
    GemDataStream &operator>>( bool &i );
    GemDataStream &operator>>( std::string& i );

    GemDataStream &operator<<( char i );
    GemDataStream &operator<<( unsigned char i ) { return operator<<( static_cast<char>(i) ); }
    GemDataStream &operator<<( signed char i ) { return operator<<( static_cast<char>(i) ); }
    GemDataStream &operator<<( short i );
    GemDataStream &operator<<( unsigned short i ) { return operator<<(static_cast<short>(i) ); }
    GemDataStream &operator<<( bool i ) { return operator<<( static_cast<short>(i) ); }
    GemDataStream &operator<<( int i );
    GemDataStream &operator<<( unsigned int i ) { return operator<<(static_cast<int>(i)); }
    GemDataStream &operator<<( long i );
    GemDataStream &operator<<( unsigned long i ) { return operator<<(static_cast<long>(i)); }
    GemDataStream &operator<<( float f );
    GemDataStream &operator<<( double f );
    GemDataStream &operator<<( std::string f );


    template <class T> void writeArray( T* arr, int size )
    {
      if( !arr )
        return;
      for(int ii=0; ii<size; ii++)
       *this << arr[ii];
    }
    template <class T> void readArray( T* arr, int size )
    {
        if( !arr )
          return;
       for(int ii=0; ii<size; ii++)
       *this >> arr[ii];
    }
    template <class T> void writeArray( const std::vector<T>& arr )
    {
      int size = arr.size();
      *this << size;
      for(int ii=0; ii<size; ii++)
       *this << arr[ii];
    }
    template <class T> void readArray( std::vector<T>& arr)
    {
       int size;
       T value;
       arr.clear();
       *this >> size;
       for(int ii=0; ii<size; ii++)
       { *this >> value;
         arr.push_back(value);
       }
    }


};

template <> void GemDataStream::readArray( char* arr, int size );
template <> void GemDataStream::writeArray( char* arr, int size );

} // namespace jsonio

#endif
