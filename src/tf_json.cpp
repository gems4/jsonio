//  This is JSONIO library+API (https://bitbucket.org/gems4/jsonio)
//
/// \file v_json.cpp
/// Implementation API to work with bson data
/// Implementation of functions for data exchange to/from json format
//
// JSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include <cstring>
#include <iostream>
#include "jsonio/tf_json.h"
using namespace std;

namespace jsonio {


enum {
  jsBeginArray = '[',    //0x5b,
  jsBeginObject = '{',   //0x7b,
  jsEndArray = ']',      //0x5d,
  jsEndObject = '}',     //0x7d,
  jsNameSeparator = ':', //0x3a,
  jsValueSeparator = ',',//0x2c,
  jsQuote = '\"'      //0x22
};


IsUnloadToJson::~IsUnloadToJson()
{ }

/// Class for read/write bson structure from/to text file or string
class ParserJsonNode
{
  protected:

  string jsontext;
  const char *curjson;
  const char *end;

  bool xblanc();
  void getString( string& str );
  void getNumber( double& value, int& type );
  void parseValue( const string& name, JsonDom* object );
  void printNodeToStream( iostream& os, const JsonDom* object, int depth, bool dense );

public:

  ParserJsonNode():curjson(nullptr), end(nullptr)
   {}
  ~ParserJsonNode()
   {}

  /// Parse internal jsontext string to bson structure (without first {)
  void parseObject( JsonDom* object );
  void parseArray( JsonDom* object );

  /// Print JsonNode structure/array to JSON string
  void printNodeToJson( string& resStr, const JsonDom* object, bool dense );

  /// Read one json object from text file
  string readObjectText( fstream& fin );
  /// Set up internal jsontext before parsing
  void  setJsonText( const string& json );
};

// Print JsonNode structure/array to Json string
void printNodeToJson( string& recJson, const JsonDom* object, bool dense )
{
    ParserJsonNode pars;
    pars.printNodeToJson( recJson, object, dense );
}

// Save data from JsonNode structure/array to  json file
void printNodeToJson( fstream& fout, const JsonDom* object )
{
    string jsonstr;
    printNodeToJson( jsonstr, object );
    fout <<  jsonstr.c_str() << endl;
}

/// Parse one Json object from string to JsonNode structure
bool parseJsonToNode( const string& recJson, JsonDom* object )
{
  if( !recJson.empty() )
  {
    ParserJsonNode pars;
    auto startpos = recJson.find_first_of("{[");
    if( startpos == std::string::npos)
      return false;

    pars.setJsonText( recJson.substr( startpos+1 ) );
    if( recJson[startpos] == '[' )   // test array
    {
       object->updateTypeTop( JSON_ARRAY );
       pars.parseArray(  object );
    }
    else
      pars.parseObject(  object );
  }
  return true;
}

// Load data from json file to bson object
bool parseJsonToNode( fstream& fin, JsonDom* object )
{
    ParserJsonNode parserJson;
    char b;
    string objStr;

    while( !fin.eof() )
    {
      fin.get(b);
      if( b == jsBeginObject ||  b == jsBeginArray )
      {

        objStr =  parserJson.readObjectText( fin );
        //cout << objStr.c_str() << endl;

        if( b == jsBeginArray )   // test array
        {
           object->updateTypeTop( JSON_ARRAY );
           parserJson.parseArray(  object );
        }
        else
            parserJson.parseObject(  object );
        break;
      }
    }
   return true;
}

//--------------------------------------------------------------

// Read one Json Object from text file to string
string  ParserJsonNode::readObjectText( fstream& ff )
{
  jsontext = "";
  //jsontext += jsBeginObject;
  int cntBeginObject = 1;
  char input;
  bool intoString = false;

  do{
       ff.get( input );
       if( input ==  jsBeginObject && !intoString )
          cntBeginObject++;
       else if( input ==  jsEndObject && !intoString )
             cntBeginObject--;
       // test into string
       if( input == jsQuote &&  jsontext.back() != '\\' )
          intoString = !intoString;
       jsontext+= input;
     } while( cntBeginObject > 0 && !ff.eof());

  curjson = jsontext.c_str();
  end = curjson + jsontext.length();
  return jsontext;
}

// Load Json text
void  ParserJsonNode::setJsonText( const string& json )
{
  jsontext = json;
  curjson = jsontext.c_str();
  end = curjson + jsontext.length();
}

bool ParserJsonNode::xblanc()
{
    curjson += strspn( curjson, " \r\n\t" );
    while( *curjson == '#' )
    {
      curjson = strchr( curjson, '\n' );
      if( !curjson )
        return ( false );
      curjson += strspn( curjson, " \r\n\t" );
    }
    return (curjson < end);
}

// Get "<string>" data
void ParserJsonNode::getString( string& str )
{
    //curjson++;
    const char * posQuote = strchr( curjson, jsQuote );
    while( posQuote && *(posQuote-1) == '\\' )
      posQuote = strchr( posQuote+1, jsQuote );

    jsonioErrIf( !posQuote ,"E01JSon: ",
             "Missing \" - end of string constant.", curjson );

    str = string(curjson, 0, posQuote-curjson);
     // conwert all pair ('\\''\n') to one simbol '\n' and other
    convertReadedString( str );
    curjson = ++posQuote;
    jsonioErrIf( curjson >= end ,"E02JSon: ",
             "Termination by String.");
}

// Get <double/integer> data (convert nan to 0)
void ParserJsonNode::getNumber( double& value, int& type )
{
    const char *start = curjson;
    bool isInt = true;

    // minus
    if (curjson < end &&  ( *curjson == '-' || *curjson == '+' ) )
        ++curjson;

    // add new to test nan
    if( (end - curjson) > 3 && (*curjson++ == 'n' &&
        *curjson++ == 'a' && *curjson++ == 'n') )
    {
        type =  JSON_DOUBLE;
        value = 0.;
        return;
    }

    // int = zero / ( digit1-9 *DIGIT )
    while (curjson < end && isdigit(*curjson) )
            ++curjson;

    // frac = decimal-point 1*DIGIT
    if (curjson < end && *curjson == '.')
    {
        isInt = false;
        ++curjson;
        while (curjson < end && isdigit(*curjson) )
            ++curjson;
    }

    // exp = e [ minus / plus ] 1*DIGIT
    if (curjson < end && (*curjson == 'e' || *curjson == 'E'))
    {
        isInt = false;
        ++curjson;
        if (curjson < end && (*curjson == '-' || *curjson == '+'))
            ++curjson;
        while (curjson < end && isdigit(*curjson) )
            ++curjson;
    }

    jsonioErrIf( curjson >= end ,"E03JSon: ",
             "Termination by Number.");
    sscanf( start, "%lg", &value );
    if( isInt && ( value < SHORT_ANY && value > SHORT_EMPTY ) )
      type = JSON_INT;
    else
      type =  JSON_DOUBLE;       \
}

// value = false / null / true / object / array / number / string
void ParserJsonNode::parseValue( const string& name, JsonDom* object)
{
    int type = JSON_UNDEFINED;
    jsonioErrIf( !xblanc() ,"E04JSon: ", "Must be value.");

    switch (*curjson++)
    {
    case 'n':
        if( (end - curjson) > 3 && (*curjson++ == 'u' &&
            *curjson++ == 'l' && *curjson++ == 'l') )
        {
            type = JSON_NULL;
            object->appendNull( name );
            break;
        }
        jsonioErr( "E05JSon: ", "Illegal Value.", curjson);
    case 't':
        if( (end - curjson) > 3 && (*curjson++ == 'r' &&
            *curjson++ == 'u' && *curjson++ == 'e') )
        {
            type = JSON_BOOL;
            object->appendBool( name, true );
            break;
        }
        jsonioErr( "E05JSon: ", "Illegal Value.", curjson);
    case 'f':
        if( (end - curjson) > 4 && (*curjson++ == 'a' &&
            *curjson++ == 'l'  && *curjson++ == 's' && *curjson++ == 'e' ) )
        {
            type = JSON_BOOL;
            object->appendBool( name, false );
            break;
        }
        jsonioErr( "E05JSon: ", "Illegal Value.", curjson);
    case jsQuote:
       {
         string str = "";
         type = JSON_STRING;
         getString( str );
         object->appendString( name, str );
       }
       break;

    case jsBeginArray:
        {
           type = JSON_ARRAY;
           JsonDom* childobj =  object->appendArray(  name );
           parseArray( childobj );
        }
        break;

    case jsBeginObject:
       {
        type = JSON_OBJECT;
        JsonDom* childobj =  object->appendObject(  name );
        parseObject( childobj );
       }
       break;

    case jsEndArray:
         --curjson;
        break;  // empty array
    case jsEndObject:
         --curjson;
        break;  // empty object

    default:  // number
      { --curjson;
        if( isdigit(*curjson) || *curjson == '+' ||
            *curjson == '-' || *curjson == '.' ||
            *curjson == 'e' || *curjson == 'E'    )
        {
            double value = DOUBLE_EMPTY;
            getNumber( value, type );
            if( type == JSON_INT )
              object->appendInt( name, static_cast<int>(value) );
            else
              object->appendDouble( name, value );
            break;
        }
        else
            jsonioErr( "E05JSon: ", "Illegal Value.", curjson );
      }
      //break;
    }
}

//    array = [ <value1>, ... <valueN> ]
void ParserJsonNode::parseArray( JsonDom* object )
{
    int ndx = 0;

    while( *curjson != jsEndArray )
    {
      parseValue( to_string(ndx), object );
      jsonioErrIf( !xblanc() ,"E06JSon: ", "Unterminated Array." );
      if( *curjson == jsValueSeparator  )
        curjson++;
      else
        jsonioErrIf( *curjson != jsEndArray ,"E07JSon: ",
                 "Missing Value Separator.", curjson );
      ndx++;
    }
    curjson++;
}

//  object = { "<key1>" : <value1>, ... , "<keyN>" : <valueN> }
void ParserJsonNode::parseObject( JsonDom* object )
{
    string name;
    // read key
    jsonioErrIf( !xblanc() ,"E08JSon: ", "Unterminated Object.");

    while( *curjson != jsEndObject )
    {
      // read key
      jsonioErrIf( !xblanc() ,"E08JSon: ", "Unterminated Object.");

      if( *curjson++== jsQuote  )
      {
          getString( name );
       }
      else
        jsonioErr( "E10JSon: ", "Missing Key of Object.", curjson);

      jsonioErrIf( !xblanc() ,"E08JSon: ", "Unterminated Object.");
      if( *curjson == jsNameSeparator  )
          curjson++;
      else
        jsonioErr( "E09JSon: ", "Missing Name Separator.", curjson );
      // read value
      parseValue( name, object );
      jsonioErrIf( !xblanc() ,"E08JSon: ", "Unterminated Object.");
      if( *curjson == jsValueSeparator  )
        curjson++;
      else
        jsonioErrIf( *curjson != jsEndObject ,"E07JSon: ",
                 "Missing Value Separator.", curjson );
    }
    curjson++;
}

void ParserJsonNode::printNodeToStream( iostream& os, const JsonDom* object, int depth, bool dense )
{
    int temp;
    bool first = true;
    int objtype = object->getType();
    auto objsize = object->getChildrenCount();

    for( std::size_t ii=0; ii<objsize; ii++ )
    {
        auto childobj = object->getChild( ii);

        // do not print empty data
        if( childobj->getKey() == "_key" && childobj->toString().empty() )
            continue;
        if( childobj->getKey() == "_id" && childobj->toString().empty() )
            continue;

        if( !first )
            os << ( dense ? "," : ",\n" );
        else
            first = false;

        // before print
        switch( objtype )
        {
        case JSON_OBJECT:
            if(!dense)
            {
                for (temp = 0; temp <= depth; temp++)
                    os <<  "     ";
            }
            os << "\"" << childobj->getKey() << ( dense ? "\":" : "\" :   " );
            break;
        case JSON_ARRAY:
            if(!dense)
            {
                for (temp = 0; temp <= depth; temp++)
                    os << "     ";
            }
            break;
        default:
            break;
        }

        switch (childobj->getType())
        {
        // impotant datatypes
        case JSON_NULL:
            os << "null";
            break;
        case JSON_BOOL:
        case JSON_INT:
            os << childobj->toString();
            break;
        case JSON_DOUBLE:
            os << std::setprecision(TArray2Base::doublePrecision) << childobj->toDouble();
            break;
        case JSON_STRING:
        { // decode string '\\' to '\\\\', '\n' to '\\n'
            string str= childobj->toString();
            convertStringToWrite( str );
            os << "\"" << str << "\"";
        }
            break;

            // main constructions
        case JSON_OBJECT:
            os << ( dense ? "{" : "{\n" );
            printNodeToStream( os, childobj, depth + 1, dense );
            if(!dense)
            {
                for (temp = 0; temp <= depth; temp++)
                    os << "     ";
            }
            os << "}";
            break;
        case JSON_ARRAY:
            os << ( dense ? "[" : "[\n" );
            printNodeToStream(os, childobj, depth + 1, dense );
            if(!dense)
            {
                for (temp = 0; temp <= depth; temp++)
                    os << "     ";
            }
            os << "]";
            break;
        default:
            os  << "can't print type : " << childobj->getType();
        }
    }
    if( !dense )
        os << "\n";
}

void ParserJsonNode::printNodeToJson( string& resStr, const JsonDom* object, bool dense )
{
    stringstream os;
    int objtype = object->getType();
    if(objtype == JSON_OBJECT)
        os << ( dense ? "{" : "{\n" );
    else
        if(objtype == JSON_ARRAY)
            os << ( dense ? "[" : "[\n" );
        else
            return;
    printNodeToStream( os, object, 0, dense );
    if(objtype == JSON_OBJECT)
        os << "}";
    else
        os << "]";
    resStr = os.str();
}


} // namespace jsonio
