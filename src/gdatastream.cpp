//  This is JSONIO library+API (https://bitbucket.org/gems4/jsonio)
//
/// \file gdatastream.cpp
/// Implementation of GemDataStream class - binary data stream class
/// (ByteOrder : BigEndian or LittleEndian )
//
// JSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include <algorithm>
#include <iostream>
#include <stdint.h>
using namespace std;

#ifdef __linux__
#include <endian.h>
#elif defined(__APPLE__)
#include <machine/endian.h>
#define __BYTE_ORDER BYTE_ORDER
#define __BIG_ENDIAN BIG_ENDIAN
#else
//#warning "other plaftorm - considering little endian"
#define __BIG_ENDIAN 4321
#define __BYTE_ORDER 1234
#endif

#ifndef __BYTE_ORDER
# error "Error: __BYTE_ORDER not defined\n";
#endif

#include "jsonio/nservice.h"
#include "gdatastream.h"

namespace jsonio {

//--------------------------------------------------------------------

inline short SWAP(short x) { 
    return (((x>>8) & 0x00ff) | ((x<<8) & 0xff00)); 
}

inline int32_t SWAP(int32_t x) {
    int32_t x_new;
    char* xc_new = (char*)&x_new;
    char* xc = (char*)&x;
    xc_new[0] = xc[3];
    xc_new[1] = xc[2];
    xc_new[2] = xc[1];
    xc_new[3] = xc[0];
    return x_new;
}

inline float SWAP(float x) {
    float x_new;
    char* xc_new = (char*)&x_new;
    char* xc = (char*)&x;
    xc_new[0] = xc[3];
    xc_new[1] = xc[2];
    xc_new[2] = xc[1];
    xc_new[3] = xc[0];
    return x_new;
}

inline double SWAP(double x) {
    double x_new;
    char* xc_new = (char*)&x_new;
    char* xc = (char*)&x;
    xc_new[0] = xc[7];
    xc_new[1] = xc[6];
    xc_new[2] = xc[5];
    xc_new[3] = xc[4];
    xc_new[4] = xc[3];
    xc_new[5] = xc[2];
    xc_new[6] = xc[1];
    xc_new[7] = xc[0];
    return x_new;
}


GemDataStream::GemDataStream( string aPath, ios::openmode aMod  ):
        mod( aMod ),
        Path( aPath ),
        ff(aPath.c_str(), aMod)
{
    setByteOrder(LittleEndian);
    jsonioErrIf( !ff.good(), Path.c_str(), "Fileopen error");
}


void GemDataStream::setByteOrder( int bo )
{
    byteorder = bo;

#if __BYTE_ORDER == __BIG_ENDIAN
	swap = (byteorder == LittleEndian);
#warning "Compiling for BIG ENDIAN architecture!"
#else
	swap = (byteorder == BigEndian);
#endif
//    cerr << "GemDataStream::swap == " << swap << endl;
}

// NOTE: these functions better to write as a templates!!

GemDataStream &GemDataStream::operator>>( char &i )
{
    ff.read((char*)&i, sizeof(char));
    return *this;
}

GemDataStream &GemDataStream::operator>>( string& str )
{
    char ch;
    ff.read((char*)&ch, sizeof(char)); // first "
    std::getline( ff, str, '\"');
    return *this;
}

GemDataStream &GemDataStream::operator>>( short &i )
{
    ff.read((char*)&i, sizeof(short));
    if( swap ) i = SWAP(i);
    return *this;
}

GemDataStream &GemDataStream::operator>>( bool &i )
{
    short ii;
    ff.read((char*)&ii, sizeof(short));
    if( swap ) ii = SWAP(ii);
    i = ii;
    return *this;
}

GemDataStream &GemDataStream::operator>>( int &i_ )
{
    //ff.read((char*)&i, sizeof(int));
    int32_t i=(int32_t)i_;
    ff.read((char*)&i, sizeof(int32_t));
    if( swap ) i = SWAP(i);
    i_ = (int)i;
    return *this;
}

GemDataStream &GemDataStream::operator>>( long &i_ )
{
    //ff.read((char*)&i, sizeof(long));
    int32_t i=(int32_t)i_;
    ff.read((char*)&i, sizeof(int32_t));
    if( swap ) i = SWAP(i);
    i_ = (long)i;
    return *this;
}

GemDataStream &GemDataStream::operator>>( float &f )
{
    ff.read((char*)&f, sizeof(float));
    if( swap ) f = SWAP(f);
    return *this;
}

GemDataStream &GemDataStream::operator>>( double &f )
{
    ff.read((char*)&f, sizeof(double));
    if( swap ) f = SWAP(f);
    return *this;
}


// NOTE: these functions are inefficient !!!
// it's faster to read the whole array
// and then loop through it to reverse byte order!!!


GemDataStream &GemDataStream::operator<<( char i )
{
    ff.write(&i, sizeof(char));
    return *this;
}

GemDataStream &GemDataStream::operator<<( string str )
{
    ff << "\"" << str.c_str() <<  "\"";
    return *this;
}

GemDataStream &GemDataStream::operator<<( short i )
{
    if( swap ) i = SWAP(i);
    ff.write((char*)&i, sizeof(short));
    return *this;
}

GemDataStream &GemDataStream::operator<<( int i_ )
{
    int32_t i=(int32_t)i_;
    if( swap ) i = SWAP(i);
    ff.write((char*)&i, sizeof(int32_t));
    return *this;
}

GemDataStream &GemDataStream::operator<<( long i_ )
{
    int32_t i=(int32_t)i_;
    if( swap ) i = SWAP(i);
    ff.write((char*)&i, sizeof(int32_t));
    return *this;
}


GemDataStream &GemDataStream::operator<<( float f )
{
    if( swap ) f = SWAP(f);
    ff.write((char*)&f, sizeof(float));
    return *this;
}


GemDataStream &GemDataStream::operator<<( double f )
{
    if( swap ) f = SWAP(f);
    ff.write((char*)&f, sizeof(double));
    return *this;
}


template <>
void GemDataStream::readArray( char* arr, int size )
{
  if( !arr )
    return;
  ff.read(arr, size);
}


template <>
void GemDataStream::writeArray( char* arr, int size )
{
  if( !arr )
    return;
  ff.write(arr, size);
}

} // namespace jsonio
