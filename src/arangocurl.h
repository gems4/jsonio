#ifndef ARANGOCURL_H
#define ARANGOCURL_H

#include <curl/curl.h>
#include "arangorequests.h"

namespace jsonio { namespace arangodb {

class RequestCurlObject {

  public:

    RequestCurlObject( const std::string& theURL, const std::string& theUser,
                       const std::string& thePasswd,
                       std::unique_ptr<HttpMessage> request );

    ~RequestCurlObject()
    {
      if (_curlHeaders != nullptr)
        curl_slist_free_all(_curlHeaders);
      if (_curl != nullptr)
        curl_easy_cleanup(_curl);
    }

    static size_t bodyCallback(  char* pdatatr, size_t size, size_t nmemb, std::string* buffer);
    static size_t headerCallback( char* data, size_t size, size_t nitems, std::string* buffer);


    std::unique_ptr<HttpMessage> getResponse();

    std::string jsonBody()
    {
        std::string data;
        if (_responseBody.length())
        {
              ::arangodb::velocypack::Buffer<uint8_t> buffer;
              buffer.append(_responseBody);
              ::arangodb::velocypack::Slice slice(buffer.data());
              data = slice.toJson();
       }
       return data;
    }


  protected:

    std::string _URL;
    std::string _dbUser;
    std::string _dbPasswd;
    std::unique_ptr<HttpMessage> _request;
    std::string _responseHeaders;
    std::string _responseBody;

    CURL* _curl;
    struct curl_slist* _curlHeaders;
};


} }


#endif // ARANGOCURL_H
