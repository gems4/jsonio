//  This is JSONIO library+API (https://bitbucket.org/gems4/jsonio)
//
/// \file dbdocument.cpp
/// Implementation of class TDBDocumentBase - base interface of abstract DB document
//
// JSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2017-2018 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include <iostream>
#include <regex>
#include <thread>
#include "jsonio/dbdocument.h"
#include "jsonio/dbconnect.h"
#include "jsonio/jsondomfree.h"
using namespace std;

namespace jsonio {

void addFieldsToQuery( DBQueryData& query, const FieldSetMap& fldvalues );

// Default configuration of the Data Base
TDBDocumentBase::TDBDocumentBase( const TDataBase* dbconnect, const string& coltype, const string& colname  ):
    _colltype(coltype), _queryResult(nullptr)
{
    _collection = dbconnect->getCollection( coltype, colname  );
    _collection->addDocument(this);
}


// Default configuration of the Data Base
TDBDocumentBase::TDBDocumentBase( TDBCollection* collection  ):
    _collection( collection ), _colltype(collection->type() ), _queryResult(nullptr)
{
    _collection->addDocument(this);
}


// Configurator by readind from bson object
TDBDocumentBase::TDBDocumentBase( TDBCollection* collection, const JsonDom *object):
    _collection( collection ), _colltype(collection->type() ), _queryResult(nullptr)
{
    fromJsonNode( object );
    _collection->addDocument(this);
}

// Writes data to bson
void TDBDocumentBase::toJsonNode( JsonDom *object ) const
{
    if( _queryResult.get() != nullptr )
    {
        auto obj = object->appendObject( "DBQueryDef");
        _queryResult->getQuery().toJsonNode( obj );
    }
}

// Reads data from bson
void TDBDocumentBase::fromJsonNode( const JsonDom *object )
{
    auto obj = object->field("DBQueryDef");
    if( obj != nullptr )
    {
        SetQuery(DBQueryDef( obj ));
    }
}


string TDBDocumentBase::CreateWithTestValues( bool testValues )
{
    string key = getKeyFromCurrent();

    if( key.empty() && testValues )
    {
        key = getKeyFromValueNode();
        if( !key.empty() )
        {
            Update( key );
            return key;
        }
    }
    return Create( key );
}

void TDBDocumentBase::UpdateWithTestValues( bool testValues )
{
    string key = getKeyFromCurrent();

    if( !Find( key ))
       CreateWithTestValues( testValues );
    else
       Update( key );
}

//=============================================================================

// Execute query
//  \return resultData - list of json strings with query result
vector<string> TDBDocumentBase::runQuery( const DBQueryData&  query ) const
{
    vector<string> resultData;

    SetReadedFunction setfnc = [&resultData]( const string& jsondata )
    {
        resultData.push_back(jsondata);
    };

    _collection->selectQuery( query, setfnc );
    return resultData;
}

// Execute query
//  \return resultData - list of json strings with query result
vector<string> TDBDocumentBase::runByKeys( const std::vector<std::string>& rkeys ) const
{
    vector<string> resultData;

    SetReadedFunction setfnc = [&resultData]( const string& jsondata )
    {
        resultData.push_back(jsondata);
    };

    _collection->lookupByDocumentKeys( rkeys, setfnc );
    return resultData;
}


// Build table of fields values by query
ValuesTable TDBDocumentBase::downloadDocuments( const DBQueryData&  query, const std::vector<std::string>& queryFields ) const
{
    ValuesTable recordsValues;

    SetReadedFunction setfnc = [&, /*&recordsValues,*/ queryFields]( const string& jsondata )
    {
        FieldSetMap fldsValues = extractFields( queryFields, jsondata );
        vector<string> rowData;
        for( const auto& fld: queryFields)
            rowData.push_back( fldsValues[fld] );
        recordsValues.push_back(rowData);
    };

    _collection->selectQuery( query, setfnc );
    return recordsValues;
}

ValuesTable TDBDocumentBase::downloadDocuments( const vector<string>& keys, const vector<string>& queryFields ) const
{
    ValuesTable recordsValues;

    SetReadedFunction setfnc = [&, queryFields]( const string& jsondata)
    {
        FieldSetMap fldsValues = extractFields( queryFields, jsondata );
        vector<string> rowData;
        for( const auto& fld: queryFields)
        {
            rowData.push_back( fldsValues[fld] );
        }
        recordsValues.push_back(rowData);
    };

    _collection->lookupByDocumentKeys( keys, setfnc );
    return recordsValues;
}

// Test existence records
bool TDBDocumentBase::existKeysByQuery( DBQueryData& query ) const
{
    vector<string> queryFields = { "_id"};
    query.setQueryFields( queryFields );
    vector<string> resultData = runQuery( query );
    return !resultData.empty();
}

// Build keys list by query
vector<string> TDBDocumentBase::getKeysByQuery( DBQueryData& query ) const
{
    vector<string> keys;
    vector<string> queryFields = { "_id"};
    query.setQueryFields( queryFields );
    vector<string> resultData = runQuery( query );

    //--std::regex re("\\{\\s*\"_id\"\\s*:\\s*\"([^\"]*)\"\\s*\\}");
    std::regex re(".*\"_id\"\\s*:\\s*\"([^\"]*)\".*");
    std::string replacement = "$1";
    for( uint ii=0; ii<resultData.size(); ii++)
    {
        string akey = std::regex_replace( resultData[ii], re, replacement);
        //cout << _resultData[ii] << "      " << akey << endl;
        keys.push_back(akey);
    }
    return keys;
}

// selection functions -----------------------------------


void TDBDocumentBase::SetQuery( const DBQueryDef& querydef )
{
    if(_queryResult.get() == nullptr )
        _queryResult.reset( new DBQueryResult( querydef ) );
    else
        _queryResult->setQuery(querydef);
    updateQuery();
}

// Set&execute query for document
void TDBDocumentBase::SetQuery( DBQueryData query, std::vector<std::string>  fieldsList )
{
    if( query.empty()  )
        query = makeDefaultQueryTemplate();

    if( fieldsList.empty()  )
        fieldsList = makeDefaultQueryFields();

    SetQuery( DBQueryDef( fieldsList, query )  );
}

// extern functions ---------------------------------------

// Extract the string value by key from query
int extractIntField( const string& key, const string& jsondata )
{
    string token;
    string query2 =  replace_all( jsondata, "\'", "\"");
    string regstr =  string(".*\"")+key+"\"\\s*:\\s*([+-]?[1-9]\\d*|0).*";
    std::regex re( regstr );
    std::smatch match;

    if( std::regex_search( query2, match, re ))
    {
        if (match.ready())
            token = match[1];
    }
    //cout << key << "  token " << token  << endl;
    if( token.empty() )  // fix bug for null or noexist fields
        return 0;
    return stoi(token);
}

// Extract the string value by key from query
string extractStringField( const string& key, const string& jsondata )
{
    string token = "";
    string query2 =  replace_all( jsondata, "\'", "\"");
    string regstr =  string(".*\"")+key+"\"\\s*:\\s*\"([^\"]*)\".*";
    std::regex re( regstr );
    std::smatch match;

    if( std::regex_search( query2, match, re ))
    {
        if (match.ready())
            token = match[1];
    }
    //cout << key << "  token " << token  << endl;
    return token;
}

// Into ArangoDB Query by example ( must be { "a.b" : value } , no { "a": { "b" : value } }
// https://docs.arangodb.com/3.3/Manual/DataModeling/Documents/DocumentMethods.html
void addFieldsToQueryAQL( DBQueryData& query, const FieldSetMap& fldvalues )
{
    cout << query.getQueryString() << endl;
    //jsonioErrIf( query.getType() != DBQueryData::qTemplate, "addFieldsToQuery", "Query not template" );

    auto newQueryString = query.getQueryString();
    auto type = query.getType();

    switch(  type )
    {
    case DBQueryData::qTemplate:
    {
        auto pos = newQueryString.find_last_of("}");
        newQueryString = newQueryString.substr(0, pos);
        newQueryString += ", " + query.generateFILTER(fldvalues, true ) +  "}";
    }
        break;
    case DBQueryData::qAll:
    {
        type = DBQueryData::qTemplate;
        newQueryString =  newQueryString += "{ " + query.generateFILTER(fldvalues, true ) +  " }";
    }
        break;
    case DBQueryData::qAQL:
    {
        auto pos = newQueryString.find("RETURN");  // add before return
        string retstring = "";
        if( pos != string::npos )
            retstring = newQueryString.substr( pos);
        newQueryString = newQueryString.substr(0, pos);
        newQueryString += query.generateFILTER(fldvalues, false, "u" ); // !!! only u
        newQueryString += retstring;
    }
        break;
    default: jsonioErr( "addFieldsToQueryAQL", "Illegal query type to modify" );

    }
    DBQueryData newQuery(newQueryString, type);
    newQuery.setBindVars( query.getBindVars());
    newQuery.setOptions( query.options());
    newQuery.setQueryFields( query.getQueryFields() );
    query = newQuery;
    cout << " Result: " <<  query.getQueryString() << endl;
}

void addFieldsToQuery( DBQueryData& query, const FieldSetMap& fldvalues )
{
    cout << query.getQueryString() << endl;

    jsonioErrIf( query.getType() != DBQueryData::qTemplate, "addFieldsToQuery", "Query not template" );

    auto jsonquery = unpackJson(query.getQueryString());
    for(auto& ent : fldvalues)
        jsonquery->updateFieldPath( ent.first, ent.second );

    string newQuery;
    printNodeToJson( newQuery, jsonquery.get() );
    query = DBQueryData(newQuery, query.getType());
    cout << " Result: " <<  query.getQueryString() << endl;
}


} // namespace jsonio
