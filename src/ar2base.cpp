//  This is JSONIO library+API (https://bitbucket.org/gems4/jsonio)
//
/// \file ar2base.cpp
/// Converters between types using a combination of implicit and
/// user-defined conversions.
//
// JSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include "jsonio/ar2base.h"
using namespace std;

namespace jsonio {

const short SHORT_EMPTY  = -32768;
const short SHORT_ANY  = 32767;
#define USHORT_EMPTY   0
#define USHORT_ANY     65535
const long LONG_EMPTY  =  -2147483648L; //Int 32 -2,147,483,648 / 2,147,483,647
const long  LONG_ANY   =   2147483647L;
#define ULONG_EMPTY   0UL
#define ULONG_ANY     4294967295UL
const float FLOAT_EMPTY	=  1.17549435e-38F;
#ifdef _MSC_VER
const float FLOAT_ANY = 3.402823466e+38F;
#else
const float FLOAT_ANY   =  3.40282347e+38F;
#endif
const double DOUBLE_EMPTY = 2.2250738585072014e-308;
const double DOUBLE_ANY   = 1.7976931348623157e+308;
#define UCHAR_EMPTY   0
#define UCHAR_ANY     0xFF
#define SCHAR_EMPTY   -127
#define SCHAR_ANY     127

const char CHAR_EMPTY =  '`';
const char CHAR_ANY  = '*';
const char* S_EMPTY	=    "`";
const char* S_ANY  	=    "*";

int TArray2Base::doublePrecision = 15;
int TArray2Base::floatPrecision = 7;

//-------------------------------------------------------


bool IsFloatEmpty( const float v )
{
    return ( v > 0 && v <= FLOAT_EMPTY);
}

bool IsDoubleEmpty( const double v )
{
    return ( v>0. && v <= DOUBLE_EMPTY);
}

TArray2Base::~TArray2Base()
{ }

template <>  bool TArray2Base::string2value( const string& data, string& value )
{
    value = data;
    return true;
}


template <> string TArray2Base::value2string( const string& value )
{
    return value;
}

template <> string TArray2Base::value2string( const double& value )
{
    if( isEMPTY( value ) )
        return S_EMPTY;
    if( isANY( value ) )
        return S_ANY;
    else
    {   char vbuf[50];
        sprintf(vbuf, "%.*lg" , doublePrecision, value );
        return vbuf;
    }
}

template <> string TArray2Base::value2string( const float& value )
{
    if( isEMPTY(value) )
        return S_EMPTY;
    if( isANY(value) )
        return S_ANY;
    else
    {   char vbuf[50];
        sprintf(vbuf, "%.*lg" , floatPrecision, value );
        return vbuf;
    }
}

// Write char value to file
template<> string TArray2Base::value2string( const char& val )
{
    return string(1, val);
}

// Write bool value to file
template<> string TArray2Base::value2string( const bool& val )
{
    if( val)
        return "true";
    else
        return "false";
}

// Write value to double
template <> double TArray2Base::value2double( const string& value )
{
    double val = 0.;
    string2value( value, val );
    return val;
}

// Read value from double
template <> void TArray2Base::double2value( const double data, string& value )
{
    value = value2string( data );
}


template<> void TArray2Base::setEMPTY( short& val )
{
    val = SHORT_EMPTY;
}

template<> void TArray2Base::setEMPTY( bool& val )
{
    val = false;
}

template<> void TArray2Base::setEMPTY( unsigned short& val )
{
    val = USHORT_EMPTY;
}

template<> void TArray2Base::setEMPTY( long& val )
{
    val = LONG_EMPTY;
}

template<> void TArray2Base::setEMPTY( int& val )
{
    val = LONG_EMPTY;
}

template<> void TArray2Base::setEMPTY( unsigned long& val )
{
    val = ULONG_EMPTY;
}

template<> void TArray2Base::setEMPTY( float& val )
{
    val = FLOAT_EMPTY;
}

template<> void TArray2Base::setEMPTY( double& val )
{
    val = DOUBLE_EMPTY;
}

template<> void TArray2Base::setEMPTY( unsigned char& val )
{
    val = UCHAR_EMPTY;
}

template<> void TArray2Base::setEMPTY( signed char& val )
{
    val = SCHAR_EMPTY;
}

template<> void TArray2Base::setEMPTY( string& val )
{
    val = S_EMPTY;
}

template<> void TArray2Base::setEMPTY( char& val )
{
    val = CHAR_EMPTY;
}

template<> bool TArray2Base::isEMPTY( const bool&  )
{
    return false;
}

template<> bool TArray2Base::isEMPTY( const short& val )
{
    return(val==SHORT_EMPTY);
}

template<> bool TArray2Base::isEMPTY( const unsigned short& val )
{
    return(val==USHORT_EMPTY);
}

template<> bool TArray2Base::isEMPTY( const long& val  )
{
    return(val==LONG_EMPTY);
}

template<> bool TArray2Base::isEMPTY( const long long& val  )
{
    return(val==LONG_EMPTY);
}

template<> bool TArray2Base::isEMPTY( const int& val  )
{
    return(val==LONG_EMPTY);
}

template<> bool TArray2Base::isEMPTY( const unsigned long& val )
{
    return(val==ULONG_EMPTY);
}

template<> bool TArray2Base::isEMPTY( const float& val )
{
    return  IsFloatEmpty(val);
}

template<> bool TArray2Base::isEMPTY( const double& val )
{
    return IsDoubleEmpty( val );
}

template<> bool TArray2Base::isEMPTY( const unsigned char& val  )
{
    return(val==UCHAR_EMPTY);
}

template<> bool TArray2Base::isEMPTY( const signed char& val)
{
    return(val==SCHAR_EMPTY);
}

template<> bool TArray2Base::isEMPTY( const char& val )
{
    return(val==CHAR_EMPTY);
}

template<> bool TArray2Base::isEMPTY( const string& val )
{
    return(val==S_EMPTY);
}

template<> bool TArray2Base::isANY( const bool&  )
{
    return false;
}

template<> bool TArray2Base::isANY( const short& val )
{
    return(val==SHORT_ANY);
}

template<> bool TArray2Base::isANY( const unsigned short& val )
{
    return(val==USHORT_ANY);
}

template<> bool TArray2Base::isANY( const long& val  )
{
    return(val==LONG_ANY);
}
template<> bool TArray2Base::isANY( const long long& val  )
{
    return(val==LONG_ANY);
}

template<> bool TArray2Base::isANY( const int& val  )
{
    return(val==LONG_ANY);
}

template<> bool TArray2Base::isANY( const unsigned long& val )
{
    return(val==ULONG_ANY);
}

template<> bool TArray2Base::isANY( const float& val )
{
    return(val>=FLOAT_ANY);
}

template<> bool TArray2Base::isANY( const double& val )
{
    return(val>=DOUBLE_ANY);
}

template<> bool TArray2Base::isANY( const unsigned char& val  )
{
    return(val==UCHAR_ANY);
}

template<> bool TArray2Base::isANY( const signed char& val)
{
    return(val==SCHAR_ANY);
}

template<> bool TArray2Base::isANY( const char& val )
{
    return(val==CHAR_ANY);
}

template<> bool TArray2Base::isANY( const string& val )
{
    return(val==S_ANY);
}

} // namespace jsonio



