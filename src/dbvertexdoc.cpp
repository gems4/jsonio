//  This is JSONIO library+API (https://bitbucket.org/gems4/jsonio)
//
/// \file dbgraph.cpp
/// Implementation of classes TGraphAbstract - working with graph databases (OLTP)
/// TGraphEJDB - working with internal EJDB data base in graph mode
/// Used  SchemaNode class - API for direct code access to internal
/// DOM based on our JSON schemas.
//
// JSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include <iostream>
#include <chrono>
#include "jsonio/dbedgedoc.h"
#include "jsonio/dbconnect.h"
#include "jsonio/io_settings.h"
using namespace std;

namespace jsonio {

// TGraphAbstract ---------------------------------------------

//vector<std::string> graphKeyFldsInf = { };

string collectionNameFromSchema( const string& schemaName )
{
    string labelVertex = ioSettings().Schema()->getVertexCollection( schemaName );
    if(!labelVertex.empty())
        return labelVertex;

    labelVertex = ioSettings().Schema()->getEdgeCollection( schemaName );
    if(!labelVertex.empty())
        return labelVertex;
    return ioSettings().collection();
}

TDBVertexDocument* TDBVertexDocument::newVertexDocument(
        const TDataBase* dbconnect, const string& schemaName  )
{
    if( schemaName.empty()  )
        return nullptr;

    TDBVertexDocument* newDov = new TDBVertexDocument( schemaName, dbconnect );
    return newDov;
}


TDBVertexDocument* TDBVertexDocument::newVertexDocumentQuery( const TDataBase* dbconnect,
                                   const string& schemaName, const DBQueryData& query  )
{
    if( schemaName.empty()  )
        return nullptr;

    std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
    TDBVertexDocument* newDov = new TDBVertexDocument( schemaName, dbconnect );
    std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();

    // init internal selection block
    newDov->SetQuery(query);
    std::chrono::high_resolution_clock::time_point t3 = std::chrono::high_resolution_clock::now();

    auto duration1 = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
    auto duration3 = std::chrono::duration_cast<std::chrono::microseconds>( t3 - t2 ).count();

    cout << schemaName <<  " Loding collection " << duration1 <<  " Loding query " << duration3 << endl;

    return newDov;
}

TDBVertexDocument::TDBVertexDocument( const string& aschemaName, TDBCollection* collection  ):
    TDBSchemaDocument( aschemaName, collection ),
    _changeSchemaMode(false)
{
    resetSchema( aschemaName, true );
}

TDBVertexDocument::TDBVertexDocument( const string& aschemaName, const TDataBase* dbconnect ):
    TDBVertexDocument( aschemaName,  dbconnect, "vertex", collectionNameFromSchema( aschemaName ) )
{
    resetSchema( aschemaName, true );
}

/// Constructor from bson data
TDBVertexDocument::TDBVertexDocument( TDBCollection* collection, const JsonDom *object ):
    TDBSchemaDocument( collection, object ), _changeSchemaMode(false)
{
    resetSchema( _schemaName, true );
}

DBQueryData TDBVertexDocument::makeDefaultQueryTemplate() const
{
    string queryJson =  "{'_label': '";
    queryJson += _label;
    queryJson += "' }";

    return DBQueryData(queryJson, DBQueryData::qTemplate );
}

// Change base collections
void TDBVertexDocument::updateCollection( const string& aschemaName )
{
    string oldName = _collection->name();
    string newName = collectionNameFromSchema( aschemaName );
    if( newName != oldName )
    {
        _collection->removeDocument(this);
        _collection = database()->getCollection( _colltype, newName  );
        _collection->addDocument(this);
    }

}

void TDBVertexDocument::resetSchema( const string& aschemaName, bool change_queries )
{
    if( _schemaName != aschemaName || change_queries  )
    {
        _schemaName = aschemaName;

        _currentData.reset( JsonDomSchema::newSchemaObject( _schemaName ) );
        auto type_node = _currentData->field("_type");
        _type = "";
        if( type_node != nullptr )
            type_node->getValue(_type);
        jsonioErrIf( _type != type(), "TGRDB0004", "Illegal record type: " + _type );

        type_node = _currentData->field("_label");
        _label = "";
        if( type_node != nullptr )
            type_node->getValue(_label);

        // update collection
        updateCollection(  aschemaName );
    }
    if( change_queries && _queryResult.get() != nullptr  )
    {
       // new one
       //SetQuery( makeDefaultQueryTemplate(), makeDefaultQueryFields());
       // old one
       _queryResult->setFieldsCollect(makeDefaultQueryFields());
       _queryResult->setQueryCondition(makeDefaultQueryTemplate());
       _queryResult->setToSchema(_schemaName);
    }

    // load unique map
    loadUniqueFields();
}

// Test true type and label for schema
void TDBVertexDocument::testSchema( const string& jsondata )
{
    string newtype = extractStringField( "_type", jsondata );
    string newlabel = extractStringField( "_label", jsondata );

    if( newtype.empty() || newlabel.empty() )
        return;

    if( _changeSchemaMode )
    {
        string newSchema;
        if( newtype == "edge")
            newSchema = _schema->getEdgeName(newlabel);
        else
            if( newtype == "vertex")
                newSchema = _schema->getVertexName(newlabel);
        jsonioErrIf(newSchema.empty(),  "TGRDB0007",
                    "Undefined record: " + newtype + " type " + newlabel + " label" );
        resetSchema( newSchema, false );
    } else
    {
        if( newtype != _type || newlabel != _label )
            jsonioErr("TGRDB0004", "Illegal record type: " + newtype + " or label: " + newlabel );
    }
}

void TDBVertexDocument::testUpdateSchema( const std::string&  pkey )
{
    string _id = pkey;
    // _id.pop_back(); // delete ":"
    string schemaName = extractSchemaFromId( _id  );
    if( !schemaName.empty() && schemaName != getSchemaName() )
        resetSchema(schemaName, false );
}

void TDBVertexDocument::loadUniqueFields()
{
    uniqueFieldsNames.clear();
    uniqueFieldsValues.clear();

    ThriftStructDef* strDef = _schema->getStruct(_schemaName);
    if( strDef == nullptr )
        return;

    /// temporaly block unique test
    //uniqueFieldsNames = strDef->getUniqueList();
    if( uniqueFieldsNames.empty() )
        return;

    uniqueFieldsNames.push_back("_id");
    ValuesTable uniqueVal = downloadDocuments( makeDefaultQueryTemplate(), uniqueFieldsNames);

    for( auto row: uniqueVal)
    {
        auto idkey = row.back();
        row.pop_back();

        if( !uniqueFieldsValues.insert( std::pair< vector<string>, string>(row, idkey)).second )
        {
            cout << "Not unique values: " << string_from_vector( row ) << endl;
        }
    }
}

// Load data from bson structure (return read record key)
string TDBVertexDocument::recFromJson( const string& jsondata )
{
    testSchema( jsondata );
    SetJson( jsondata );
    // Get key of record
    return getKeyFromCurrent();
}


/// Delete all edges by vertex id
void TDBVertexDocument::beforeRm( const std::string& key )
{
    string  _vertexid = key;

    /*string bsonquery = "{  \"_type\": \"edge\",  \"$or\": [ { \"_to\":  \"";
           bsonquery += _vertexid + "\" }, { \"_from\": \"";
           bsonquery += _vertexid + "\" } ], \"$dropall\": true }";
    DBQueryData  query( bsonquery, DBQueryData::qEJDB );*/

    shared_ptr<TDBEdgeDocument> edges( documentAllEdges( this->database() ));
    auto query = edges->allEdgesQuery( _vertexid );
    auto edgekeys =  edges->getKeysByQuery( query );

    for( auto idedge: edgekeys )
    {
      //cout << idedge << endl;
      edges->testUpdateSchema( idedge );
      edges->Delete(idedge);
    }

   // very slow
   // _collection->deleteEdges( _vertexid );
}

void TDBVertexDocument::afterRm( const std::string& key )
{

    // delete from unique map
    if( !uniqueFieldsNames.empty() )
    {
        string  _vertexid = key;
        //strip_all( _vertexid, ":" ); // get id from key
        //_vertexid =_vertexid.substr(0, _vertexid.size()-1); // delete ":"
        auto itrow = uniqueLinebyId( _vertexid );
        if( itrow != uniqueFieldsValues.end() )
            uniqueFieldsValues.erase(itrow);
    }
}

UniqueFieldsMap::iterator TDBVertexDocument::uniqueLinebyId( const string& idschem )
{
    UniqueFieldsMap::iterator itrow = uniqueFieldsValues.begin();
    while( itrow != uniqueFieldsValues.end() )
    {
        if( itrow->second == idschem )
            break;
        itrow++;
    }
    return itrow;
}

void TDBVertexDocument::beforeSaveUpdate( const std::string&  )
{
    if( !uniqueFieldsNames.empty() )
    {
        FieldSetMap uniqfields = extractFields( uniqueFieldsNames, _currentData.get() );

        vector<string> uniqValues;
        for( uint ii=0; ii<uniqueFieldsNames.size()-1; ii++ )
            uniqValues.push_back( uniqfields[uniqueFieldsNames[ii]] );

        auto itfind = uniqueFieldsValues.find(uniqValues);
        if( itfind != uniqueFieldsValues.end() )
        {
            if( itfind->second != uniqfields["_id"] )
                jsonioErr("TGRDB0009", "Not unique values: " + string_from_vector( uniqValues ) );
        }
    }
}

void TDBVertexDocument::afterSaveUpdate( const std::string&  )
{
    if( !uniqueFieldsNames.empty() )
    {
        FieldSetMap uniqfields = extractFields( uniqueFieldsNames, _currentData.get() );

        // delete old
        auto itrow = uniqueLinebyId( uniqfields["_id"] );
        if( itrow != uniqueFieldsValues.end() )
            uniqueFieldsValues.erase(itrow);

        // insert new
        vector<string> uniqValues;
        for( uint ii=0; ii<uniqueFieldsNames.size()-1; ii++ )
            uniqValues.push_back( uniqfields[uniqueFieldsNames[ii]] );
        if( !uniqueFieldsValues.insert( std::pair< vector<string>, string>(uniqValues, uniqfields["_id"])).second )
        {
            cout << "Not unique values: " << string_from_vector( uniqValues ) << endl;
        }
    }
}


// Extract label by id (old function )
string TDBVertexDocument::extractLabelById( const string& id )
{
    string token;

    auto query =  idQuery( id );
    vector<string> queryFields = { "_label"};
    query.setQueryFields( queryFields );
    vector<string> resultData = runQuery( query );
    if( resultData.size()>0  )
        token = extractStringField( "_label", resultData[0] );

    return token;
}

// Extract label from id
string TDBVertexDocument::extractLabelFromId( const string& id )
{
    string token;
    queue<string> names = split(id, "/");
    if(names.size()>1)
        token = names.front();
    return token;
}

/*string TDBVertexDocument::getSchemaFromId( const string& id  )
{
    string schemaName, label = extractLabelById( id );
    if( !( label.empty() ) )
       schemaName = _schema->getVertexName(label);
    return schemaName;
}*/

// build functions ------------------------------------------------------------

// Define new Vertex
void TDBVertexDocument::setVertexObject( const string& aschemaName, const FieldSetMap& fldvalues )
{
    // check schema
    if( _schemaName != aschemaName )
    {
        if( _changeSchemaMode )
            resetSchema( aschemaName, false );
        else
            jsonioErr("TGRDB0007", "Illegal record schame: " + aschemaName + " current schema: " + _schemaName );
    }
    _currentData->clearField(); // set default values

    for(auto const &ent : fldvalues)
        setValue( ent.first, ent.second  );
}

// Update current schema data
void TDBVertexDocument::updateVertexObject( const string& aschemaName, const FieldSetMap& fldvalues )
{
    // check schema
    if( _schemaName != aschemaName )
        jsonioErr("TGRDB0008", "Illegal record schame: " + aschemaName + " current schema: " + _schemaName );
    for(auto const &ent : fldvalues)
        setValue( ent.first, ent.second  );
}


/*ValuesTable TDBVertexDocument::loadRecords( const vector<string>& ids, const vector<string>& queryFields )
{
    ValuesTable recordsValues;
    FieldSetMap  fldsValues;
    vector<string> rowData;

    for( const auto& id_: ids )
    {
      GetRecord( (id_+":").c_str() );
      fldsValues = extractFields( queryFields, _currentData.get() );

      rowData.clear();
      for( const auto& fld: queryFields)
        rowData.push_back( fldsValues[fld] );
      recordsValues.push_back(rowData);
    }

    return recordsValues;
}*/

FieldSetMap TDBVertexDocument::loadRecordFields( const string& id, const vector<string>& queryFields )
{
    Read( id/*+":"*/ );
    FieldSetMap fldsValues = extractFields( queryFields, _currentData.get() );
    return fldsValues;
}


} // namespace jsonio
