//  This is JSONIO library+API (https://bitbucket.org/gems4/jsonio)
//
/// \file traversal.cpp
/// Implementation of class GraphTraversal - Graph Traversal for Database.
//
// JSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2017 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include "jsonio/traversal.h"
#include "jsonio/io_settings.h"
//#include "jsonio/arrs2cfg.h"

namespace jsonio {

GraphTraversal::GraphTraversal( const TDataBase* dbconnect )
{
   std::string  schemaName = ioSettings().Schema()->getVertexesList()[0];

   TDBVertexDocument* verDoc = new TDBVertexDocument( schemaName, dbconnect  );
   verDoc->resetMode(true);
   _vertexdoc.reset( verDoc );

   schemaName = ioSettings().Schema()->getEdgesList()[0];
   TDBEdgeDocument* edgeDoc = new TDBEdgeDocument( schemaName, dbconnect  );
   edgeDoc->resetMode(true);
   _edgedoc.reset( edgeDoc );

}

// visit Vertex and all linked Edges
bool GraphTraversal::parseVertex( const std::string& idVertex, GraphElementFunction afunc  )
{
  // Test Vertex is parsed before
  if( _vertexList.find( idVertex ) !=  _vertexList.end() )
    return false;

  std::cout << "idVertex " << idVertex << std::endl;
  _vertexList.insert( idVertex );
  _vertexdoc->Read( idVertex );
  std::string tojson;
  printNodeToJson( tojson, _vertexdoc->getDom() );
  afunc( true, tojson );

  // read all edges
  std::vector<std::string> edgeslst;

  if( travType&trOut )
  {
    edgeslst = _edgedoc->getOutEdgesKeys( idVertex );
    for(auto const &ent : edgeslst)
       parseEdge( ent,  afunc  );
  }

  if( travType&trIn )
  {
    edgeslst = _edgedoc->getInEdgesKeys( idVertex );
    for(auto const &ent : edgeslst)
      parseEdge( ent, afunc  );
  }
  return true;

}

//  visit Edge and all linked Vertexes
bool GraphTraversal::parseEdge( const std::string& idEdge, GraphElementFunction afunc  )
{
    std::string vertexId, vertexkey;
    // Test Edge is parsed before
    if( _edgeList.find( idEdge ) !=  _edgeList.end() )
      return false;

    std::cout << "idEdge " << idEdge << std::endl;
    _edgeList.insert( idEdge );
    _edgedoc->Read( idEdge );
    std::string tojson;
    printNodeToJson( tojson, _edgedoc->getDom() );

    if( (travType&trIn) )
    {
      if( _edgedoc->getValue("_from", vertexId) )
      {
         vertexkey = vertexId;
        _vertexdoc->testUpdateSchema( vertexkey );
        if( _vertexdoc->Find( vertexkey ) )
           parseVertex( vertexId, afunc );
      }
    }

    if( (travType&trOut) )
    {
     if( _edgedoc->getValue("_to", vertexId) )
     {
         vertexkey = vertexId;
        _vertexdoc->testUpdateSchema( vertexkey );
        if( _vertexdoc->Find( vertexkey ) )
               parseVertex( vertexId, afunc );
     }
    }
    afunc( false, tojson );
    return true;
}

void GraphTraversal::RestoreGraphFromFile( const std::string& filePath )
{
    std::string type;
    std::string curRecord;

    FJsonArray file( filePath);
    file.Open( OpenModeTypes::ReadOnly );
    while( file.LoadNext( curRecord ) )
    {
        type = extractStringField( "_type", curRecord );
        if( type.empty() )
           continue;
        if( type == "edge" )
           _edgedoc->UpdateFromJson( curRecord );
        else
            if( type == "vertex" )
               _vertexdoc->UpdateFromJson( curRecord );
    }
    file.Close();
}


} // namespace jsonio
