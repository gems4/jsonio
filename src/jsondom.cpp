
#include "jsonio/jsondom.h"
#include "jsonio/tf_json.h"
using namespace std;

namespace jsonio {

template <> bool JsonDom::appendValue( const std::string& akey, int value )
{
    return appendInt( akey, value );
}

template <> bool JsonDom::appendValue( const std::string& akey, bool value )
{
    return appendBool( akey, value );
}

template <> bool JsonDom::appendValue( const std::string& akey, std::string value )
{
    return appendString( akey, value );
}

template <> bool JsonDom::appendValue( const std::string& akey, const char* value )
{
    return appendString( akey, value );
}

// json string for Object and Array
string JsonDom::toString( bool dense ) const
{
   if( getType()==JSON_OBJECT || getType()==JSON_ARRAY )
   {
     string jsondata;
     printNodeToJson( jsondata, this, dense );
     return jsondata;
   }
   return getFieldValue();
}

double JsonDom::toDouble() const
{
  double val = 0.0;
  if( getType()==JSON_DOUBLE || getType()==JSON_INT )
        TArray2Base::string2value( getFieldValue(), val );
  return val;
}

int JsonDom::toInt() const
{
  int val = 0;
  if( getType()==JSON_INT || getType()==JSON_BOOL )
        TArray2Base::string2value( getFieldValue(), val );
  return val;
}

bool JsonDom::toBool() const
{
  bool val = false;
  if( getType()==JSON_BOOL )
      val = ( getFieldValue() == "true");
  return val;
}

bool JsonDom::appendScalar(const string& key, const string& value )
{
    int ival = 0;
    double dval=0.;

    if( testKey( key ) )  // field with key already exist
      return false;

    if( value == "~" )
       appendNull( key );
    else
     if( value == "null" )
        appendNull( key );
     else
      if( value == "true" )
        appendBool( key, true );
      else
       if( value == "false" )
            appendBool( key, false );
       else
        if( is<int>( ival, value ) )
           appendInt(  key, ival );
        else
         if( is<double>( dval, value ))
              appendDouble( key, dval );
         else
              appendString( key, value );
    return true;
}


//----------------------------------------------------------------------


/// Get Field Path from Node
string JsonDom::getFieldPath() const
{
  if( getParent()->isTop() )
     return getKey();
  else
     return  getParent()->getFieldPath()+"."+ getKey();
}

// Set _id to Node
void JsonDom::setOid_( const string& _oid  )
{
    jsonioErrIf( !isTop(), "setOid", "Illegal function on level");

    if( !_oid.empty() )
    {
        queue<string>  parts = split( _oid, "/");
        if( !setFieldValue( "_key", parts.back() ) )
            appendValue( "_key", parts.back() );
        if( !setFieldValue( "_id", _oid ) )
            appendValue( "_id", _oid );
    }
    else
    {
       // only clear
       setFieldValue( "_key", _oid );
       setFieldValue( "_id", _oid );
    }

}


} // namespace jsonio
