//  This is JSONIO library+API (https://bitbucket.org/gems4/jsonio)
//
/// \file dbarango.cpp
/// Implementation of class
/// TArangoDBClient - DB driver for working with ArangoDB
//
// JSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include <cstring>
#include <iostream>
#include "arangodbusers.h"
#include "jsonio/dbquerydef.h"
#include "jsonio/io_settings.h"

namespace jsonio {

const char* local_server_endpoint = "http://localhost:8529";
const char* local_server_username  = "root";
const char* local_server_password  = "";
const char* local_server_database  = "_system";
const char* local_root_username  = "root";
const char* local_root_password  = "";
const char* local_root_database  = "_system";
const char* remote_server_endpoint = "https://db.thermohub.net";
const char* remote_server_username  = "__put_here_the_user_name__";
const char* remote_server_password  = "__put_here_the_remote_password__";
const char* remote_server_database  = "_system";


//ArangoDBConnect TArangoDBClientOne::theConnect( local_server_endpoint,
//           local_server_username,  local_server_password,  local_server_database  );


bool operator!=(const ArangoDBUser& lhs, const ArangoDBUser& rhs)
{
    return  lhs.name != rhs.name || lhs.access != rhs.access || lhs.password != rhs.password;
}

bool operator!=(const ArangoDBConnect& lhs, const ArangoDBConnect& rhs)
{
    return lhs.serverUrl != rhs.serverUrl || lhs.user != rhs.user ||
              lhs.databaseName != rhs.databaseName;
}

void ArangoDBConnect::getFromSettings( const std::string& group, bool rootdata )
{
    GroupSettings dbGroup(ioSettings().group(group));

    if( rootdata)
    {
        serverUrl = dbGroup.value( "DB_URL", serverUrl );
        databaseName = dbGroup.value( "DBRootName", databaseName );
        user.name = dbGroup.value( "DBRootUser", user.name );
        user.password = dbGroup.value( "DBRootPassword", user.password  );
        user.access = dbGroup.value( "DBAccess", user.access  );
    }
    else
    {
        serverUrl = dbGroup.value( "DB_URL", serverUrl );
        databaseName = dbGroup.value( "DBName", databaseName );
        user.name = dbGroup.value( "DBUser", user.name );
        user.password = dbGroup.value( "DBUserPassword", user.password  );
        user.access = dbGroup.value( "DBAccess", user.access  );
    }
}


// TArangoDBClient ---------------------------------------------

// Default Constructor
TArangoDBClient::TArangoDBClient()
{
  connectData.getFromSettings(ioSettings().defaultArangoDB());
  resetDBConnection( connectData );
}

void TArangoDBClient::resetDBConnection( const ArangoDBConnect& aconnectData )
{
    connectData =aconnectData;
    pdata.reset( new arangodb::ArangoDBCollectionAPI(connectData) );
}


// Creates a new document in the collection from the given data
std::string TArangoDBClient::createRecord( const std::string& collname, const std::string& jsonrec )
{
    return pdata->CreateRecord( collname, jsonrec);
}

// Read record by rid to JSON representation of a single document
bool TArangoDBClient::readRecord( const std::string& collname, const std::string& id, std::string& jsonrec )
{
    return pdata->ReadRecord( collname, id, jsonrec );
}

// Updates an existing document described by the selector, which must be an object containing the _id or _key attribute.
std::string TArangoDBClient::updateRecord( const std::string& collname, const std::string& id, const std::string& jsonrec )
{
    return pdata->UpdateRecord( collname, id, jsonrec );
}


// Removes a document described by the selector, which must be an object containing the _id or _key attribute.
bool TArangoDBClient::deleteRecord(const std::string& collname, const std::string& id )
{
    return pdata->DeleteRecord(collname, id );
}

// Read record header
bool TArangoDBClient::existsRecord( const std::string& collname, const std::string& id )
{
    return pdata->ExistsRecord( collname, id );
}

// Retrive one record from the collection
bool TArangoDBClient::loadRecord( const std::string& collname, KeysSet::iterator& itr, std::string& jsonrec )
{
    std::string rid = getServerKey( itr->second.get() );
    return readRecord( collname, rid, jsonrec );
}

// Removes record from the collection
bool TArangoDBClient::removeRecord( const std::string& collname,KeysSet::iterator& itr  )
{
    jsonioErrIf( connectData.readonlyDBAccess(), collname, "Trying to remove document into read only mode.");
    std::string rid = getServerKey( itr->second.get() );
    return deleteRecord(collname, rid );
}

// Save/update record in the collection
std::string TArangoDBClient::saveRecord( const std::string& collname, KeysSet::iterator& itr, const std::string& jsonrec )
{
  jsonioErrIf( connectData.readonlyDBAccess(), collname, "Trying to create/update document into read only mode.");
  std::string newrid;
  if( itr->second.get() == nullptr ) // new record
    {
       newrid = createRecord( collname, jsonrec);
       if( !newrid.empty() )
         setServerKey( newrid, itr );
    }
    else
       {
          std::string rid = getServerKey( itr->second.get() );
          newrid = updateRecord( collname, rid, jsonrec );
        }
    return (newrid);
}

// Create collection if no exist
void TArangoDBClient::createCollection(const std::string& collname, const std::string& ctype)
{
    pdata->CreateCollection(collname, ctype );
}

// list collection names
std::set<std::string> TArangoDBClient::getCollectionNames( CollectionTypes ctype )
{
    return pdata->getCollectionNames( ctype );
}

// Run query
//  \param collname - collection name
//  \param query -  query  condition (where)
//  \param setfnc - function for set up readed data
void TArangoDBClient::selectQuery( const std::string& collname, const DBQueryData& query,  SetReadedFunction setfnc )
{
  if( query.getType() == DBQueryData::qEdgesFrom ||
      query.getType() == DBQueryData::qEdgesTo  ||
          query.getType() == DBQueryData::qEdgesAll    )
        pdata->SearchEdgesToFrom( collname,  query, setfnc );
  else
        pdata->SelectQuery( collname,  query, setfnc );
}

// Test if we have complex field path
bool TArangoDBClient::isComlexFields( const std::set<std::string>& queryFields)
{
  for( auto fld: queryFields )
    if( fld.find(".") !=  std::string::npos )
       return true;
  return false;
}

void TArangoDBClient::allQuery( const std::string& collname, const std::set<std::string>& queryFields,
    SetReadedFunctionKey setfnc )
{
    pdata->AllQueryFields( isComlexFields(queryFields), collname, queryFields, setfnc );
//    pdata->AllQuery( collname, setfnc );
}


void TArangoDBClient::deleteEdges(const std::string& collname, const std::string& vertexid )
{
    jsonioErrIf( connectData.readonlyDBAccess(), collname, "Trying to remove edges into read only mode.");
    pdata->RemoveEdges( collname, vertexid );
}

void TArangoDBClient::fpathCollect( const std::string& collname, const std::string& fpath,
                                    std::vector<std::string>& values )
{
   pdata->CollectQuery( collname, fpath, values);
}

void TArangoDBClient::lookupByIds( const std::string& collname,  const std::vector<std::string>& ids,
                                   SetReadedFunction setfnc )
{
  // we can use ids and keys
   pdata->LookupByKeys( collname, ids, setfnc);
}

void TArangoDBClient::removeByIds( const std::string& collname,  const std::vector<std::string>& ids )
{
   jsonioErrIf( connectData.readonlyDBAccess(), collname, "Trying to remove documents into read only mode.");
   pdata->RemoveByKeys( collname, ids );
}



// TArangoDBRootClient ------------------------------------------

/// Default Constructor
TArangoDBRootClient::TArangoDBRootClient()
{
  rootData.getFromSettings(ioSettings().defaultArangoDB(), true );
  resetDBConnection( rootData );
}

TArangoDBRootClient::~TArangoDBRootClient()
{ }

void TArangoDBRootClient::resetDBConnection( const ArangoDBConnect& aconnectData )
{
    rootData =aconnectData;
    pusers.reset( new arangodb::ArangoDBUsersAPI(rootData ) ); /// here must be root data
}

std::set<std::string> TArangoDBRootClient::getDatabaseNames()
{
  return pusers->getDatabaseNames();
}

void TArangoDBRootClient::CreateDatabase( const std::string& dbname, const std::vector<ArangoDBUser>& users )
{
   jsonioErrIf( rootData.readonlyDBAccess(), dbname, "Trying create database into read only mode.");
   pusers->CreateDatabase( dbname, users );
}

bool TArangoDBRootClient::ExistDatabase(const std::string &dbname)
{
   return pusers->ExistDatabase( dbname );
}

void TArangoDBRootClient::CreateUser( const ArangoDBUser& userdata )
{
   jsonioErrIf( rootData.readonlyDBAccess(), userdata.name, "Trying create user into read only mode.");
   return pusers->CreateUser( userdata );
}

std::map<std::string,std::string> TArangoDBRootClient::getDatabaseNames( const std::string&  user )
{
   return pusers->getDatabaseNames( user );
}

std::set<std::string> TArangoDBRootClient::getUserNames()
{
   return pusers->getUserNames();
}


} // namespace jsonio
