//  This is JSONIO library+API (https://bitbucket.org/gems4/jsonio)
//
/// \file dbconnect.cpp
/// Implementation of class TDataBase to managing database connection
//
// JSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2018 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include "jsonio/dbconnect.h"
#include "jsonio/dbarango.h"

namespace jsonio {

//  Constructor used internal ArangoDB driver
TDataBase::TDataBase()
{
    // Default Constructor - extract data from settings
    std::shared_ptr<TAbstractDBDriver> dbDriver( new TArangoDBClient() );
    updateDriver( dbDriver );
}

TDataBase::~TDataBase()
{ }

void TDataBase::updateDriver( std::shared_ptr<TAbstractDBDriver> dbDriver )
{
    _driver = dbDriver;

    for( auto coll:  _collections )
        coll.second->changeDriver( _driver.get() );
}

TDBCollection *TDataBase::addCollection( const std::string& type, const std::string& colname  ) const
{
    TDBCollection *col = new TDBCollection( this, colname );
    col->setCollectionType( type );
    col->Load();
    _collections[colname] = std::shared_ptr<TDBCollection>(col);
    return col;
}


} // namespace jsonio
