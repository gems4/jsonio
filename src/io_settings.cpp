//  This is JSONIO library+API (https://bitbucket.org/gems4/jsonio)
//
/// \file io_settings.cpp
/// Implementation of JsonioSettings object for storing preferences to JSONIO
//
// JSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2017 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//


#include <iostream>
#include <cstring>
#include "jsonio/io_settings.h"
#include "jsonio/dbarango.h"

#ifndef _MSC_VER
#include <pwd.h>
#include <unistd.h>
#else
#include  <io.h>
#endif

#ifdef __APPLE__
#include <filesystem>
namespace fs = std::filesystem;
//#include "boost/filesystem.hpp"
//namespace fs = boost::filesystem;
#else
#include <experimental/filesystem>
namespace fs = std::experimental::filesystem;
#endif

namespace jsonio {

// Get home directory in Linux, C++
// https://stackoverflow.com/questions/3020187/getting-home-directory-in-mac-os-x-using-c-language
// This should work under Linux, Unix and OS X, for Windows you need to make a slight modification.
std::string  getHomeDir()
{
    const char *homeDir;
#ifdef _MSC_VER
    //char homedir[MAX_PATH];
    //snprintf(homedir, MAX_PATH, "%s%s", getenv("HOMEDRIVE"), getenv("HOMEPATH"));
    homeDir =  getenv("HOMEDRIVE");
#else
    homeDir = getenv("HOME");

    if( !homeDir )
    {
       struct passwd* pwd = getpwuid(getuid());
        if (pwd)
            homeDir = pwd->pw_dir;
    }
#endif
    jsonioErrIf( !homeDir, "error:",  "HOME environment variable not set.");
    //std::cout << "Home directory is " << homeDir << std::endl;
    return std::string(homeDir);
}

//  "~" generally refers to the user's home directory, this is solely an artifact of tilde expansion in Unix shells.
std::string expandHomeDir( const std::string& inPath, const std::string& homeDir )
{
    static std::string  aHomeDir(getHomeDir());

    if( inPath.size () > 0 && inPath[0] == '~')
    {
        auto newpath = ( homeDir.empty() ? aHomeDir: homeDir);
        newpath += inPath.substr(1);
        return newpath;
    }
    else
        return inPath;
}

std::string JsonioSettings::settingsFileName = "jsonuio-config.json";

JsonioSettings& ioSettings()
{
  static  JsonioSettings data( JsonioSettings::settingsFileName );
  return  data;
}

void GroupSettings::sync() const
{
  _iosettins.sync();
}

std::string GroupSettings::directoryPath( const std::string& fldpath, const std::string& defvalue  ) const
{
    std::string dirPath = value(fldpath, defvalue );
    return expandHomeDir( dirPath, _iosettins.homeDir() );
}


//------------------------------------------------------------

JsonioSettings::JsonioSettings( const std::string& inifile ):
    _file( inifile ), _mainSettings(JsonDomFree::newObject()),
    _topGroup(group("")),_jsonioGroup(group(""))
{
    getDataFromPreferences();
}

void JsonioSettings::getDataFromPreferences()
{
   if( _file.Exist() )
       _file.LoadJson( _mainSettings.get() );
   _jsonioGroup = group( groupName );
   HomeDir = value( "common.UserHomeDirectoryPath", std::string("") );
   HomeDir = expandHomeDir( HomeDir, "" ); // "~" or empty generally refers to the user's home directory
   UserDir = directoryPath( "common.WorkDirectoryPath", std::string(".") );
   SchemDir =  "";
   updateSchemaDir();
}


// Connect localDB to new path
bool JsonioSettings::updateSchemaDir()
{
  std::string newSchema = directoryPath("common.SchemasDirectory", std::string(""));
  if( newSchema != SchemDir )
  {
    SchemDir  = newSchema;
    readSchemaDir( SchemDir );
    return true;
  }
  return false;
}

void JsonioSettings::setUserDir( const std::string& dirPath)
{
  if( dirPath.empty() )
          return;
  setValue( "common.WorkDirectoryPath", dirPath );
  UserDir = directoryPath( "common.WorkDirectoryPath", std::string(".") );
}

void JsonioSettings::setHomeDir( const std::string& dirPath )
{
  setValue( "common.UserHomeDirectoryPath", dirPath );
  HomeDir = value( "common.UserHomeDirectoryPath", std::string("") );
}

// Read all thrift schemas from Dir
void JsonioSettings::readSchemaDir( const std::string& dirPath )
{
#ifdef _MSC_VER
    if( dirPath.empty() || _access( dirPath.c_str(), 0 ))
           return;
#else
   if( dirPath.empty() || access( dirPath.c_str(), 0 ))
          return;
#endif

   fs::path p (dirPath);
   fs::directory_iterator end_itr;
   std::vector<std::string> fileNames;

    for(fs::directory_iterator itr(p); itr != end_itr; ++itr)
    {
      if (is_regular_file(itr->path()))
      {
        std::string file = itr->path().string();
        //cout << "file = " << file << endl;
        if ( file.find(".schema.json") != std::string::npos)
            fileNames.push_back(file);
      }
    }

   schema.clearAll();
   for (auto file: fileNames)
      schema.addSchemaFile( file.c_str() );
}

} // namespace jsonio
