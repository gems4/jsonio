//  This is JSONIO library+API (https://bitbucket.org/gems4/jsonio)
//
/// \file dbgraph.cpp
/// Implementation of classes TGraphAbstract - working with graph databases (OLTP)
/// TGraphEJDB - working with internal EJDB data base in graph mode
/// Used  SchemaNode class - API for direct code access to internal
/// DOM based on our JSON schemas.
//
// JSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

/* query all connected edges
FOR v, e, p IN 1..1 ANY 'reactions/H2_15_0'
 inherits, takes, defines, master, product, prodreac, basis, pulls, involves, adds, yields
RETURN e

*/

#include <iostream>
#include "jsonio/dbedgedoc.h"
#include "jsonio/io_settings.h"
using namespace std;

namespace jsonio {

TDBEdgeDocument* TDBEdgeDocument::newEdgeDocument( const TDataBase* dbconnect, const std::string& schemaName )
{
    if( schemaName.empty()  )
        return nullptr;

    TDBEdgeDocument* newDov = new TDBEdgeDocument( schemaName, dbconnect );
    return newDov;
}


TDBEdgeDocument* TDBEdgeDocument::newEdgeDocumentQuery( const TDataBase* dbconnect,
                                 const string& schemaName, const DBQueryData& query  )
{
    if( schemaName.empty()  )
        return nullptr;

    TDBEdgeDocument* newDov = new TDBEdgeDocument( schemaName, dbconnect );

    // internal selection
    newDov->SetQuery(query);
    return newDov;
}


TDBEdgeDocument* documentAllEdges( const TDataBase* dbconnect )
{
    string schemaName = ioSettings().Schema()->getEdgesList()[0];
    TDBEdgeDocument* edgeDoc = new TDBEdgeDocument( schemaName, dbconnect  );
    edgeDoc->resetMode(true);
    return edgeDoc;
}


TDBEdgeDocument::TDBEdgeDocument( const string& aschemaName, TDBCollection* collection  ):
    TDBVertexDocument( aschemaName, collection )
{
    resetSchema( aschemaName, true );
}

TDBEdgeDocument::TDBEdgeDocument( const string& aschemaName, const TDataBase* dbconnect ):
    TDBEdgeDocument( aschemaName,  dbconnect, "edge", collectionNameFromSchema( aschemaName ) )
{
    resetSchema( aschemaName, true );
}


// Constructor from bson data
TDBEdgeDocument::TDBEdgeDocument( TDBCollection* collection, const JsonDom *object ):
    TDBVertexDocument( collection, object )
{
    resetSchema( _schemaName, true );
}

DBQueryData TDBEdgeDocument::makeDefaultQueryTemplate() const
{
    string queryJson =  "{'_label': '";
    queryJson += _label;
    queryJson += "' }";

    return DBQueryData(queryJson, DBQueryData::qTemplate );
}


// Define new Edge
void TDBEdgeDocument::setEdgeObject( const string& aschemaName, const string& outV,
                               const string& inV, const FieldSetMap& fldvalues )
{
    // check schema
    if( _schemaName != aschemaName )
    {
        if( _changeSchemaMode )
            resetSchema( aschemaName, false );
        else
            jsonioErr("TGRDB0007", "Illegal record schame: " + aschemaName + " current schema: " + _schemaName );
    }

    _currentData->clearField(); // set default values
    setValue("_to", inV);
    setValue("_from", outV);
    for(auto const &ent : fldvalues)
        setValue( ent.first, ent.second  );
}


} // namespace jsonio
