#include "jsonio/jsondomfree.h"
#include "jsonio/tf_json.h"
using namespace std;

namespace jsonio {


shared_ptr<JsonDomFree> unpackJson( const string& jsondata )
{
    shared_ptr<JsonDomFree> domdata(JsonDomFree::newObject());
    parseJsonToNode( jsondata, domdata.get());
    return domdata;
}

string getJsonTypeName( int bsonType )
{
    string btype = "Null";
    switch ( bsonType )
        {
          // impotant datatypes
          case JSON_NULL:  btype = "Null";
                           break;
          case JSON_BOOL:  btype = "Bool";
                           break;
          case JSON_INT:   btype = "Int";
                           break;
          case JSON_DOUBLE: btype = "Double";
                            break;
          case JSON_STRING: btype = "String";
                      break;
          // main constructions
          case JSON_OBJECT: btype = "Object";
                            break;
          case JSON_ARRAY:  btype = "Array";
                            break;
          default:          break;
        }
        return btype;
}

string fixedDefValue( int btype, const string& defval )
{
    string value = defval;
    switch ( btype )
    {
      // impotant datatypes
      case JSON_NULL:  value = "null";
                       break;
      case JSON_BOOL:  if( value != "true" )
                          value = "false";
                       break;
      case JSON_INT:   if( value.empty() )
                         value = "0";
                       else
                       { int ival;
                        if( is<int>( ival, value.c_str()) )
                             value = to_string( ival );
                        else
                             value = "0";
                       }
                       break;
      case JSON_DOUBLE: if( value.empty() )
                           value = "0.0";
                      else
                        { double dval;
                          if( is<double>( dval, value.c_str()) )
                             value = TArray2Base::value2string( dval );
                          else
                             value = "0.0";
                          }
                       break;
      case JSON_STRING: if( value.empty() )
                           value = "";//emptiness;
                        break;
      // main constructions
      case JSON_OBJECT:
      case JSON_ARRAY:
      default:          value = "";
                        break;
    }
    return value;
}


//----------------------------------------------------------------------

// Get field by fieldpath ("name1.name2.name3")
JsonDomFree *JsonDomFree::field( const string& fieldpath) const
{
   queue<string> names = split(fieldpath, ".");
   return field(names);
}

// Get field by fieldpath
JsonDomFree *JsonDomFree::field( queue<string> names ) const
{
  if( names.empty() )
    return const_cast<JsonDomFree *>(this);

  string fname = names.front();
  names.pop();

  for(std::size_t ii=0; ii< getChildrenCount(); ii++ )
   if( getChild(ii)->getKey() == fname )
       return getChild(ii)->field( names );

  return nullptr;  // not found
}

// Remove current field from json
bool JsonDomFree::removeField()
{
  if( _parent == nullptr )
    return false;

  int thisndx = -1;
  for(std::size_t ii=0; ii< _parent->getChildrenCount(); ii++ )
  {
     if( _parent->_children[ii].get() == this )
        thisndx = static_cast<int>(ii);
     if( thisndx >= 0 )
       _parent->_children[ii]->_ndx--;
  }
  if( thisndx >= 0 )
  {   _parent->_children.erase(_parent->_children.begin() + thisndx);
      return true;
  }
  return false;
}

// Set up string json value to array or struct field
void JsonDomFree::setComplexValue(const string& newval )
{
    clearField();
    if( !newval.empty() )
       parseJsonToNode( newval, this );
}

// Resize top level Array
void JsonDomFree::resizeArray( const vector<std::size_t>& sizes, const string& defval )
{
  if( !isArray()  )
     jsonioErr("resizeArray" , _fldKey+" is not a top level array" );
  resizeArray( 0, sizes, defval  );
}

void JsonDomFree::resizeArray( std::size_t  size, const string& defval  )
{
    if( !isArray() )
        jsonioErr("resizeArray" , _fldKey+" is not a array" );

    // resizeline item
    if( size == _children.size() )
       ;
    else // delete if smaler
      if( size < _children.size() )
       _children.resize(size);
      else
      {
        if( _children.size()<1)
        {
          for( uint ii=0; ii<size; ii++)
              appendScalar( to_string(ii), defval );
        } else
          {
             string value = defval;
             int type = _children[0]->getType();
             if( value.empty() )
                 value = _children[0]->toString();
             for( auto ii=_children.size(); ii<size; ii++)
                  appendNode( to_string(ii), type, value );
         }
      }
}


void JsonDomFree::resizeArray(uint level, const vector<std::size_t>& sizes, const string& defval  )
{
    if( sizes.size() <= level )
      return;
    auto size = sizes[level];

    // resize current level
    resizeArray( size, defval  );

    // try to resize next level
    for( uint ii=0; ii< _children.size(); ii++)
      _children[ii]->resizeArray( level+1, sizes, defval );
}


bool JsonDomFree::updateFieldPath( const std::string& fieldpath, const std::string& newValue)
{
    queue<string> names = split(fieldpath, ".");
    return updateFieldPath(names, newValue);
}

bool JsonDomFree::updateFieldPath( std::queue<std::string> names, const std::string& newValue)
{
    if( names.empty() )
      return updateField(newValue);

    if( !isStruct() && !isArray() )
      return false;

    string fname = names.front();
    names.pop();

    for(std::size_t ii=0; ii< getChildrenCount(); ii++ )
     if( getChild(ii)->getKey() == fname )
         return getChild(ii)->updateFieldPath( names, newValue );

    // not found
    if( !names.empty() )
    {
      JsonDomFree* newobj = dynamic_cast<JsonDomFree*>(appendObject(fname));
      return newobj->updateFieldPath( names, newValue );
    }
    appendScalar(fname, newValue);
    return true;
}

JsonDomFree *JsonDomFree::appendObjectFieldPath( const std::string& fieldpath)
{
    queue<string> names = split(fieldpath, ".");
    return appendObjectFieldPath(names);
}


JsonDomFree *JsonDomFree::appendObjectFieldPath( std::queue<std::string> names )
{
    if( names.empty() )
      return const_cast<JsonDomFree *>(this);

    if( !isStruct() && !isArray() )
      return nullptr;

    string fname = names.front();
    names.pop();

    for(std::size_t ii=0; ii< getChildrenCount(); ii++ )
     if( getChild(ii)->getKey() == fname )
         return getChild(ii)->appendObjectFieldPath( names );

    // not found
    if( !names.empty() )
    {
      JsonDomFree* newobj = dynamic_cast<JsonDomFree*>(appendObject(fname));
      return newobj->appendObjectFieldPath( names );
    }
    return dynamic_cast<JsonDomFree*>(appendObject( fname ));
}


// list of defined fields
vector<string> JsonDomFree::getUsedKeys() const
{
    vector<string> usekeys;
    for(uint ii=0; ii< _children.size(); ii++ )
         usekeys.push_back(_children[ii]->getKey());
    return usekeys;
}

} // namespace jsonio
