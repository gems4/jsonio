//  This is JSONIO library+API (https://bitbucket.org/gems4/jsonio)
//
/// \file dbquerydef.cpp
/// Implementation of QueryLine, DBQueryData, DBQueryDef and DBQueryResult
/// classes to work with data base queries
//
// JSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#include "jsonio/dbquerydef.h"
#include "jsonio/io_settings.h"
#include "jsonio/jsondomschema.h"
#include "jsonio/jsondomfree.h"
#include "jsonio/tf_json.h"
using namespace std;

namespace jsonio {

DBQueryData emptyQuery(DBQueryData::qUndef);

bool operator !=( const DBQueryData& iEl,  const DBQueryData& iEr)
{
    return (iEl._type != iEr._type) || (iEl._findCondition != iEr._findCondition);
}

// set data to bson
void DBQueryData::toJsonNode( JsonDom *object ) const
{
   object->appendInt( "style", _type );
   object->appendString(  "find", _findCondition  );
   if( !_bindVars.empty() )
     object->appendString(  "bind", _bindVars  );
   if( !_queryFields.empty() )
   {
     auto oflds = object->appendObject("fields");
     for( auto fld: _queryFields )
         oflds->appendString(  fld.first, fld.second  );
   }
}

void DBQueryData::fromJsonNode( const JsonDom *object )
{
   if(!object->findValue(  "style", _type ) )
             _type = qUndef;
   // read condition
   if(!object->findValue(  "find", _findCondition ))
           _findCondition = "";
   if(!object->findValue(  "bind", _bindVars ))
           _bindVars = "";
    object->findObject( "fields", _queryFields );
}

//RETURN { "_label": u._label, "properties.symbol": u.properties.symbol, "properties.sourcetdb":u.properties.sourcetdb,
// "properties.name": u.properties.name, "properties.formula":u.properties.formula }
string DBQueryData::generateReturn( bool isDistinct, const QueryFields& mapFields, const std::string& collvalue )
{
  string retdata = "\nRETURN ";
  if( isDistinct )
     retdata += "DISTINCT ";
  if( mapFields.empty() )
     retdata += collvalue + " ";
  else
  {
      retdata += " { ";
      string fldslst = "";
      for( auto fld: mapFields )
      {
         if( !fldslst.empty() )
            fldslst += ", ";
         fldslst += " \"" + fld.first + "\" : " + collvalue + "." + fld.second;
      }
      retdata += fldslst+ " } ";
  }
  return   retdata;
}

/// Generate AQL FILTER with predefined fields
std::string DBQueryData::generateFILTER(  const FieldSetMap& fldvalues,
                    bool asTemplate, const std::string& collvalue )
{
   string generated;
   if( asTemplate )
   {
       for(auto& ent : fldvalues)
       {
          if( !generated.empty() )
                 generated += ", ";
          generated += " \"" + ent.first +"\" : " + ent.second+ " ";
       }
   }else
      {
         for(auto& ent : fldvalues)
         {
            if( !generated.empty() )
                 generated += " && ";
            generated += collvalue + "." + ent.first +" == " + ent.second;
         }
        generated = "\nFILTER " + generated +" ";
      }

   return generated;
}


// set data to bson
void DBQueryDef::toJsonNode( JsonDom *object ) const
{
   object->clearField();
   object->appendString( "name", keyName );
   object->appendString( "comment", comment );

   auto condq = object->appendObject("condition");
   condition.toJsonNode( condq );

   auto arr = object->appendArray("collect");
   for(uint ii=0; ii<fieldsCollect.size(); ii++)
        arr->appendString( to_string(ii), fieldsCollect[ii] );

   object->appendString( "qschema", toschema );
}

void DBQueryDef::fromJsonNode( const JsonDom *object )
{
   if(!object->findValue(  "name", keyName ) )
             keyName = "undefined";
   if(!object->findValue(  "comment", comment ) )
             comment = "";
   if(!object->findValue(  "qschema", toschema ) )
             toschema = "";

   // read condition
   auto condq = object->field("condition");
   if( condq != nullptr )
     condition.fromJsonNode( condq );
   else
     condition.fromJsonNode( object ); // old records

   fieldsCollect.clear();
   auto arr  = object->field( "collect"  );
   for( std::size_t ii=0; ii<arr->getChildrenCount(); ii++ )
   {
       auto it = arr->getChild( ii );
       jsonioErrIf( it->getType() != JSON_STRING, "collect", "Error file format..." );
       fieldsCollect.push_back( it->toString());
   }
}


//---------------------------------------------------------------

// Make line to view table
void DBQueryResult::nodeToValues( const JsonDom* node, vector<string>& values ) const
{
   values.clear();
   string kbuf;
   JsonDom *data;

   for(uint ii=0; ii< query.getFieldsCollect().size(); ii++ )
   {
       data = node->field(  query.getFieldsCollect()[ii] );
       if( data == nullptr || !data->getValue( kbuf ) )
            kbuf = "---";
       strip( kbuf );
       values.push_back(kbuf);
   }
}

// Add line to view table
void DBQueryResult::addLine( const string& keyStr, const JsonDom* nodedata, bool isupdate )
{
   vector<string> values;
   nodeToValues( nodedata, values );

   if( isupdate)
   { auto it =  queryValues.find(keyStr);
     if( it != queryValues.end() )
     {   it->second = values;
         return;
     }
   }

   queryValues.insert(pair<string,vector<string>>(keyStr, values ));
}

void DBQueryResult::updateLine( const string& keyStr, const JsonDom* nodedata )
{
   vector<string> values;
   nodeToValues( nodedata, values );

   auto it =  queryValues.find(keyStr);
     if( it != queryValues.end() )
        it->second = values;
}


// Delete line from view table
void DBQueryResult::deleteLine( const string& keyStr )
{
  auto it =  queryValues.find(keyStr);
    if( it != queryValues.end() )
      queryValues.erase(it);
}


std::size_t DBQueryResult::getKeyValueList( vector<string>& aKeyList,
       vector<vector<string>>& aValList ) const
{
    aKeyList.clear();
    aValList.clear();

    auto it = queryValues.begin();
    while( it != queryValues.end() )
    {
        aKeyList.push_back( it->first );
        aValList.push_back( it->second );
        it++;
    }
    return aKeyList.size();
}

/// Extract first key from data
std::string DBQueryResult::getFirstKeyFromList() const
{
    auto it = queryValues.begin();
    if( it != queryValues.end() )
       return it->first;
    return "";  // empty table
}

std::size_t DBQueryResult::getKeyValueList( vector<string>& aKeyList,  vector<vector<string>>& aValList,
                              const char* keypart, CompareTemplateFunction compareTemplate ) const
{
    aKeyList.clear();
    aValList.clear();

    auto it = queryValues.begin();
    while( it != queryValues.end() )
    {
        string key =it->first;
        if( compareTemplate(keypart, key ) )
        {
          aKeyList.push_back( key );
          aValList.push_back( it->second );
        }
        it++;
    }
    return aKeyList.size();
}


// Get keys list for current query and defined field values
std::size_t DBQueryResult::getKeyValueList( std::vector<std::string>& aKeyList, std::vector<std::vector<std::string>>& aValList,
                                    const std::vector<std::string>& fieldnames, const std::vector<std::string>& fieldvalues ) const
{
    aKeyList.clear();
    aValList.clear();

    uint ii;
    std::vector<uint> fieldindexes;

    for( auto fname: fieldnames )
    {
        for( ii=0; ii< query.getFieldsCollect().size(); ii++ )
        {
            if( fname == query.getFieldsCollect()[ii])
            {
                fieldindexes.push_back(ii);
                break;
            }
        }
        if( ii == query.getFieldsCollect().size() )
            return 0;  // no field
    }

    auto it = queryValues.begin();
    while( it != queryValues.end() )
    {
        bool getdata=true;

        for( ii=0; ii<fieldindexes.size(); ii++ )
        {
            if( it->second[fieldindexes[ii]] != fieldvalues[ii] )
            {
                getdata = false;
                break;
            }
        }

        if( getdata )
        {
            aKeyList.push_back( it->first );
            aValList.push_back( it->second );
        }
        it++;
    }

    return aKeyList.size();
}

string DBQueryResult::getKeyFromValue( const JsonDom* node ) const
{
    uint ii;
    vector<string> value;
    nodeToValues( node, value );
    auto it = queryValues.begin();
    while( it != queryValues.end() )
    {
        for( ii=0; ii< query.getFieldsCollect().size(); ii++ )
        {
           if( it->second[ii] != value[ii] &&
               query.getFieldsCollect()[ii] != "_id" &&
               query.getFieldsCollect()[ii] != "_key" &&
               query.getFieldsCollect()[ii] != "_rev" )
             break;
        }
        if( ii >= query.getFieldsCollect().size() )
          return it->first;
        //if( it->second == value )
        //  return it->first;
       it++;
    }
    return "";
}


} // namespace jsonio


