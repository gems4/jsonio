#include <iostream>
#ifdef _MSC_VER
#include  <io.h>
#else
#include <unistd.h>
#endif
#include "jsonio/json2file.h"
#include "jsonio/tf_json.h"
using namespace std;

namespace jsonio {

// File class -----------------------------------------

TFileBase::TFileBase(const string& fName, const string& fExt, const string& fDir):
    dir_(fDir), name_(fName), ext_(fExt), isopened_( false )
{
    makePath();
    makeKeyword();
}

TFileBase::TFileBase(const string& path):
         path_(path), isopened_( false )
{
    u_splitpath(path_, dir_, name_, ext_);
    makeKeyword();
}

// Constructor by reading from configurator file
TFileBase::TFileBase( const JsonDom* object):
        isopened_( false )
{
    fromJsonDom(object);
}

// Writes DOD data to ostream file
void TFileBase::toJsonDom( JsonDom *object ) const
{

    object->appendString( "Path",  path_ );
    object->appendString( "Keywd", keywd_ );
    object->appendString( "Type",  to_string(type_) );
}

// Reads DOD data from istream file
void TFileBase::fromJsonDom( const JsonDom* object )
{
   if( !object->findValue( "Path", path_ ) )
        jsonioErr( "E011BSon: ", "Undefined File Path.");
    u_splitpath(path_, dir_, name_, ext_);
    if( !object->findValue( "Keywd", keywd_ ) )
       makeKeyword();
    string sbuf;
    if( !object->findValue( "Type", sbuf ) )
        sbuf="t";
    type_=sbuf[0];
}

// For all files first simbols for name
void TFileBase::makeKeyword()
{
    if( name_.empty() )
        return;
     keywd_ = name_;
}

/// Check for existence of file
bool TFileBase::Exist()
{
    if( testOpen() )
        return true;  // file alredy open
#ifdef _MSC_VER
    if( _access( path_.c_str(), 0 ))
        return false;
#else
    if( access( path_.c_str(), 0 ) )
        return false;
#endif
    else  return true;
}

// FJson --------------------------------------------------------

/// Load data from json file to bson object
void FJson::loadJson( JsonDom* object )
{
    fstream fin(path_, ios::in );
    jsonioErrIf( !fin.good() , path_, "Fileopen error...");
    parseJsonToNode( fin, object );
}

/// Save data from bson object to  json file
void FJson::saveJson( const JsonDom* object ) const
{
    fstream fout(path_, ios::out);
    jsonioErrIf( !fout.good() , path_, "Fileopen error...");
    printNodeToJson( fout, object );
}

string FJson::LoadtoJsonString()
{
    string jsonstr;
    shared_ptr<JsonDomFree> domdata(JsonDomFree::newObject());
    LoadJson( domdata.get() );
    printNodeToJson( jsonstr, domdata.get() );
    return jsonstr;
}

void FJson::LoadJson( JsonDom* object )
{
    if( type_ == FileTypes::Json_ )
         loadJson( object );
}

void FJson::SaveJson( const JsonDom* object ) const
{
     saveJson(  object );
}

//-------------------------------------------------------------------------


// Open file to import
void FJsonArray::Open(int amode )
{
  mode_ = amode;
  arrObjects = shared_ptr<JsonDomFree>(JsonDomFree::newArray());
  loadedndx_ = 0;

  if( mode_ == OpenModeTypes::ReadOnly )
  {
    if( type_ == FileTypes::Json_ )
    {
        // open  file for other types
        fstream _stream(path_,  std::fstream::in );
        jsonioErrIf( !_stream.good() , path_, "Fileopen error...");
        if( !parseJsonToNode( _stream, arrObjects.get() ))
           return;
    }
  }
  else
      if( mode_ != OpenModeTypes::WriteOnly )
        jsonioErr( path_, "Illegal file mode...");

  isopened_ = true;
}

// Close internal file
void FJsonArray::Close()
{
    if( mode_ == OpenModeTypes::WriteOnly )
    {
        fstream   _stream(path_,  std::fstream::out );
        jsonioErrIf( !_stream.good() , path_, "Output fileopen error...");
        if(  type_ == FileTypes::Json_ )
           printNodeToJson( _stream, arrObjects.get()  ) ;
    }
    isopened_ = false;
}

// Read next json object
JsonDom*  FJsonArray::LoadNext()
{
  if( loadedndx_>=arrObjects->getChildrenCount() )
    return nullptr;
  return arrObjects->getChild(loadedndx_++);
}

// Read next bson object
void FJsonArray::SaveNext( const JsonDom* object )
{
   string tojson;
   printNodeToJson( tojson, object );
   SaveNext( tojson );
}

/// Load next object data from file to json string
bool FJsonArray::LoadNext( string& strjson )
{
  auto obj = FJsonArray::LoadNext();
  if( obj == nullptr )
    return false;

  printNodeToJson( strjson, obj );
  return true;
}

/// Save next json string to file
void FJsonArray::SaveNext( const string& strjson )
{
    auto obj = arrObjects->appendObject(to_string(loadedndx_));
    parseJsonToNode( strjson, obj );
    loadedndx_++;
}


} // namespace jsonio
