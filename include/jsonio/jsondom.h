#ifndef JSON_DOM_H
#define JSON_DOM_H

#include <iostream>
#include <iomanip>
#include "jsonio/ar2base.h"

namespace jsonio {

enum JsonType {
    JSON_UNDEFINED = -1,
    JSON_NULL = 1,
    JSON_BOOL = 2,    // T_BOOL
    JSON_INT = 8,     // T_I32
    JSON_DOUBLE = 4,  // T_DOUBLE
    JSON_STRING = 11, // T_STRING
    JSON_OBJECT = 12, // T_STRUCT
    JSON_ARRAY = 15   // T_LIST
};

/// Get string name of json type
std::string getJsonTypeName( int jsonType );

/// Determine the default value corresponding to the json internal type
std::string fixedDefValue( int jsonType, const std::string& defval );

/// \class JsonDom represents an abstract item in a tree view.
/// Item data defines one json object
class JsonDom
{

protected:

    explicit JsonDom() {}

    virtual bool testKey( const std::string& akey ) = 0;

    virtual bool updateField( const std::string& newValue) = 0;

public:

    virtual ~JsonDom()
    { }

    // Get methods  --------------------------

    virtual const std::string& getKey() const = 0;

    virtual int getType() const = 0;

    virtual int getNdx() const = 0;

    virtual const std::string& getFieldValue() const = 0;

    virtual std::size_t getChildrenCount() const = 0;

    virtual JsonDom* getChild( std::size_t ndx ) const = 0;

    virtual JsonDom* getParent() const = 0;

    /// Get key name from Node
    virtual std::string getHelpName() const
    {
        auto item = this;
        while( item->getParent() != nullptr && item->getParent()->isArray() )
          item = item->getParent();

        return item->getKey();
    }

    /// Get Description from Node
    virtual std::string getDescription() const
    {
        return getHelpName();
    }

    virtual std::vector<std::string> getUsedKeys() const = 0;

    virtual std::string toString(bool dense = false) const;  // json string for Object and Array
    virtual double toDouble() const;
    virtual int    toInt() const;
    virtual bool   toBool() const;

    // Test methods  --------------------------------------------

     /// Test top Node
     virtual bool isTop() const = 0;

     /// Test Node as Struct
     virtual bool isStruct() const
     { return( getType() == JSON_OBJECT ); }

     /// Test Node as Array
     virtual bool isArray() const
     { return( getType() == JSON_ARRAY ); }

     /// Test Node as numeric value
     virtual bool isNumber( ) const
     { return( getType() == JSON_INT || getType() == JSON_DOUBLE || getType() == JSON_BOOL  ); }

     /// Test Node as Null
     virtual bool isNull() const
     { return( getType() == JSON_NULL ); }


    // Append functions --------------------------------------

    virtual JsonDom* appendNode( const std::string& akey, int atype, const std::string& avalue ) =0;

    JsonDom *appendObject( const std::string& akey )
    {
        return appendNode( akey, JSON_OBJECT, "" );
    }

    JsonDom *appendArray( const std::string& akey )
    {
        return appendNode( akey, JSON_ARRAY, "" );
    }

    bool appendNull( const std::string& akey )
    {
        return appendNode( akey, JSON_NULL, "null" );
    }

    bool appendBool( const std::string& key, bool value )
    {
        std::string strvalue =TArray2Base::value2string( value );
        return appendNode( key, JSON_BOOL, strvalue );
    }

    bool appendInt( const std::string& key, int value )
    {
        std::string strvalue =TArray2Base::value2string( value );
        return appendNode( key, JSON_INT, strvalue );
    }

    bool appendDouble( const std::string& key, double value )
    {
        std::string strvalue =TArray2Base::value2string( value );
        return appendNode( key, JSON_DOUBLE, strvalue );
    }

    bool appendString( const std::string& key, const std::string& value )
    {
        return appendNode( key, JSON_STRING, value );
    }

    /// Append obvious object - get type from string data
    bool appendScalar(const std::string& key, const std::string& value );


    template <class T>
    bool appendValue( const std::string& akey, T value )
    {
        std::string strvalue =TArray2Base::value2string( value );
        return appendNode( akey, JSON_DOUBLE, strvalue );
    }

    template < class T >
    void appendArray( const std::string& akey, const std::vector<T>& values )
    {
        auto arr = appendNode( akey, JSON_ARRAY, "" );
        if( arr != nullptr )
          for(uint ii=0; ii<values.size(); ii++)
             arr->appendValue( std::to_string(ii), values[ii]);

    }

    // Access functions  ------------------------

    /// Get field by fieldpath ("name1.name2.name3")
    virtual JsonDom *field(  const std::string& fieldpath ) const = 0;
    /// Get field by fieldpath
    virtual JsonDom *field( std::queue<std::string> names ) const = 0;

    /// Get Field Path from Node
    std::string getFieldPath() const;

    /// Get Value from Node
    /// If field is not number, the false will be returned.
    template <class T>
    bool getValue( T& value  ) const
    {
       switch( getType() )
       {
        case JSON_DOUBLE: value = toDouble(); break;
        case JSON_INT:    value = toInt();  break;
        case JSON_BOOL:   value = toBool();  break;
        default:     return false;
       }
       return true;
    }

    bool getValue( std::string& value  ) const
    {
       value = toString( true );
       return true;
    }

    bool getValue( bool& value  ) const
    {
       value = toBool();
       return true;
    }

    /// Get Value bhy path  from Node
    /// If field is not number, the false will be returned.
    template <class T>
    bool findValue( const std::string& keypath, T& value  ) const
    {
      auto child = field( keypath );
      if( child == nullptr )
         return false;
      return child->getValue(value);
    }

    /// Read array from bson structure by name to value vector
    template<class T>
    bool findArray( const std::string& keypath, std::vector<T>& lst ) const
    {
        T value;
        lst.clear();
        auto child = field( keypath );
        if( child == nullptr )
           return false;

        if( child->getType() == JSON_NULL  )  // empty array
          return true;
        if( child->getType() != JSON_ARRAY  )
          return false;

        for(std::size_t ii=0; ii< child->getChildrenCount(); ii++ )
        {
           if( !child->getChild(ii)->getValue(value) )
              value = T(0);
           lst.push_back(value);
        }
        return true;
     }

    /// Read array from dom structure by name to value vector
    template<class T>
    bool findToArray( const std::string& keypath, std::vector<T>& lst ) const
    {
        T value;
        lst.clear();
        auto child = field( keypath );
        if( child == nullptr )
           return false;

        if( child->getType() == JSON_NULL  )  // empty array
          return true;
        if( child->getType() == JSON_ARRAY ||  child->getType() == JSON_OBJECT )
        {  for(std::size_t ii=0; ii< child->getChildrenCount(); ii++ )
           {
             if( !child->getChild(ii)->getValue(value) )
                value = T(0);
             lst.push_back(value);
           }
        }
        else
        {
            if( !child->getValue(value) )
                value = T(0);
            lst.push_back(value);
        }
        return true;
     }


    /// Read map from bson structure by name to value map
    template <class T>
    bool findObject( const std::string& keypath, std::map<std::string,T>& newmap ) const
    {
        newmap.clear();
        T value;

        auto child = field( keypath );
        if( child == nullptr )
           return false;

        if( child->getType() == JSON_NULL  )  // empty map
          return true;
        if( child->getType() != JSON_OBJECT  )
          return false;

        for(std::size_t ii=0; ii< child->getChildrenCount(); ii++ )
        {
            auto childobj = child->getChild( ii);
            if( !childobj->getValue(value) )
              value = T(0);
           newmap[childobj->getKey()] = (value);
        }
        return true;
    }

    bool findKey( const std::string& keypath, std::string& tokey ) const
    {
       auto child = field( keypath );
       if( child == nullptr )
           return false;
       tokey = child->getFieldValue(); // internal string for object and array
       return true;
    }

    // Update functions  -----------------------------------

    /// Resize top level Array
    virtual void resizeArray( const std::vector<std::size_t>& sizes, const std::string& defval  = "" ) = 0;

    /// Clear Fields and set value to default
    virtual void clearField() =0;

    /// Remove current field from json
    virtual bool removeField() = 0;

    /// Set Value to current Node
    template <class T>
    bool setValue( const T& value  )
    {
        std::string strval  =  TArray2Base::value2string( value );
        return updateField( strval );
    }

    /// Set value to children Node
    template <class T>
    bool setFieldValue( const std::string& keypath, const T& value  )
    {
        auto child = field( keypath );

        if( child != nullptr )
            return child->setValue(value);

        return false;
    }

    virtual bool updateTypeTop( int newtype ) = 0;

    /// Set _id to Node
    virtual void setOid_( const std::string& _oid  );

};

template <> bool JsonDom::appendValue( const std::string& akey, int value );
template <> bool JsonDom::appendValue( const std::string& akey, std::string value );
template <> bool JsonDom::appendValue( const std::string& akey, const char* value );
template <> bool JsonDom::appendValue( const std::string& akey, bool value );


} // namespace jsonio

#endif // JSON_DOM_H
