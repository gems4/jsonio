//  This is JSONIO library+API (https://bitbucket.org/gems4/jsonio)
//
/// \file io_settings.h
/// Declaration of JsonioSettings object for storing preferences to JSONIO
//
// JSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2017 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef IOSETTINGS_H
#define IOSETTINGS_H

#include "jsonio/json2file.h"
#include "jsonio/thrift_schema.h"

namespace jsonio {

class JsonioSettings;

/// Get home directory in Linux, Unix and OS X, C++
std::string  getHomeDir();
///  Link path to home directory
///  "~" generally refers to the user's home directory.
std::string expandHomeDir( const std::string& inPath, const std::string& homeDir = "" );

const std::string groupName = "jsonio";

/// Definition of json based group settings
class GroupSettings
{

    friend class JsonioSettings;

protected:

    /// Settings data
    const JsonioSettings& _iosettins;

    /// Top object for group level
    JsonDomFree* _topData;

    void sync() const;

    GroupSettings( const JsonioSettings& iosettins,
                   JsonDomFree* topData ):
        _iosettins( iosettins ),_topData( topData )
    { }


public:

    GroupSettings( const GroupSettings& other ):
        _iosettins( other._iosettins ), _topData( other._topData )
    { }

    GroupSettings &operator=( const GroupSettings& other)
    {
        if (this != &other)
        {
            // _iosettins = other._iosettins;
            _topData = other._topData;
        }
        return *this;
    }

    /// Returns true if there exists a setting called key; returns false otherwise.
    bool contains(const std::string& keypath ) const
    {
        return( _topData->field( keypath ) != nullptr);
    }

    /// Return curent settins in json format std::string
    std::string getJson() const
    {
        return  _topData->toString();
    }

    /// Get Value from Node
    /// If field is not type T, the false will be returned.
    template <class T>
    T value( const std::string& fldpath, const T& defvalue  ) const
    {
        T avalue;
        if( !_topData->findValue( fldpath, avalue )  )
            avalue = defvalue;
        return avalue;
    }

    /// Get Value from Node
    /// If field is not type T, the false will be returned.
    std::string value( const std::string& fldpath, const char* defvalue  ) const
    {
        std::string avalue;
        if( !_topData->findValue( fldpath, avalue )  )
            avalue = defvalue;
        return avalue;
    }

    /// Get Directory path from Node
    /// "~" refers to the "UserHomeDirectoryPath" directory
    std::string directoryPath( const std::string& fldpath, const std::string& defvalue  ) const;

    /// Update/add value into settings
    template <class T>
    bool setValue( const std::string& fldpath, const T& value  )
    {
        std::string strval  =  TArray2Base::value2string( value );
        if( _topData->updateFieldPath( fldpath, strval ) )
        {
            sync();
            return true;
        }
        return false;
    }

    bool setValue( const std::string& fldpath, const char* value  )
    {
        return setValue( fldpath, std::string(value) );
    }
};


/// \class JsonioSettings - storing preferences to JSONIO
class JsonioSettings
{
    friend JsonioSettings& ioSettings();

    /// Constructor
    JsonioSettings( const std::string& inifile );

public:

    ~JsonioSettings()
    {
//        sync(); Temporarily disabled by DK on 3.07.19 to test abnormal behavior of GEMSW
    }

    /// Task settings file name
    static std::string settingsFileName;

    /// Get object group from settings
    GroupSettings group( const std::string& groupname )
    {
        JsonDomFree* topData = _mainSettings->appendObjectFieldPath(groupname);
        jsonioErrIf( topData == nullptr , groupname, "Illegal group name..." );
        return GroupSettings( *this, topData );
    }

    /// Returns true if there exists a setting called key; returns false otherwise.
    bool contains(const std::string& keypath ) const
    {
        return _topGroup.contains( keypath );
    }

    /// Get value from settings
    template <class T>
    T value(const std::string& keypath, const T& defaultValue ) const
    {
        return _topGroup.value( keypath, defaultValue);
    }

    /// Set addition settings
    template <class T>
    void setValue( const char *keypath, const T& avalue )
    {
        _topGroup.setValue( keypath, avalue);
    }

    /// Get Directory path from Node
    /// "~" refers to the "UserHomeDirectoryPath" directory
    std::string directoryPath( const std::string& fldpath, const std::string& defvalue  ) const
    {
      return _topGroup.directoryPath( fldpath, defvalue);
    }

    /// Writes any unsaved changes to permanent storage
    void sync() const
    {
        _file.SaveJson( _mainSettings.get() );
    }

    // jsonio internal data links ------------------------------


    /// Link to Thrift schema definition
    const ThriftSchema*  Schema() const
    {
        return &schema;
    }

    /// Connect localDB to new path
    bool updateSchemaDir();

    /// Current work directory
    std::string userDir() const
    {
        return UserDir;
    }

    /// updatre user directory
    void setUserDir( const std::string& dirPath );

    /// Current work directory
    std::string homeDir() const
    {
        return HomeDir;
    }

    /// updatre user directory
    void setHomeDir( const std::string& dirPath );

    /// Default ArangoDB connection data group
    std::string groupArangoDB( const std::string& link ) const
    {
        return groupName+"."+ link ;
    }

    /// Default ArangoDB connection data group
    std::string defaultArangoDB() const
    {
        return groupName+"."+_jsonioGroup.value( "UseArangoDBInstance",  std::string("ArangoDBLocal") );
    }

    /// Use content type Velocypack on sending requests (fu_content_type_vpack)
    bool useVelocypackPut() const
    {
        return _jsonioGroup.value( "UseVelocypackPut", true );
    }

    /// Use content type Velocypack on getting results
    bool useVelocypackGet() const
    {
        return _jsonioGroup.value( "UseVelocypackGet", true );
    }

    /// Overwrite mode when loading data
    bool overwrite() const
    {
        return _jsonioGroup.value( "ImportFormatOverwrite", true );
    }

    /// Graph database collection name (by default "data") deprecated
    std::string collection() const
    {
        return _jsonioGroup.value( "LocalDBCollection", std::string("data") );
    }

    /// Path to Resources files
    std::string resourcesDir() const
    {
        return directoryPath( "common.ResourcesDirectory",  std::string("") );
    }

private:

    // settings data allocation

    /// Internal Data File
    FJson _file;

    /// Properties for program
    std::shared_ptr<JsonDomFree> _mainSettings;

    /// Link to top settings
    GroupSettings _topGroup;

    std::string jsonioGroupName = "jsonio";

    /// Link to jsonio settings
    GroupSettings _jsonioGroup;

    // jsonio internal data allocation

    /// Path to schemas directory
    std::string SchemDir;
    /// Current work directory
    std::string UserDir;
    /// Current home directory
    /// "~" generally refers to the user's home directory
    /// Would be used to update directory path containing "~"
    std::string HomeDir;

    /// Current schema structure
    ThriftSchema schema;

    /// Read data from settings
    void getDataFromPreferences();

    /// Read all thrift schemas from Dir
    void readSchemaDir( const std::string& dirPath );

    /// Current schema directory
    std::string schemDir() const
    {
        return SchemDir;
    }


};

/// Function to connect to only one Preferences object
extern JsonioSettings& ioSettings();


} // namespace jsonio

#endif // IOSETTINGS_H
