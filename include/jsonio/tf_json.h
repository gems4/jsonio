//  This is JSONIO library+API (https://bitbucket.org/gems4/jsonio)
//
/// \file v_json.h
/// Declaration API to work with bson data
/// Declarations of functions for data exchange to/from json format
//
// JSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef TF_JSON_H
#define TF_JSON_H

#include <fstream>
#include "jsonio/jsondom.h"

namespace jsonio {


   /// Print JsonNode structure/array to Json string
   void printNodeToJson( std::string& recJson, const JsonDom* object, bool dense = false );

   /// Save data from JsonNode structure/array to  json file
   void printNodeToJson( std::fstream& fout, const JsonDom* object );

   /// Parse one Json object from string to JsonNode structure/array
   bool parseJsonToNode( const std::string& recJson, JsonDom* object );

   /// Read one Json object from text file and parse to JsonNode structure/array
   bool parseJsonToNode( std::fstream& fin, JsonDom* object );


   /// Interface for future using in/out cfg files
   class IsUnloadToJson
   {
    public:

       IsUnloadToJson() {}
       virtual ~IsUnloadToJson();

       virtual void toJsonNode( JsonDom *object ) const = 0;
       virtual void fromJsonNode( const JsonDom *object )= 0;
   };

} // namespace jsonio

#endif	// TF_JSON_H
