//  This is JSONIO library+API (https://bitbucket.org/gems4/jsonio)
//
/// \file dbjsondoc.h
/// Declarations of class TDBJsonDocument - working with database collections documents
/// Used  JsonDomFree class - API for direct code access to internal JSON data.
//
// JSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2018 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef TDBJSONDOC_H
#define TDBJSONDOC_H

#include "jsonio/dbdocument.h"
#include "jsonio/jsondomfree.h"

namespace jsonio {


/// Definition of json based database document
class TDBJsonDocument : public TDBDocumentBase
{
protected:

    TDBJsonDocument(   const TDataBase* dbconnect,
                       const std::string& coltype, const std::string& colname,
                       const std::vector<std::string>& keyTemplateSelect );

public:

    static TDBJsonDocument* newJsonDocumentQuery( const TDataBase* dbconnect, const std::string& collcName,
                         const std::vector<std::string>& keyTemplateSelect = {}, const DBQueryData& query = emptyQuery  );

    static TDBJsonDocument* newJsonDocument( const TDataBase* dbconnect, const std::string& collcName,
                                             const std::vector<std::string>& keyTemplateSelect = {} );

    ///  Constructor
    TDBJsonDocument( const TDataBase* dbconnect, const std::string& colname,
                     const std::vector<std::string>& keyTemplateSelect = {} ):
        TDBJsonDocument( dbconnect, "undef", colname, keyTemplateSelect )
    { }

    ///  Constructor
    TDBJsonDocument( TDBCollection* collection, const std::vector<std::string>& keyTemplateSelect = {}  );

    /// Constructor from configuration data
    TDBJsonDocument( TDBCollection* collection, const JsonDom *object );

    ///  Destructor
    virtual ~TDBJsonDocument(){}

    /// Link to internal dom data
    virtual const JsonDomFree* getDom() const
    {
        return _currentData.get();
    }

    /// Set _id to document
    void setOid( const std::string& _oid  )
    {
        _currentData->setOid_( _oid );
    }

    /// Extract _id from current document
    std::string getOid() const
    {
        std::string stroid;
        getValue( "_id", stroid );
        return stroid;
    }

    /// Return curent document as json string
    std::string GetJson( bool dense = false ) const;
    /// Load document from json string
    void SetJson( const std::string& sjson );

    /// Extract key from current document
    std::string getKeyFromCurrent() const;
    /// Load document from json string
    /// \return current document key
    std::string recFromJson( const std::string& jsondata );

    /// Get field value from document
    /// If field is not type T, the false will be returned.
    template <class T>
    bool getValue( const std::string& fldpath, T& value  ) const
    {
        return  _currentData->findValue( fldpath, value );
    }

    /// Update field value to document
    /// Add field and path, if no exist
    template <class T>
    bool setValue( const std::string& fldpath, const T& value  )
    {
        std::string strval  =  TArray2Base::value2string( value );
        return _currentData->updateFieldPath( fldpath, strval);
    }

    FieldSetMap extractFields( const std::vector<std::string> queryFields, const JsonDom* domobj ) const;
    FieldSetMap extractFields( const std::vector<std::string> queryFields, const std::string& jsondata ) const;
    /// Run current query, rebuild internal table of values
    void updateQuery();

protected:

    /// Names of fields to generate key template
    std::vector<std::string>  _keyTemplateFields;

    /// Current document object
    std::shared_ptr<JsonDomFree> _currentData;

    void setData();

    /// Prepare data to save to database
    virtual JsonDomFree* recToSave( time_t crtt, char* oid );

    /// Find key from current data
    /// Compare to data into query table
    std::string getKeyFromValueNode() const
    {
      jsonioErrIf( _queryResult.get() == nullptr, "testValues", "Could be execute only into selection mode.");
      return  _queryResult->getKeyFromValue(_currentData.get());
    }

};

} // namespace jsonio

#endif // TDBSCHEMADOC_H
