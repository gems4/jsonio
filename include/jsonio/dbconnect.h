//  This is JSONIO library+API (https://bitbucket.org/gems4/jsonio)
//
/// \file dbconnect.h
/// Declarations of class TDataBase to managing database connection
//
// JSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2018 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef TDBCONNECT_H
#define TDBCONNECT_H

#include <memory>
#include "jsonio/dbcollection.h"

namespace jsonio {

/// Class TDataBase to managing database connection
class TDataBase
{

public:

    /// Constructor use specific database vendor.
    TDataBase( std::shared_ptr<TAbstractDBDriver> dbDriver )
    {
        updateDriver( dbDriver );
    }
    ///  Constructor used internal TArangoDBClient
    TDataBase();

    /// Destructor
    virtual ~TDataBase();


    ///  Switch to the specified database driver (provided it exists and the user can connect to it).
    ///  From this point on, any following action in the same shell or connection will use the specified database.
    void updateDriver( std::shared_ptr<TAbstractDBDriver> dbDriver );

    /// Get current database driver
    TAbstractDBDriver* theDriver() const
    {
        return _driver.get();
    }

    /// Load the collection with the given colname or create new if no such collection exists.
    /// \param type - type of collection ( "undef", "schema", "vertex", "edge" )
    /// \param colname - name of collection
    TDBCollection *getCollection( const std::string& type, const std::string& colname  ) const
    {
        auto itr = _collections.find(colname);
        if( itr != _collections.end() )
            return itr->second.get();

        return addCollection( type, colname );
    }

protected:

    /// Current Database Driver
    std::shared_ptr<TAbstractDBDriver> _driver;

    /// List of loaded collections
    mutable std::map< std::string, std::shared_ptr<TDBCollection> > _collections;

    /// Load the collection with the given colname or create new if no such collection exists.
    /// \param type - type of collection ( "undef", "schema", "vertex", "edge" )
    /// \param colname - name of collection
    TDBCollection *addCollection( const std::string& type, const std::string& colname  ) const ;

};

} // namespace jsonio

#endif // TDBCONNECT_H
