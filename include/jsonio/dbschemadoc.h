//  This is JSONIO library+API (https://bitbucket.org/gems4/jsonio)
//
/// \file dbschemadoc.h
/// Declarations of class TDBSchema - working with database
/// Used  SchemaNode class - API for direct code access to internal
/// DOM based on our JSON schemas.
//
// JSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2018 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef TDBSCHEMADOC_H
#define TDBSCHEMADOC_H

#include "jsonio/dbdocument.h"
#include "jsonio/jsondomschema.h"

namespace jsonio {


/// Definition of schema based database document
class TDBSchemaDocument : public TDBDocumentBase
{
    friend class GraphTraversal;

public:

    static TDBSchemaDocument* newSchemaDocumentQuery( const TDataBase* dbconnect, const std::string& schemaName,
                         const std::string& collcName, const DBQueryData& query = emptyQuery  );

    static TDBSchemaDocument* newSchemaDocument( const TDataBase* dbconnect,
              const std::string& schemaName, const std::string& collcName  );

    ///  Constructor
    TDBSchemaDocument( const std::string& aschemaName,
                       const TDataBase* dbconnect, const std::string& colname ):
        TDBSchemaDocument( aschemaName, dbconnect, "schema", colname )
    {
        resetSchema( aschemaName, true );
    }

    ///  Constructor
    TDBSchemaDocument( const std::string& schemaName, TDBCollection* collection  );

    /// Constructor from configuration data
    TDBSchemaDocument( TDBCollection* collection, const JsonDom *object );
    virtual void toJsonNode( JsonDom *object ) const;
    virtual void fromJsonNode( const JsonDom *object );

    ///  Destructor
    virtual ~TDBSchemaDocument(){}

    /// Change current schema
    virtual void resetSchema( const std::string& aschemaName, bool change_queries );

    /// Get the name of thrift schema
    const std::string& getSchemaName() const
    {
        return _schemaName;
    }

    /// Link to internal dom data
    virtual const JsonDomSchema* getDom() const
    {
        return _currentData.get();
    }

    /// Set _id to document
    void setOid( const std::string& _oid  )
    {
        _currentData->setOid_( _oid );
    }
    /// Extract _id from current document
    std::string getOid() const
    {
        std::string stroid;
        getValue( "_id", stroid );
        return stroid;
    }

    /// Return curent document as json string
    std::string GetJson( bool dense = false ) const;
    /// Load document from json string
    void SetJson( const std::string& sjson );


    /// Extract key from current document
    std::string getKeyFromCurrent() const;
    /// Load document from json string
    /// \return current document key
    std::string recFromJson( const std::string& jsondata );


    /// Get field value from document
    /// If field is not type T, the false will be returned.
    template <class T>
    bool getValue( const std::string& fldpath, T& value  ) const
    {
        return  _currentData->findValue( fldpath, value );
    }

    /// Update field value to document
    template <class T>
    bool setValue( const std::string& fldpath, const T& value  )
    {
        return _currentData->setFieldValue( fldpath, value );
    }

    FieldSetMap extractFields( const std::vector<std::string> queryFields, const JsonDom* domobj ) const;
    FieldSetMap extractFields( const std::vector<std::string> queryFields, const std::string& jsondata ) const;

    /// Set&execute query for document
    void SetQuery( const DBQueryDef& querydef );

    /// Set&execute query for document
    void SetQuery( DBQueryData query, std::vector<std::string>  fieldsList = {} );

    /// Run current query, rebuild internal table of values
    void updateQuery();

protected:

    /// Link to current schema definition
    const ThriftSchema *_schema;

    /// Current schema name
    std::string _schemaName = "";

    /// Current document object
    std::shared_ptr<JsonDomSchema> _currentData;

    /// Constructor
    TDBSchemaDocument( const std::string& schemaName, const TDataBase* dbconnect,
                       const std::string& coltype, const std::string& colname );

    /// Prepare data to save to database
    JsonDomSchema* recToSave( time_t crtt, char* oid );

    /// Build default query fields ( by default internal )
    std::vector<std::string>  makeDefaultQueryFields() const;

    /// Find key from current data
    /// Compare to data into query table
    std::string getKeyFromValueNode() const
    {
      jsonioErrIf( _queryResult.get() == nullptr, "testValues", "Could be execute only into selection mode.");
      return  _queryResult->getKeyFromValue(_currentData.get());
    }

};

} // namespace jsonio

#endif // TDBSCHEMADOC_H
