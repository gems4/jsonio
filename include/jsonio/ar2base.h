//  This is JSONIO library+API (https://bitbucket.org/gems4/jsonio)
//
/// \file ar2base.h
/// Declarations of  basic class for read/write scalar/array to format.
/// Converters between types using a combination of implicit and
/// user-defined conversions.
//
// JSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef AR2BASE_H
#define AR2BASE_H

#include <sstream>
#include "jsonio/nservice.h"

namespace jsonio {

extern const char* S_EMPTY;
extern const char* S_ANY;
extern const char CHAR_EMPTY;
extern const char CHAR_ANY;
extern const short SHORT_EMPTY;
extern const short SHORT_ANY;
extern const double DOUBLE_EMPTY;
extern const float FLOAT_EMPTY;
const std::string emptiness("---");


bool IsFloatEmpty( const float v );
bool IsDoubleEmpty( const double v );

/// Basic class for read/write scalar/array to format
class TArray2Base
{
protected:

public:

    static int doublePrecision;	///< Precision of double
    static int floatPrecision;	///< Precision of float

    /// Set empty value
    template <class T>
    static void setEMPTY( T& val );

    /// Test empty value
    template <class T>
    static  bool isEMPTY( const T& val );

    /// Test any value
    template <class T>
    static  bool isANY( const T& val );

    /// Write value to string
    template <class T>
    static std::string value2string( const T& value )
    {
        if( isEMPTY(value) )
            return S_EMPTY;
        if( isANY(value) )
            return S_ANY;
        else
            return std::to_string(value);
    }

    static std::string value2string( const char* value )
    {
        return std::string(value);
    }


    /// Read value from string
    template <class T>
    static bool string2value( const std::string& data, T& value )
    {
        std::string ss = data;
        strip( ss );
        if(  ss==S_EMPTY )
        {
            TArray2Base::setEMPTY( value );
            return true;
        }
        //if( ss == S_ANY )
        //{
        //   TArray2Base::setANY( value );
        //   return true;
        //}
        std::stringstream sstr(ss);
        sstr >> value;
        if( sstr.fail() )
            return false;
        else
            return true;
    }

    /// Write value to double
    template <class T>
    static double value2double( const T& value )
    {
        return static_cast<double>(value);
    }

    /// Read value from double
    template <class T>
    static void double2value( const double data, T& value )
    {
        value = static_cast<T>(data);
    }


    /// Constructor
    TArray2Base()
    { }

    /// Destructor
    virtual ~TArray2Base();

};

template <>  bool TArray2Base::string2value( const std::string& data, std::string& value );
template<> std::string TArray2Base::value2string( const double& value );
template<> std::string TArray2Base::value2string( const float& value );
template<> std::string TArray2Base::value2string( const char& val );
template<> std::string TArray2Base::value2string( const bool& val );
template<> std::string TArray2Base::value2string( const std::string& val );
template<> double TArray2Base::value2double( const std::string& value );
template<> void TArray2Base::double2value( const double data, std::string& value );
template<> void TArray2Base::setEMPTY( short& val );
template<> void TArray2Base::setEMPTY( bool& val );
template<> void TArray2Base::setEMPTY( unsigned short& val );
template<> void TArray2Base::setEMPTY( long& val );
template<> void TArray2Base::setEMPTY( int& val );
template<> void TArray2Base::setEMPTY( unsigned long& val );
template<> void TArray2Base::setEMPTY( float& val );
template<> void TArray2Base::setEMPTY( double& val );
template<> void TArray2Base::setEMPTY( unsigned char& val );
template<> void TArray2Base::setEMPTY( signed char& val );
template<> void TArray2Base::setEMPTY( char& val );
template<> void TArray2Base::setEMPTY( std::string& val );
template<> bool TArray2Base::isANY( const bool&  );
template<> bool TArray2Base::isANY( const short&  );
template<> bool TArray2Base::isANY( const unsigned short&  );
template<> bool TArray2Base::isANY( const long&  );
template<> bool TArray2Base::isANY( const long long&  );
template<> bool TArray2Base::isANY( const int&  );
template<> bool TArray2Base::isANY( const unsigned long& );
template<> bool TArray2Base::isANY( const float&  );
template<> bool TArray2Base::isANY( const double& );
template<> bool TArray2Base::isANY( const unsigned char&  );
template<> bool TArray2Base::isANY( const signed char& );
template<> bool TArray2Base::isANY( const char& );
template<> bool TArray2Base::isANY( const std::string& );
template<> bool TArray2Base::isEMPTY( const bool&  );
template<> bool TArray2Base::isEMPTY( const short&  );
template<> bool TArray2Base::isEMPTY( const unsigned short&  );
template<> bool TArray2Base::isEMPTY( const long&  );
template<> bool TArray2Base::isEMPTY( const long  long&  );
template<> bool TArray2Base::isEMPTY( const int&  );
template<> bool TArray2Base::isEMPTY( const unsigned long& );
template<> bool TArray2Base::isEMPTY( const float&  );
template<> bool TArray2Base::isEMPTY( const double& );
template<> bool TArray2Base::isEMPTY( const unsigned char&  );
template<> bool TArray2Base::isEMPTY( const signed char& );
template<> bool TArray2Base::isEMPTY( const char& );
template<> bool TArray2Base::isEMPTY( const std::string& );

} // namespace jsonio

#endif // AR2BASE_H
