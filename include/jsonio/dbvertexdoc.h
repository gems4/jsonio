//  This is JSONIO library+API (https://bitbucket.org/gems4/jsonio)
//
/// \file dbvertexdoc.h
/// Declarations of class TDBGraph - working with graph databases (OLTP)
/// Used  SchemaNode class - API for direct code access to internal
/// DOM based on our JSON schemas.
//
// JSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2018 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef TDBVERTEX_H
#define TDBVERTEX_H

#include "jsonio/dbschemadoc.h"

namespace jsonio {

using UniqueFieldsMap = std::map<std::vector<std::string>, std::string >;  ///< map unique fields values<-> record key

std::string collectionNameFromSchema( const std::string& schemaName );

/// A graph is a structure composed of vertices and edges.
/// Both vertices and edges can have an arbitrary number of
/// key/value-pairs called properties.
enum  GraphDBRecordType
{
    rVertex = 0,
    rEdge = 1,
    rQuery = 2,  // future use
    rUndef = 10
};

/// Definition of graph databases chain
class TDBVertexDocument : public TDBSchemaDocument
{

    virtual void beforeRm(const std::string& key);
    virtual void beforeSaveUpdate(const std::string& key);

    virtual void afterRm(const std::string& key);
    virtual void afterSaveUpdate(const std::string&  key);

protected:

    /// Type constructor
    TDBVertexDocument( const std::string& schemaName, const TDataBase* dbconnect,
                       const std::string& coltype, const std::string& colname ):
        TDBSchemaDocument( schemaName, dbconnect, coltype, colname ),
        _changeSchemaMode(false)
    {}

public:

    static TDBVertexDocument* newVertexDocumentQuery( const TDataBase* dbconnect, const std::string& schemaName,
                                                          const DBQueryData& query = emptyQuery  );
    static TDBVertexDocument* newVertexDocument( const TDataBase* dbconnect, const std::string& schemaName );

    ///  Constructor collection&document
    TDBVertexDocument( const std::string& aschemaName, const TDataBase* dbconnect );
    ///  Constructor document
    TDBVertexDocument( const std::string& schemaName, TDBCollection* collection  );
    /// Constructor from configuration data
    TDBVertexDocument( TDBCollection* collection, const JsonDom *object );

    ///  Destructor
    virtual ~TDBVertexDocument(){}

    /// Change current schema mode
    /// If true,  current schemaName can be changed when read new record
    void resetMode( bool mode )
    {
        _changeSchemaMode = mode;
    }
    /// Test&change schema
    void testUpdateSchema( const std::string&  pkey );

    /// Change current schema
    void resetSchema( const std::string& aschemaName, bool change_queries );

    /// Load document from json string
    /// \return current document key
    std::string recFromJson( const std::string& jsondata );

    /// Make query by keyword
    DBQueryData idQuery( const std::string& id ) const
    {
        return  DBQueryData( std::string("{ \"_id\" : \"")+ id + "\" }", DBQueryData::qTemplate);
    }

    /// Make all Edges query
    DBQueryData allEdgesQuery( const std::string& idVertex ) const
    {
        return edgesQuery( idVertex, DBQueryData::qEdgesAll, "" );
    }

    /// Make all Edges query ( edgeCollections - comma separated edges collections list )
    DBQueryData allEdgesQuery( const std::string& edgeCollections, const std::string& idVertex ) const
    {
        return edgesQuery( idVertex, DBQueryData::qEdgesAll, edgeCollections );
    }

    /// Make Follow Outgoing Edges query
    DBQueryData outEdgesQuery( const std::string& idVertex ) const
    {
        return edgesQuery( idVertex, DBQueryData::qEdgesFrom, "" );
    }

    /// Make Follow Outgoing Edges query ( edgeCollections - comma separated edges collections list )
    DBQueryData outEdgesQuery( const std::string& edgeCollections, const std::string& idVertex ) const
    {
        return edgesQuery( idVertex, DBQueryData::qEdgesFrom, edgeCollections );
    }

    /// Make Follow Incoming Edges query
    DBQueryData inEdgesQuery( const std::string& idVertex ) const
    {
        return edgesQuery( idVertex, DBQueryData::qEdgesTo, "" );
    }

    /// Make Follow Incoming Edges query ( edgeCollections - comma separated edges collections list )
    DBQueryData inEdgesQuery( const std::string& edgeCollections, const std::string& idVertex ) const
    {
        return edgesQuery( idVertex, DBQueryData::qEdgesTo, edgeCollections );
    }

    /// Test existence Outgoing Edges
    bool existOutEdges( const std::string& id )
    {
        auto query = outEdgesQuery( id );
        return existKeysByQuery( query );
    }
    /// Build Outgoing Edges keys list
    std::vector<std::string> getOutEdgesKeys( const std::string& id )
    {
        auto query = outEdgesQuery( id );
        return getKeysByQuery( query );
    }

    /// Test existence Incoming Edges
    bool existInEdges( const std::string& id )
    {
        auto query = inEdgesQuery( id );
        return existKeysByQuery( query );
    }
    /// Build Incoming Edges keys list
    std::vector<std::string> getInEdgesKeys( const std::string& id )
    {
        auto query = inEdgesQuery( id );
        return getKeysByQuery( query );
    }


    /// Define new Vertex document
    void setVertexObject( const std::string& aschemaName, const FieldSetMap& fldvalues );
    /// Change current schema document data
    void updateVertexObject( const std::string& aschemaName, const FieldSetMap& fldvalues );

    /// Creates a new vertex document in the collection from the given fldvalues data.
    /// \param  fldvalues - data to save
    /// \param  testValues - If testValues is true, we compare the current data with the internally loaded values,
    /// and if all the values are the same, then we update the selected record instead of creating new ones.
    /// \return new key of document
    std::string CreateVertex( const std::string& aschemaName, const FieldSetMap& fldvalues, bool testValues = false )
    {
        setVertexObject( aschemaName, fldvalues );
        return CreateWithTestValues( testValues );
    }

    /// Update/create a vertex document.
    /// \param  fldvalues - values to update
    /// \param  testValues - If testValues is true, we compare the current data with the internally loaded values,
    /// and if all the values are the same, then we update the selected record instead of creating new ones.
    void UpdateVertex( const std::string& aschemaName, const FieldSetMap& fldvalues, bool testValues = false )
    {
        updateVertexObject( aschemaName, fldvalues );
        UpdateWithTestValues( testValues );
    }

    /// Build map of fields-value pairs
    FieldSetMap loadRecordFields( const std::string& id, const std::vector<std::string>& queryFields );

    // service functions

    /// Extract label by id  ( old  using query - observed )
    std::string extractLabelById( const std::string& id );

    /// Extract label from id  ( no query )
    std::string extractLabelFromId( const std::string& id );

    /// Extract schema by id  ( no query )
    virtual std::string  extractSchemaFromId( const std::string& id  )
    {
        std::string label = extractLabelFromId( id );
        label.pop_back(); // delete last "s"
        return _schema->getVertexName( label );
    }


protected:

    // Internal data
    bool _changeSchemaMode = false;  ///< When read new record from DB, schemaName can be changed
    std::string _type;
    std::string _label;

    std::vector<std::string>  uniqueFieldsNames;     ///< names of fields to be unique
    UniqueFieldsMap uniqueFieldsValues;              ///< map to save unique fields values


    // internal functions
    /// Test true type and label for schema
    void testSchema( const std::string& jsondata );

    /// Change base collections
    void updateCollection( const std::string& aschemaName );

    /// Build default json query std::string for collection
    DBQueryData makeDefaultQueryTemplate() const;

    UniqueFieldsMap::iterator uniqueLinebyId( const std::string& idschem );

    /// Init uniqueFields when load collection
    void loadUniqueFields();

    /// Make Edges query
    DBQueryData edgesQuery( const std::string& id, int type, const std::string& edgeCollections ) const
    {
        std::string _edgeCollections = edgeCollections;
        //if( _edgeCollections.empty() )
        //   _edgeCollections = "inherits, takes, defines, master, product, prodreac, basis,"
        //                      "pulls, involves, adds, yields";

        std::string queryJson = "{ \"startVertex\": \""
                + id  + "\", \"edgeCollections\": \""
                + _edgeCollections + "\" }";
        return DBQueryData( queryJson, type);
    }

};

} // namespace jsonio

#endif // TDBVERTEX_H
