//  This is JSONIO library+API (https://bitbucket.org/gems4/jsonio)
//
/// \file dbcollection.h
/// Declarations of classes TAbstractDBDriver - Interface for Abstract Database Driver
/// and class  TDBCollection - definition collections API
//
// JSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2018 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef TDCOLLECTION_H
#define TDCOLLECTION_H

#include <functional>
#include <memory>
#include "jsonio/tf_json.h"

namespace jsonio {

//https://docs.arangodb.com/3.3/HTTP/Gharial/Management.html

class TDBDocumentBase;
class TDataBase;
class DBQueryData;

/// Table key -> the selector, which must be an object containing the _id or _key attribute.
using KeysSet = std::map<std::string, std::unique_ptr<char> >;
// Now we use "_id" as key and "_id" as selector, but for internal key we can use other data in future
// and for selector data can be used other data in driver

/// Callback function fetching document from a collection that match the specified condition
using  SetReadedFunction = std::function<void( const std::string& jsondata )>;
/// Callback function fetching document and _id from a collection that match the specified condition
using  SetReadedFunctionKey = std::function<void( const std::string& jsondata, const std::string& key )>;

/// Internal function to generate key template
std::string makeTemplateKey( const JsonDom *object, const std::vector<std::string>&  keyTemplateFields );

/// Interface for Abstract Database Driver
/// The driver works like an adaptor which connects a generic interface
/// to a specific database vendor implementation.
class TAbstractDBDriver
{

public:

    /// Types of collection to select
    enum CollectionTypes {
        clVertex  = 0x0001,        ///< No edges collections
        clEdge    = 0x0002,        ///< Edges
        clAll = clVertex|clEdge    ///< List all vertexes&edges
    };

    ///  Constructor
    TAbstractDBDriver()
    {}

    ///  Destructor
    virtual ~TAbstractDBDriver();

    // Collections API

    /// Create collection if no exist
    /// \param colname - name of collection
    /// \param type - type of collection ( "undef", "schema", "vertex", "edge" )
    virtual void createCollection(const std::string& collname, const std::string& ctype) = 0;

    /// Returns all collections names of the given database.
    /// \param ctype - types of collection to select.
    virtual std::set<std::string> getCollectionNames( CollectionTypes ctype ) = 0;

    virtual std::string getServerKey( char* pars ) = 0;

    // Gen new oid or other pointer of location
    //virtual string genOid(const char* collname, const std::string& _keytemplate ) = 0 ;

    /// Returns the document described by the selector.
    /// \param collname - collection name
    /// \param it -  pair: key -> selector
    /// \param jsonrec - object to receive data
    virtual bool loadRecord( const std::string& collname, KeysSet::iterator& it, std::string& jsonrec ) = 0;

    /// Removes a document described by the selector.
    /// \param collname - collection name
    /// \param it -  pair: key -> selector
    virtual bool removeRecord(const std::string& collname, KeysSet::iterator& it ) = 0;

    /// Creates a new document in the collection from the given data or
    /// replaces an existing document described by the selector.
    /// \param collname - collection name
    /// \param it -  pair: key -> selector
    /// \param jsonrec - json object with data
    virtual std::string saveRecord( const std::string& collname, KeysSet::iterator& it, const std::string& jsonrec ) = 0;

    /// Fetches all documents from a collection that match the specified condition.
    ///  \param collname - collection name
    ///  \param query -    selection condition
    ///  \param setfnc -   callback function fetching document data
    virtual void selectQuery( const std::string& collname, const DBQueryData& query, SetReadedFunction setfnc ) = 0;

    /// Looks up the documents in the specified collection using the array of ids provided.
    ///  \param collname - collection name
    ///  \param ids -      array of _ids
    ///  \param setfnc -   callback function fetching document data
    virtual void lookupByIds( const std::string& collname,  const std::vector<std::string>& ids,  SetReadedFunction setfnc ) = 0 ;

    /// Fetches all documents from a collection.
    ///  \param collname -    collection name
    ///  \param queryFields - list of fields to selection
    ///  \param setfnc -     callback function fetching document data
    virtual void allQuery( const std::string& collname,
                           const std::set<std::string>& queryFields,  SetReadedFunctionKey setfnc ) = 0;

    /// Delete all edges linked to vertex record.
    ///  \param collname - collection name
    ///  \param vertexid - vertex record id
    virtual void deleteEdges(const std::string& collname, const std::string& vertexid ) = 0;

    /// Removes all documents from the collection whose keys are contained in the keys array.
    ///  \param collname - collection name
    ///  \param ids -      array of keys
    virtual void removeByIds( const std::string& collname,  const std::vector<std::string>& ids  ) = 0;

    ///  Provides 'distinct' operation over collection
    ///  \param collname - collection name
    ///  \param fpath    - field path to collect distinct values from
    ///  \param  values  - return values by specified fpath and collname
    virtual void fpathCollect( const std::string& collname, const std::string& fpath,
                               std::vector<std::string>& values ) = 0;

};

/// Class  TDBCollection - definition collections API
/// Documents are grouped into collections.
/// A collection contains zero or more documents.
class TDBCollection: public IsUnloadToJson
{
    friend class TDataBase;

public:

    ///  Constructor
    TDBCollection( const TDataBase* adbconnect, const std::string& name );
    ///  Destructor
    virtual ~TDBCollection()
    {}

    /// Constructor from json data
    TDBCollection( const TDataBase* adbconnect, const JsonDom *object );
    virtual void toJsonNode( JsonDom *object ) const;
    virtual void fromJsonNode( const JsonDom *object );


    /// Load the collection or create new if no such collection exists.
    void Load();
    /// Close the collection and free internal data
    void Close();

    /// Refresh list of documents keys
    void reloadCollection();

    //--- Selectors

    /// Get the unique name of collection
    const std::string& name() const
    {  return _name;   }

    /// Update type of collection
    void setCollectionType( const std::string& coltype )
    {
        _coltype = coltype;
    }
    /// Get type of collection
    std::string type() const
    {
        return _coltype;
    }

    /// Get number of documents into collection
    std::size_t getDocumentsCount() const
    {
        return _recList.size();
    }

    /// Access to the database
    const TDataBase* database() const
    {
        return _dbconnect;
    }

    /// Add new opened document from collection
    void addDocument( TDBDocumentBase* doc)
    {
        _documents.insert(doc);
    }
    /// Erase opened document from collection
    void removeDocument( TDBDocumentBase* doc)
    {
        auto itr =  _documents.find(doc);
        if( itr != _documents.end() )
            _documents.erase(doc);
    }

    /// Get document key from dom object
    virtual std::string getKeyFromDom( const JsonDom* object );

    //--- Manipulation records

    /// Checks whether a document exists.
    bool Find( const std::string& key );

    /// Creates a new document in the collection from the given data.
    /// \param document - object with data
    std::string Create( TDBDocumentBase* document );

    /// Retrive one document from the collection
    ///  \param document - object with data
    ///  \param key      - key of document
    void Read( TDBDocumentBase* document, const std::string& key );

    /// Updates an existing document or creates a new document described by the key,
    /// which must be an object containing the _id or _key attribute.
    ///  \param document - object with data
    ///  \param key      - key of document
    std::string Update( TDBDocumentBase* document, const std::string& key );

    /// Removes document from the collection
    /// \param key      - key of document
    void Delete( const std::string& key );

    /// Generate new _id or other pointer of location
    virtual std::string genOid( const std::string& _keytemplate )
    {
        //temporaly
        if( _keytemplate.empty())
            return "";
        return _name+"/"+genKeyFromTemplate(_keytemplate);
        //return _dbdriver->genOid(getName(), genKeyFromTemplate(_keytemplate));
    }

    //--- Manipulation list of records (tables)

    /// Build list of key fields for query
    virtual std::set<std::string> listKeyFields();

    /// Fetches all documents from a collection that match the specified condition.
    ///  \param query -    selection condition
    ///  \param setfnc -   callback function fetching document data
    void selectQuery( const DBQueryData& query,  SetReadedFunction setfnc )
    {
        _dbdriver->selectQuery( name(), query, setfnc );
    }

    /// Looks up the documents in the specified collection using the array of keys provided.
    ///  \param rkeys -      array of keys
    ///  \param setfnc -   callback function fetching document data
    void lookupByDocumentKeys( const std::vector<std::string>& rkeys,  SetReadedFunction setfnc )
    {
        auto ids = idsFromRecordKeys(  rkeys );
        _dbdriver->lookupByIds( name(), ids, setfnc );
    }

    /// Looks up the documents in the specified collection using the array of ids provided.
    ///  \param ids -      array of _ids
    ///  \param setfnc -   callback function fetching document data
    void lookupByIds( const std::vector<std::string>& ids,  SetReadedFunction setfnc )
    {
        _dbdriver->lookupByIds( name(), ids, setfnc );
    }

    /// Fetches all documents from a collection.
    ///  \param queryFields - list of fields to selection
    ///  \param setfnc -     callback function fetching document data
    void allQuery( const std::set<std::string>& queryFields, SetReadedFunctionKey setfnc )
    {
        _dbdriver->allQuery( name(), queryFields, setfnc );
    }

    /// Delete all edges linked to vertex record.
    ///  \param vertexid - vertex record id
    void deleteEdges( const std::string& vertexid )
    {
        _dbdriver->deleteEdges( name(), vertexid );
        reloadCollection();
    }

    /// Removes all documents from the collection whose keys are contained in the keys array.
    ///  \param collname - collection name
    ///  \param rkeys -      array of keys
    void removeByDocumentKeys( const std::vector<std::string>& rkeys  )
    {
        auto ids = idsFromRecordKeys(  rkeys );
        _dbdriver->removeByIds( name(), ids );
        reloadCollection();
    }

    ///  Provides 'distinct' operation over collection
    ///  \param fpath Field path to collect distinct values from.
    ///  \param return values by specified fpath and collection
    void fpathCollect( const std::string& fpath, std::vector<std::string>& values )
    {
        _dbdriver->fpathCollect( name(), fpath, values );
    }

protected:

    /// Internal type of collection ( "undef", "schema", "vertex", "edge" )
    /// There are two types of collections: document collection as well as edge collections.
    std::string _coltype = "undef";

    /// The unique name of collection
    std::string _name;

    /// Operations started in one database
    const TDataBase* _dbconnect;

    /// Access to a specific database vendor implementation
    TAbstractDBDriver* _dbdriver;

    /// Documents are linked to collection
    std::set<TDBDocumentBase*> _documents; ///< Linked documents

    // Definition of record key list - internal loaded data

    /// List all documents keys into collection
    KeysSet _recList;
    /// Current item in recList
    KeysSet::iterator _itrL;

    /// Reconnect DataBase ( switch to the new database driver)
    void changeDriver( TAbstractDBDriver* adriver );

    // internal functions

    /// Load all keys from one collection
    virtual void loadCollectionFile( const std::set<std::string>& queryFields );
    /// Close collection file
    virtual void closeCollectionFile() {}

    /// Close collection
    void closeCollection()
    {
        closeCollectionFile();
    }

    /// Load collection
    void loadCollection()
    {
        // create collection if no exist
        _dbdriver->createCollection( name(), _coltype );
        auto keys = listKeyFields();
        loadCollectionFile( keys );
    }

    /// Add key from json structure to recList
    void listKeyFromDom( const std::string& jsondata, const std::string& keydata );

    /// Generate new _id from key template
    std::string genKeyFromTemplate( const std::string& _keytemplate );

    /// Convert record keys to ids
    std::vector<std::string> idsFromRecordKeys( const std::vector<std::string>& rkeys );

    /// Get ids list for a wildcard search
    std::size_t GetIdsList( const std::string& idpart, std::set<std::string>& aKeyList );

    /// Check if pattern/undefined in record key
    bool isPattern( const std::string& akey );


};



} // namespace jsonio

#endif // TDCOLLECTION_H
