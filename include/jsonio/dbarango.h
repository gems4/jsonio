//  This is JSONIO library+API (https://bitbucket.org/gems4/jsonio)
//
/// \file dbarango.h
/// Declarations of class
/// TArangoDBClient - DB driver for working with ArangoDB
//
// JSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); ArangoDB (https://github.com/arangodb/fuerte).
//

#ifndef TDBARANGO_H
#define TDBARANGO_H

#include <cstring>
#include "jsonio/dbconnect.h"

namespace jsonio {

extern const char* local_server_endpoint;
extern const char* local_server_username;
extern const char* local_server_password;
extern const char* local_server_database;
extern const char* local_root_username;
extern const char* local_root_password;
extern const char* local_root_database;
extern const char* remote_server_endpoint;
extern const char* remote_server_username;
extern const char* remote_server_password;
extern const char* remote_server_database;

namespace arangodb {
class ArangoDBCollectionAPI;
class ArangoDBUsersAPI;
}

/// HTTP Interface for User Management
struct ArangoDBUser
{
    /// The name of the user as a string. This is mandatory
    std::string name;
    /// The user password as a string
    std::string password;
    /// Database Access  "rw" or "ro"
    std::string access;
    /// An optional flag that specifies whether the user is active. If not specified, this will default to true
    bool active;          // must be true
    /// An optional JSON object with arbitrary extra data about the user
    std::string extra;


    ArangoDBUser( const std::string& theUser, const std::string& thePasswd,
                  const std::string& theAccess = "rw",
                  bool isactive = true, const std::string&  jsonextra=""):
       name(theUser), password(thePasswd), access(theAccess), active(isactive), extra(jsonextra)
    { }

    ArangoDBUser():
    name("root"), password(""), access("rw"), active(true), extra("")
    { }
};
bool operator!=(const ArangoDBUser& lhs, const ArangoDBUser& rhs);


struct ArangoDBConnect
{
    /// Server URL
    std::string serverUrl;   // "http://localhost:8529"
    /// The Database Name
    std::string databaseName; //  "hub_test"

    /// Server User Data
    ArangoDBUser user;

    ArangoDBConnect( const std::string& theURL, const std::string& theUser,
               const std::string& thePasswd, const std::string& theDBname):
       serverUrl(theURL),  databaseName(theDBname), user(theUser, thePasswd) { }

    ArangoDBConnect():
    serverUrl(local_server_endpoint), databaseName(local_server_database)  {}

    std::string fullHost() const
    {  return serverUrl+"/_db/"+databaseName; }

    std::string fullURL( const std::string& localpath ) const
    {
       return  fullHost()+localpath;
    }

    bool readonlyDBAccess( ) const
    {
      return( user.access=="ro");
    }

    void getFromSettings(const std::string& group, bool rootdata = false );
};

bool operator!=(const ArangoDBConnect& lhs, const ArangoDBConnect& rhs);


/// Definition of Collection Client used Low Level C++ Driver for ArangoDB
class TArangoDBClient : public TAbstractDBDriver
{

    /// Connection
    ArangoDBConnect connectData;
    /// ArangoDB connection data
    std::shared_ptr<arangodb::ArangoDBCollectionAPI> pdata;

    void setServerKey( const std::string& key, KeysSet::iterator& itr )
    {
        char *bytes = new char[key.length()+1];
        strncpy( bytes, key.c_str(), key.length() );
        bytes[key.length()] = '\0';
        itr->second = std::unique_ptr<char>(bytes);
    }

    bool isComlexFields( const std::set<std::string>& queryFields);

public:

    /// Constructor
    TArangoDBClient( const ArangoDBConnect& aconnectData )
    {
      resetDBConnection( aconnectData );
    }

    /// Default Constructor
    TArangoDBClient();

    ///  Destructor
    virtual ~TArangoDBClient()
    {}

    /// Reset connections to ArangoDB server
    void resetDBConnection( const ArangoDBConnect& connectData );
    /// Get ArangoDB connection data
    const jsonio::ArangoDBConnect& dbConnection() const
    {
       return connectData;
    }

    std::string getServerKey( char* pars )
    {
      return std::string(pars);
    }

    // Collections API

    /// Create collection if no exist
    void createCollection(const std::string& collname, const std::string& ctype);

   /// Collect collection names in current database
   std::set<std::string> getCollectionNames( CollectionTypes ctype );

   // Gen new oid or other pointer of location
   // virtual string genOid(const char* collname, const std::string& _keytemplate );

   /// Read record from DB to json string
   bool loadRecord( const std::string& collname, KeysSet::iterator& it, std::string& jsonrec );
   /// Remove current object from collection.
   bool removeRecord(const std::string& collname, KeysSet::iterator& it );
   /// Write json record to DB
   std::string saveRecord( const std::string& collname, KeysSet::iterator& it, const std::string& jsonrec );

   /// Creates a new document in the collection from the given data
   /// \param collname  - collection name
   /// \param jsonrec   - a JSON representation of a single document
   /// \return the document handle of the newly created document
   std::string createRecord( const std::string& collname, const std::string& jsonrec );

   /// Read record by rid to JSON representation of a single document
   /// \param collname  - collection name
   /// \param id        - the document handle
   /// \return false if error
   bool readRecord( const std::string& collname, const std::string& id, std::string& jsonrec );

   /// Updates an existing document described by the selector, which must be an object containing the _id or _key attribute.
   /// \param collname  - collection name
   /// \param id        - the document handle
   /// \param jsonrec    - a JSON representation of a single document
   /// \return the document handle of the updated document
   std::string updateRecord( const std::string& collname, const std::string& id, const std::string& jsonrec );

   /// Removes a document described by the selector, which must be an object containing the _id or _key attribute.
   /// \param collname  - collection name
   /// \param id        - the document handle
   /// \return false if error
   bool deleteRecord(const std::string& collname, const std::string& id );

   /// Read document header
   /// \param collname  - collection name
   /// \param id        - the document handle
   /// \return true is returned if the document was found
   bool  existsRecord( const std::string& collname, const std::string& id );

   /// Run query
   ///  \param collname - collection name
   ///  \param query - json string with query  condition (where) and list of fileds selection and other
   ///  \param setfnc - function for set up readed data
   void selectQuery( const std::string& collname, const DBQueryData& query,  SetReadedFunction setfnc );

   /// Execute function to multiple documents by their ids
   ///  \param collname - collection name
   ///  \param ids - list of _ids
   ///  \param setfnc - function for set up readed data
   void lookupByIds( const std::string& collname,  const std::vector<std::string>& ids,  SetReadedFunction setfnc );

   /// Run query for read Linked records list
   ///  \param collname - collection name
   ///  \param queryFields - list of fileds selection
   ///  \param setfnc - function for set up Linked records list
    void allQuery( const std::string& collname, const std::set<std::string>& queryFields,  SetReadedFunctionKey setfnc );

    /// Run delete edges connected to vertex record
    ///  \param collname - collection name
    ///  \param vertexid - vertex record id
    void deleteEdges(const std::string& collname, const std::string& vertexid );

    /// Removes multiple documents by their ids
    ///  \param collname - collection name
    ///  \param ids - list of _ids
    void removeByIds( const std::string& collname,  const std::vector<std::string>& ids  );

    ///  Provides 'distinct' operation over collection
    ///  \param fpath Field path to collect distinct values from.
    ///  \param collname - collection name
    ///  \param return values by specified fpath and collname
    void fpathCollect( const std::string& collname, const std::string& fpath,
                             std::vector<std::string>& values );

};

/// Definition of ArangoDB root API Low Level C++ Driver for ArangoDB
class TArangoDBRootClient
{

    /// Connection
    ArangoDBConnect rootData;

    /// ArangoDB database/user data
    std::shared_ptr<arangodb::ArangoDBUsersAPI> pusers;

public:

    /// Constructor
    TArangoDBRootClient( const ArangoDBConnect& rootconnectData )
    {
      resetDBConnection( rootconnectData );
    }

    /// Default Constructor
    TArangoDBRootClient();

    ///  Destructor
    virtual ~TArangoDBRootClient();

    /// Reset connections to ArangoDB server
    void resetDBConnection( const ArangoDBConnect& connectData );
    /// Get ArangoDB connection data
    const jsonio::ArangoDBConnect& dbConnection() const
    {
       return rootData;
    }

    // Database API

    /// Retrieves the list of all existing databases
    std::set<std::string> getDatabaseNames();
    /// Create Data base dbname with list of existing users if no exist
    /// \param users: Login Names of the users to be accessed
    void CreateDatabase( const std::string& dbname, const std::vector<ArangoDBUser>&  users = {} );

    /// Test exist database
    bool ExistDatabase( const std::string& dbname );

    // Users API

    /// Create user
    void CreateUser( const ArangoDBUser& userdata );
    /// Retrieves a map contains the databases names as object keys, and the associated privileges
    /// for the database as values of all databases the current user can access
    std::map<std::string,std::string> getDatabaseNames( const std::string&  user );
    /// Fetches data about all users
    std::set<std::string> getUserNames();

};


} // namespace jsonio

#endif // TDBARANGO_H
