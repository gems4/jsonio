#ifndef JSON_NODE_H
#define JSON_NODE_H

#include <iostream>
#include <memory>
#include "jsonio/jsondom.h"
#include "jsonio/thrift_schema.h"

namespace jsonio {

/// Get Json type from thrift type
int jsonTypeFromThrift( int thriftType );
/// Determine the default value corresponding to the thrift internal type
std::string getDefValue( int thriftType, const std::string& defval );

/// \class JsonDomSchema implementation an API for direct code access to internal
/// DOM based on our JSON schemas.
class JsonDomSchema: public JsonDom
{
    static ThriftFieldDef topfield;

    ThriftStructDef* _strDef;   ///< Struct definition
    ThriftFieldDef*  _fldDef;   ///< Field definition
    bool _isSetUp;              ///< Value set up by user
    std::string _fldKey;             ///< Key of Node
    std::string _fldValue;           ///< Value of Node

    JsonDomSchema* _parent;                      ///< Parent node
    std::vector<std::shared_ptr<JsonDomSchema>> _children; ///< Children nodes for Object or Array

    // internal data
    std::size_t _level;                   ///< index into fldDef->fTypeId array
    //  internal for GUI
    int _ndx;                     ///< internal ndx

    // -----------------------------------------------

    /// Set up default value to field
    void setDefault(const std::string& newdef, int deflevel );
    /// Set up current time as default value
    void setCurrentTime();
    /// Add levels for objects and arrays
    void addChildren( const std::string& jsondata );
    /// Define Struct
    void struct2model( ThriftStructDef* strDef, const std::string& jsondata );
    /// Define Array ( LIST, SET, MAP )
    void list2model(  const std::string& jsondata );

    /// Constructor for struct (up level )
    explicit JsonDomSchema( ThriftStructDef* aStrDef );
    /// Constructor for empty field
    JsonDomSchema( ThriftFieldDef* afldDef, JsonDomSchema* aparent );
    /// Constructor for array field
    JsonDomSchema( const std::string& key, int atype, const std::string& value, JsonDomSchema* aparent );

    virtual bool testKey( const std::string&  )
    {
        /*for(uint ii=0; ii< _children.size(); ii++ )
          if( _children[ii]->getKey() == akey )
              return true;*/
        return false;
    }

    virtual bool updateField( const std::string& newValue)
    {
        // convert string to internal type
        if( isArray() || isStruct()  )
            setComplexValue( newValue );
        else
           _fldValue = getDefValue( _fldDef->fTypeId[_level], newValue );
         addField();
        return true;
    }

    /// Resize Array with all levels
    void resizeArray( uint level, const std::vector<std::size_t>& sizes, const std::string& defval  );
    /// Resize current Array
    void resizeArray( std::size_t size, const std::string& defval  );

public:

    /// Create new Node for struct name
   static  JsonDomSchema* newSchemaObject( const std::string& className );

    virtual ~JsonDomSchema()
    {
     // cout << "~JsonDomSchema" << getKey();
    }

   virtual JsonDom *appendNode( const std::string& akey, int atype, const std::string& avalue );

    // Get methods  --------------------------

    /// Get Current key from Node
    virtual const std::string& getKey() const
    {
      return  _fldKey;
    }

    /// Get field key from Node
    const std::string& getFieldKey() const
    {
       return _fldDef->fName;
    }

    virtual int getType() const
    {
      return  jsonTypeFromThrift( getFieldType() );
    }

    /// Get field type from Node
    int getFieldType( bool key = false ) const
    {
      if( key )
        return  _fldDef->fTypeId[_parent->_level+1];
      return _fldDef->fTypeId[_level];
    }

    virtual int getNdx() const
    {
       auto aparent = getParent();
       if( !aparent )
         return _ndx;

       int internalndx = -1;
       for(uint ii=0; ii< aparent->_children.size(); ii++ )
       {
           if( aparent->_children[ii]->_isSetUp )
             internalndx++;
           if( aparent->_children[ii].get() == this )
             return  internalndx;
       }
       return 0;

    }

    virtual const std::string& getFieldValue() const
    {
      return  _fldValue;
    }

    virtual std::size_t getChildrenCount() const
    {
        std::size_t size = 0;
        for(std::size_t ii=0; ii< _children.size(); ii++ )
         if( _children[ii]->_isSetUp )
             size++;
        return size;
    }

    virtual JsonDomSchema* getChild( std::size_t ndx ) const
    {
      int internalndx = -1;
      for(std::size_t ii=0; ii< _children.size(); ii++ )
      {
          if( _children[ii]->_isSetUp )
            internalndx++;
          if( internalndx == static_cast<int>(ndx))
            return  _children[ii].get();
      }
      return nullptr;
    }

    virtual JsonDomSchema* getParent() const
    {
      return _parent;
    }

    /// Get field elements type from Node
    int elemTypeId() const
    {
      return _fldDef->fTypeId.back();
    }

    /// Get key name from Node
    virtual std::string getHelpName() const
    {
        std::string  schema = _strDef->getName();
                     schema += "#";
        return schema+getFieldKey();
    }

    /// Get Description from Node
    std::string getDescription() const
    {
       if( _level == 0 )
         return  _fldDef->fDoc;
       return "";
    }

    /// Get Description from Node
    std::string getFullDescription() const;

    /// Get min Value to Node
    double minValue() const
    {
      return _fldDef->minval;
    }

    /// Get max Value to Node
    double maxValue() const
    {
      return _fldDef->maxval;
    }

    /// Get def Value to Node
    std::string getDefault() const
    {
       if( _level == 0 )
        return _fldDef->fDefault;
       return "";
    }

    std::string getEnumI32Name() const
    {
        if( _fldDef->fTypeId[_level] == Th_I32 )
           return  _fldDef->className;
        else
           return "";
    }

    std::string getEnumMapName() const
    {
        if(  _fldDef->fTypeId[_level] == Th_MAP )
           return  _fldDef->className;
        else
           return "";
    }

    ThriftEnumDef* getMapEnumdef() const;

    // list of not defined fields
    std::vector<std::string> getNoUsedKeys() const;
    std::vector<std::string> getUsedKeys() const;

    /// Get Struct definition
    const ThriftStructDef* getSructDef() const
    {
        return _strDef;
    }

    /// Get Field definition
    const ThriftFieldDef* getFieldDef()  const
    {
        return _fldDef;
    }


    // Test methods  --------------------------------------------

     /// Test top Node
     virtual bool isTop() const
     { return( _parent==nullptr); }

     /// Test Node as Map
     bool isMap() const
     {
       if( isTop() )
         return false;
       return( _fldDef->fTypeId[_level] == Th_MAP );
     }

     /// Test Node as Struct
     bool isStruct() const
     { return( isTop() ||  _fldDef->fTypeId[_level] == Th_STRUCT ); }

     /// Test Node as Array
     bool isArray() const
     {
         if( isTop() )
           return false;
         return( _fldDef->fTypeId[_level] == Th_MAP ||
                 _fldDef->fTypeId[_level] == Th_SET ||
                 _fldDef->fTypeId[_level] == Th_LIST );
     }

     /// Test Node as numeric value
     bool isNumber( ) const
     {
       if( isTop() )
           return false;
       return( _fldDef->fTypeId[_level] >= Th_I08 &&
               _fldDef->fTypeId[_level] <= Th_I64 );
     }

     bool isUnion( ) const
     {
         return ( _parent->isStruct() && _parent->_strDef->testUnion() );
     }

     bool isReqiered( ) const
     {
         return ( _level==0 && _fldDef->fRequired == fld_required );
     }

     bool isSetUp() const
     {
         return _isSetUp;
     }

    // Access functions  ------------------------

   /// Get field by fieldpath ("name1.name2.name3")
   JsonDomSchema *field(  const std::string& fieldpath ) const;
   /// Get field by idspath
   JsonDomSchema *field( std::queue<int> ids ) const;
   /// Get field by fieldpath
   JsonDomSchema *field( std::queue<std::string> names ) const;

   // Update functions  -----------------------------------

   /// Reset Node to default values
   void clearField()
   {
      setDefault("", 0/*2*/ );
   }

   /// Set up string json value to array or struct field
   void setComplexValue(const std::string& newval )
   {
     setDefault( newval, 2 );
   }

   /// Add field to json with default values
   void addField()
   {
     _isSetUp = true;
     if( _parent != nullptr )
      _parent->addField();
   }

   /// Remove current field from json, set field to defValue
   /// return false if requred field
   bool removeField()
   {
     if( _parent->isMap() )
     {
       _parent->delMapKey( _fldKey  );
       return true;
     }
     else
     {
       if( _fldDef->fRequired == fld_required )
         return false;

       setDefault("", 0 );
       _isSetUp = false;
       return true;
     }
   }

   /// Top must be schema object
   virtual bool updateTypeTop( int /*newtype*/ )
   {
      return false;
   }

   std::vector<std::string>  getTemplateKeyList() const
   {
     return _strDef->getKeyTemplateList();
   }

   // Set _id to Node
   //void setOid_( const std::string& _oid  );

   // special work with arrays ---------------------

   /// get top level Array sizes
   std::vector<size_t> getArraySizes();

   /// Resize top level Array
   void resizeArray( const std::vector<std::size_t>& sizes, const std::string& defval  = "" );

   /// Reset top level Array
   void resetArray( const std::vector<std::string> vals );

   /// Set Array to Node (must be resized before )
   template <class T>
   void setArray( const std::vector<T>& value  )
   {
       if( !isArray() )
         jsonioErr("setArray" , _fldKey+" is not an array" );

       for( uint ii =0; ii<_children.size(); ii++)
       {
          if( _children[ii]->isArray() )
            _children[ii]->setArray( value[ii]  );
          else
            if( _children[ii]->isStruct() )
                ; // ??????
            else
              _children[ii]->setValue( value[ii] );
       }
   }

   /// Get Array from Node (must be resized before )
   template <class T>
   void getArray( std::vector<T>& value  )
   {
       if( !isArray() )
         jsonioErr("getArray" , _fldKey+" is not an array" );

       for( uint ii =0; ii<_children.size(); ii++)
       {
          if( _children[ii]->isArray() )
            ; // _children[ii]->getArray( value[ii]  ); DM 17.06.2016 - not compiling
          else
            if( _children[ii]->isStruct() )
                ; // ??????
            else
              _children[ii]->getValue( value[ii] );
       }
   }

  // special work with map  ------------------------

   /// Set Value to Map Key
   template <class T>
   void setMapKey( T& value  )
   {
       if( _parent->isMap() )
       {
         std::string def  =  TArray2Base::value2string( value );
         // convert string to internal type
         _fldKey = getDefValue( _fldDef->fTypeId[_parent->_level+1], def );
       }
   }

   /// Set Value to Map Key
   template <class T>
   void addMapKeyValue( T& key, const std::string& value  )
   {
       if( isMap() )
       {
           std::vector<std::size_t> sizes;
           sizes.push_back(_children.size()+1);
           resizeArray( sizes, value );
           _children.back()->setMapKey( key );
       }
   }

   /// Delete Key from map
   void delMapKey( const std::string& key  )
   {
       if( !isMap() )
        return;
       bool move = false;
       auto it = _children.begin();

       while( it != _children.end() )
       {
          if( move )
             it++->get()->_ndx--;
          else
            if( it->get()->_fldKey == key )
            {
                it =  _children.erase( it );
                move = true;
            }
            else
               it++;
        }
   }

   size_t getLevel() const
   {
     return   _level;
   }
};

/// Unpack json string data to JsonDomSchema
std::shared_ptr<JsonDomSchema> unpackJson( const std::string& jsondata, const std::string& schemaname );


} // namespace jsonio

#endif // JSON_NODE_H
