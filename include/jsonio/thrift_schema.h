//  This is JSONIO library+API (https://bitbucket.org/gems4/jsonio)
//
/// \file thrift_schema.h
/// Declarations of classes and functions to work with Thrift Schemas
//
// JSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef THRIFTSCHEMA_H
#define THRIFTSCHEMA_H

#include <memory>
#include "jsonio/jsondom.h"

namespace jsonio {

/** #include <thrift/protocol/TVirtualProtocol.h>
 * Enumerated definition of the types that the Thrift protocol supports.
 * Take special note of the T_END type which is used specifically to mark
 * the end of a sequence of fields.
 */
enum ThType {
  Th_STOP       = 0,
  Th_VOID       = 1,
  Th_BOOL       = 2,
  Th_BYTE       = 3,
  Th_I08        = 3,
  Th_I16        = 6,
  Th_I32        = 8,
  Th_U64        = 9,
  Th_I64        = 10,
  Th_DOUBLE     = 4,
  Th_STRING     = 11,
  Th_UTF7       = 11,
  Th_STRUCT     = 12,
  Th_MAP        = 13,
  Th_SET        = 14,
  Th_LIST       = 15,
  Th_UTF8       = 16,
  Th_UTF16      = 17
};


/// A requiredness attribute which defines how
/// Apache Thrift reads and writes the field
enum  REQURED_FIELD
{
     fld_required = 0,
     fld_default = 1,
     fld_optional = 2
};

class ThriftSchema;

/// Definition of one field in thrift structure
/// [id:] [requiredness] type FieldName [=default] [,|;]
struct ThriftFieldDef
{
    int fId;             ///< Key id
    std::string fName;        ///< Field Name
    std::vector<int> fTypeId; ///< Type or all "typeId"+"type"+"elemTypeId"+"elemTypeId"  - all levels
    int fRequired;       ///< A requiredness attribute (REQURED_FIELD)
    std::string  fDefault;    ///< Default value
    std::string  insertedDefault;    ///< Default value from editor
    std::string  className;   ///< "class": Struct or enum name for corresponding field type
    std::string  fDoc;        ///< "doc" - Comment string
    double minval;
    double maxval;
};

/// Thrift structs definition
class ThriftStructDef
{
    std::string name;      ///< Name of strucure
    bool isException; ///< True if exeption structure
    bool isUnion;     ///< True if union
    std::string  sDoc;     ///< Comment
//    vector<ThriftFieldDef> fields;   ///< List of fields

    std::map<int, uint> id2index;
    std::map<std::string, uint> name2index;

    std::vector<std::string>  keyIdSelect;
    std::vector<std::string>  keyTemplateSelect;
    std::vector<std::string>  uniqueList;

    void readSchema( const JsonDom* object );
    void read_type_spec( const JsonDom* object,
       ThriftFieldDef& value, const char* keyspec, const std::string& typeID);

public:

    std::vector<ThriftFieldDef> fields;   ///< List of fields
    static  ThriftFieldDef emptyfield;

    /// Constructor - read information from json/bson schema
    ThriftStructDef( const JsonDom* object )
    {
       readSchema( object );
    }

    /// Get name of structure
    const char* getName() const
    { return name.c_str();}

    /// Get description of structure
    const char* getComment() const
    { return sDoc.c_str();}

    /// Test structure is Union
    bool testUnion() const
    { return isUnion;}

    /// Get number of base select fields
    std::size_t getSelectSize() const
    { return keyIdSelect.size(); }


    /// Return short record key field name
    std::string keyFldIdName(uint i) const
    {
        if( i < keyIdSelect.size() )
          return keyIdSelect[i];
        else
          return "";
    }

    /// Get unique fields list
    const std::vector<std::string>& getUniqueList()
    {
        return uniqueList;
    }

    /// Get key template fields list
    const std::vector<std::string>& getKeyTemplateList()
    {
      return keyTemplateSelect;
    }


    /// Get field definition by index
    ThriftFieldDef& getSchema( int fId )
    {
        auto it = id2index.find( fId);
        if( it == id2index.end() )
         return emptyfield;
        else
         return fields[it->second];
    }

    /// Get field definition by name
    ThriftFieldDef& getSchema( const std::string& fName )
    {
        auto it = name2index.find( fName );
        if( it == name2index.end() )
         return emptyfield;
        else
         return fields[it->second];
    }


    /// Get field definition by name
    ThriftFieldDef* getFieldDef( const std::string& fName )
    {
        auto it = name2index.find( fName );
        if( it == name2index.end() )
         return nullptr;
        else
         return &fields[it->second];
    }

    /// Get field definition by index
    ThriftFieldDef* getFieldDef( int fId )
    {
        auto it = id2index.find( fId);
        if( it == id2index.end() )
         return nullptr;
        else
         return &fields[it->second];
    }

};

/// Thrift enums definition
class ThriftEnumDef
{
    std::string name;      ///< Name of enum
    std::string  sDoc;     ///< Comment
    std::map<std::string, int> name2index; ///< Members of enum
    std::map<std::string, std::string> name2doc; ///< Comments of enum

    void readEnum( const JsonDom* object );

public:

    static  int emptyenum;

    /// ThriftEnumDef - read information from json schema
    ThriftEnumDef( const JsonDom* object )
    {
       readEnum( object );
    }

    /// Get name of enum
    const char* getName()
    { return name.c_str();}

    /// Get description of structure
    const char* getComment()
    { return sDoc.c_str();}

    /// Get enum value by name
    int& getId( const std::string& lName )
    {
        auto it = name2index.find( lName );
        if( it == name2index.end() )
         return emptyenum;
        else
         return it->second;
    }

    /// Get enum comment by name
    std::string getDoc( const std::string& lName )
    {
        auto it = name2doc.find( lName );
        if( it == name2doc.end() )
         return "";
        else
         return it->second;
    }


    /// Get enum names list
    std::vector<std::string> getNamesList()
    {
      std::vector<std::string> members;
      auto it = name2index.begin();
      while( it != name2index.end() )
        members.push_back(it++->first);
      return members;
    }

    /// Get enum name by value
    std::string getNamebyId( int id )
    {
      auto it = name2index.begin();
      while( it != name2index.end() )
      {
         if( it->second == id )
          return it->first;
         it++;
      }
      return "";
    }

};

/// Thrift schema definition
class ThriftSchema
{
    std::map<std::string, std::string> files; ///< List of readed files (name, doc)

    std::map<std::string, std::unique_ptr<ThriftStructDef>> structs; ///< List of all structures
    std::map<std::string, std::unique_ptr<ThriftEnumDef>> enums;     ///< List of all enums

    /// Map of readed vertex schemas (_label, schema name)
    std::map<std::string, std::string> vertexes;
    /// Map of readed edge schemas  (_label, schema name)
    std::map<std::string, std::string> edges;
    /// Map of used vertex collections (schema name, collect_name)
    std::map<std::string, std::string> vertexCollections;
    /// Map of used edge collections  (schema name, collect_name)
    std::map<std::string, std::string> edgeCollections;
    /// String with all edge collections (Anonymous graphs traverse)
    /// Instead of GRAPH graphName you may specify a list of edge collections.
     std::vector<std::string> allEdgesTraverse;

    void readSchema( const JsonDom* object );
    void setTypeMap();

public:

static std::map<std::string, int> name_to_thrift_types;

    /// ThriftSchema - constructor
    ThriftSchema(  )
    {
       setTypeMap();
    }

    /// Read thrift schema from json file fileName
    void addSchemaFile( const char *fileName );


    /// Get description of structure
    ThriftStructDef* getStruct(const std::string& structName) const
    {
        auto it = structs.find( structName );
        if( it == structs.end() )
         return nullptr;
        else
         return it->second.get();
    }

    /// Get description of enum
    ThriftEnumDef* getEnum(const std::string& enumName) const
    {
        auto it = enums.find( enumName );
        if( it == enums.end() )
         return nullptr;
        else
         return it->second.get();
    }

    /// Get structs names list
    std::vector<std::string> getStructsList( bool withDoc ) const;


    /// Get enums names list
    std::vector<std::string> getEnumsList() const
    {
      std::vector<std::string> members;
      auto it = enums.begin();
      while( it != enums.end() )
        members.push_back(it++->first);
      return members;
    }

    /// Get vertex names list
    std::vector<std::string> getVertexesList() const
    {
      std::vector<std::string> members;
      auto it = vertexes.begin();
      while( it != vertexes.end() )
        members.push_back(it++->second);
      return members;
    }

    /// Get vertex schema name from label data
    std::string getVertexName(const std::string& _label) const
    {
        auto it = vertexes.find( _label );
        if( it == vertexes.end() )
         return "";
        else
         return it->second;
    }

    /// Get vertex collection from Schema name
    std::string getVertexCollection(const std::string& _name) const
    {
        auto it = vertexCollections.find( _name );
        if( it == vertexCollections.end() )
         return "";
        else
         return it->second;
    }

    /// Link Map of used vertex collections (schema name, collect_name)
    const std::map<std::string, std::string>& usedVertexCollections() const
    {
        return vertexCollections;
    }

    /// Get edge names list
    std::vector<std::string> getEdgesList() const
    {
      std::vector<std::string> members;
      auto it = edges.begin();
      while( it != edges.end() )
        members.push_back(it++->second);
      return members;
    }

    /// Get edge schema name from label data
    std::string getEdgeName(const std::string& _label) const
    {
        auto it = edges.find( _label );
        if( it == edges.end() )
         return "";
        else
         return it->second;
    }

    /// Get edge collection from Schema name
    std::string getEdgeCollection(const std::string& _name) const
    {
        auto it = edgeCollections.find( _name );
        if( it == edgeCollections.end() )
         return "";
        else
         return it->second;
    }

    /// Link Map of used edge collections  (schema name, collect_name)
    const std::map<std::string, std::string>& usedEdgeCollections() const
    {
        return edgeCollections;
    }

    /// Get list of edges defined from schemas
    const std::vector<std::string>& getEdgesAllDefined() const
    {
      return allEdgesTraverse;
      //"inherits, takes, defines, master, product, prodreac, basis, pulls, involves, adds, yields";
    }

    /// Clear all internal data
    void clearAll()
    {
      files.clear();
      structs.clear();
      enums.clear();
      vertexes.clear();
      edges.clear();
    }

    /// Find for schema <schemaname> all paths for field <fieldname>
    void findPathTo( const std::string& schemaname, const std::string& fieldname,
                     std::set<std::string>& paths, const std::string& fieldhead = "" ) const ;

};

} // namespace jsonio

#endif // THRIFTSCHEMA_H
