//  This is JSONIO library+API (https://bitbucket.org/gems4/jsonio)
//
/// \file nservice.h
/// Declarations of jsonio_exception class and service functions
//
// JSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef NSERVICE_H
#define NSERVICE_H

#include <sstream>
#include <string>
#include <vector>
#include <queue>
#include <map>
#include <set>
#include <cmath>
#include <limits>
//#include <stdnoreturn.h>


#ifndef  __unix

typedef unsigned int uint;

#endif //  __noborl

namespace jsonio {

using ValuesTable = std::vector< std::vector<std::string> >; ///< Lines of colums values table

void delColumn( ValuesTable& matr, int column );

void addColumn( ValuesTable& matr, int column, const std::string& fillvalue = "" );


class jsonio_exception: public std::exception
{

public:

    std::string mess_;
    std::string title_;
    std::string field_;  ///< Last readed field

    jsonio_exception()
    {}

    jsonio_exception(const std::string& titl, const std::string& msg, const std::string& fld):
            mess_(msg),  title_(titl), field_(fld)
    {}

   const char* what() const noexcept
   {
      return mess_.c_str();
   }

   virtual const char* title() const noexcept
   {
      return title_.c_str();
   }

   virtual const char* field() const noexcept;

};

/// Throw  jsonio_exception
[[ noreturn ]] inline void jsonioErr(const std::string& title, const std::string& message, const std::string& fld= "")
{
    throw jsonio_exception(title, message, fld);
}

/// Throw condition jsonio_exception
inline void jsonioErrIf(bool error, const std::string& title, const std::string& message, const std::string& fld = "")
{
    if(error) throw jsonio_exception(title, message, fld);
}

/// Whitespace characters have been stripped from the beginning and the end of the std::string.
   void strip(std::string& str);
/// All whitespace characters have been stripped from the beginning and the end of the std::string.
   void strip_all(std::string& str, const std::string& valof =  " \n\t\r"  );
/// The method replace() returns a copy of the std::string
/// in which the first occurrence of old have been replaced with new
   std::string replace( std::string str, const char* old_part, const char* new_part);
/// The method replace() returns a copy of the std::string
/// in which all occurrences of old have been replaced with new
   std::string replace_all( std::string str, const char* old_part, const char* new_part);
   void replaceall(std::string& str, char ch1, char ch2);
   void replaceall(std::string& str, const std::string& allReplased, char ch2);

   void convertReadedString(std::string& strvalue );
   void convertStringToWrite( std::string& strvalue );

   /// Split std::string to int array
   std::queue<int> string2intarray( const std::string& str_data, const std::string& delimiters );
   /// A split function
   std::queue<std::string> split(const std::string& str, const std::string& delimiters);


   /// Convert std::vector to std::string
   std::string string_from_vector(const std::vector<std::string> &pieces, const std::string& delimiter=" " );
   /// Extract coefficient from strings like +10.7H2O
   std::string extractCoef( const std::string& data, double& coef );

   /// Splits full pathname to path, directory, name and extension
   void u_splitpath(const std::string& Path, std::string& dir, std::string& name, std::string& ext);
   /// Combines path, directory, name and extension to full pathname
   std::string u_makepath(const std::string& dir, const std::string& name, const std::string& ext);

   inline int ROUND( double x )
   {
    return static_cast<int>(x>0?(x)+.5:(x)-0.4);
   }

   template<typename T>
   bool approximatelyEqual( const T& a, const T& b, const T& epsilon = std::numeric_limits<T>::epsilon() )
   {
       return fabs(a - b) <= ( (fabs(a) < fabs(b) ? fabs(b) : fabs(a)) * epsilon);
   }

   template<typename T>
   bool essentiallyEqual( const T& a, const T& b, const T& epsilon = std::numeric_limits<T>::epsilon() )
   {
       return fabs(a - b) <= ( (fabs(a) > fabs(b) ? fabs(b) : fabs(a)) * epsilon);
   }

   template<typename T>
   bool definitelyGreaterThan( const T& a, const T& b, const T& epsilon = std::numeric_limits<T>::epsilon() )
   {
       return (a - b) > ( (fabs(a) < fabs(b) ? fabs(b) : fabs(a)) * epsilon);
   }

   template<typename T>
   bool definitelyLessThan( const T& a, const T& b, const T& epsilon = std::numeric_limits<T>::epsilon() )
   {
       return (b - a) > ( (fabs(a) < fabs(b) ? fabs(b) : fabs(a)) * epsilon);
   }

   template <typename T>
   bool is( T& x, const std::string& s)
   {
     std::istringstream iss(s);
     //char c;
     return iss >> x && !iss.ignore();
  }

   /// Test function if testVector is subset of fullSet (to jsonio )
   template <typename T>
   bool isSubset(const std::vector<T>& testVector, const std::set<T>& fullSet)
   {
       for( auto val: testVector)
       {
         if( fullSet.find(val) == fullSet.end() )
             return false;
       }
       return true;
   }

   /// Get subset testVector is in fullSet
   template <typename T>
   std::vector<T> getSubset(const std::vector<T>& testVector, const std::set<T>& fullSet)
   {
       std::vector<T> subset;
       for( auto val: testVector)
       {
         if( fullSet.find(val) != fullSet.end() )
             subset.push_back(val);
       }
       return subset;
   }

#ifdef _MSC_VER
#else
   template<typename T, template <typename, typename = std::allocator<T>> class Container>
   std::string vectorToJson( const Container<T>& elems )
   {
     std::string genjson = "[ ";

     for( auto elm: elems )
          genjson += "\n "+std::to_string(elm) + ",";
     genjson.pop_back();
     genjson += "\n]";

     return genjson;
   }
#endif

   template< template <typename, typename = std::allocator<std::string>> class Container>
   std::string vectorToJson( const Container<std::string>& elems )
   {
     std::string genjson = "[ ";

     for( auto elm: elems )
          genjson +=  "\n \""+ elm + "\",";
     genjson.pop_back();
     genjson += "\n]";

     return genjson;
   }
   //template <> std::string vectorToJson( const std::vector<std::string>& elems );

   template <typename T>
   std::string mapToJson( const std::map<std::string,T>& elems )
   {
     std::string genjson = "{ ";

     for( auto row: elems )
         genjson += "\n \""+row.first+ "\" : " + std::to_string(row.second) + ",";
     genjson.pop_back();
     genjson += "\n}";

     return genjson;
   }
   template <>  std::string mapToJson( const std::map<std::string,std::string>& elems );

   /// Returns std::string representation of current date in dd/mm/yyyy format
   std::string curDate();

   /// Returns std::string representation of current time in HH:MM  format
   std::string curTime();

} // namespace jsonio

#endif // NSERVICE_H_H

