#ifndef JSON_FREE_H
#define JSON_FREE_H

#include <iostream>
#include <memory>
#include "jsonio/jsondom.h"

namespace jsonio {

/// \class JsonDomFree represents a json free format object.
class JsonDomFree: public JsonDom
{
    std::string _fldKey;        ///< Key of object
    std::string _fldValue;      ///< Value of object
    int    _fldType;            ///< Type of object


    int _ndx;                     ///< internal ndx for GUI
    JsonDomFree *_parent;                      ///< Parent object
    std::vector<std::shared_ptr<JsonDomFree>> _children; ///< Children objects for Object or Array


    explicit JsonDomFree( int atype = JSON_OBJECT ):
      _fldKey("top"),  _fldValue(""), _fldType(atype), _parent(nullptr)
    { }

    JsonDomFree( const std::string& akey, int atype, const std::string& avalue, JsonDomFree *aparent ):
      _fldKey(akey), _fldValue(avalue), _fldType(atype), _parent(aparent)
    {
       if(_parent)
       {  _ndx = static_cast<int>(_parent->_children.size());
          _parent->_children.push_back(std::shared_ptr<JsonDomFree>(this));
       }
    }

    virtual bool testKey( const std::string& akey )
    {
        for(uint ii=0; ii< _children.size(); ii++ )
          if( _children[ii]->getKey() == akey )
              return true;
        return false;
    }


    /// Set up string json value to array or struct field
    void setComplexValue(const std::string& newval );
    void resizeArray(uint level, const std::vector<std::size_t>& sizes, const std::string& defval  );

    virtual bool updateField( const std::string& newValue)
    {
        // convert string to internal type
        if( isArray() || isStruct()  )
            setComplexValue( newValue );
        else
           _fldValue = fixedDefValue( getType(), newValue );
        return true;
    }

    bool updateFieldPath( std::queue<std::string> names, const std::string& newValue);

public:

    // Allocate top Object
    static JsonDomFree *newObject()
    {
       return ( new JsonDomFree( JSON_OBJECT ));
    }

    // Allocate top Array
    static JsonDomFree *newArray()
    {
      return ( new JsonDomFree( JSON_ARRAY ));
    }

    virtual ~JsonDomFree()
    {
      //cout << "~JsonDomFree" << getKey();
    }

    virtual JsonDomFree *appendNode( const std::string& akey, int atype, const std::string& avalue )
    {
      if( testKey( akey ) )  // field with key already exist
        jsonioErr( "JsonDomFree001", "Field with key already exists.", akey);
      JsonDomFree* newNode = new  JsonDomFree( akey, atype, avalue, this );
      return newNode;
    }


    // Get methods  --------------------------

    virtual const std::string& getKey() const
    {
      return  _fldKey;
    }

    virtual int getType() const
    {
      return  _fldType;
    }

    virtual int getNdx() const
    {
       return _ndx;
    }

    virtual const std::string& getFieldValue() const
    {
      return  _fldValue;
    }

    virtual std::size_t getChildrenCount() const
    {
       return _children.size();
    }

    virtual JsonDomFree* getChild( std::size_t ndx ) const
    {
      if( ndx < getChildrenCount() )
        return  _children[ndx].get();
      return nullptr;
    }

    virtual JsonDomFree* getParent() const
    {
      return _parent;
    }

    std::vector<std::string> getUsedKeys() const;

    // Test methods  --------------------------------------------

     /// Test top Node
     virtual bool isTop() const
     { return( _parent==nullptr); }

    // Access functions  ------------------------

    /// Get field by fieldpath ("name1.name2.name3")
    virtual JsonDomFree *field(  const std::string& fieldpath ) const;
    /// Get field by fieldpath
    virtual JsonDomFree *field( std::queue<std::string> names ) const;

    // Update functions  -----------------------------------

    /// Resize top level Array
    void resizeArray( const std::vector<std::size_t>& sizes, const std::string& defval  = "" );
    void resizeArray( std::size_t  size, const std::string& defval );

    /// Reset Node to default values
    virtual void clearField()
    {
       _children.clear();
       _fldValue =  fixedDefValue( _fldType, "" );
    }

    /// Remove current field from json
    virtual bool removeField();


    virtual bool updateTypeTop( int newtype )
    {
      // could be changed type only top free object on read step
      if( /*!_parent &&*/  getChildrenCount()<1 &&
          ( newtype==JSON_ARRAY || newtype==JSON_OBJECT) &&
          ( _fldType==JSON_ARRAY || _fldType==JSON_OBJECT)    )
      {
          _fldType = newtype;
          return  true;
       }
       return false;
    }

    /// Update if exist or add field by path
    /// Added all internal levels as objects
    bool updateFieldPath( const std::string& fieldpath, const std::string& newValue);

    /// Update if exist or add field by path
    /// Added all internal levels as objects
    JsonDomFree *appendObjectFieldPath( const std::string& fieldpath);
    JsonDomFree *appendObjectFieldPath( std::queue<std::string> names );
};

/// Unpack json string data to JsonDom
std::shared_ptr<JsonDomFree> unpackJson( const std::string& jsondata );

} // namespace jsonio

#endif // JSON_FREE_H
