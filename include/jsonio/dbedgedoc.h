//  This is JSONIO library+API (https://bitbucket.org/gems4/jsonio)
//
/// \file dbedgedoc.h
/// Declarations of class TDBGraph - working with graph databases (OLTP)
/// Used  SchemaNode class - API for direct code access to internal
/// DOM based on our JSON schemas.
//
// JSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2018 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef TDBEDGE_H
#define TDBEDGE_H

#include "jsonio/dbvertexdoc.h"

namespace jsonio {


/// Definition of graph databases chain
class TDBEdgeDocument : public TDBVertexDocument
{
    void beforeRm( const std::string&  ) {}

    /// Type constructor
    TDBEdgeDocument( const std::string& schemaName, const TDataBase* dbconnect,
                     const std::string& coltype, const std::string& colname ):
        TDBVertexDocument( schemaName, dbconnect, coltype, colname )
    { }

public:

    static TDBEdgeDocument* newEdgeDocumentQuery( const TDataBase* dbconnect, const std::string& schemaName,
                                                      const DBQueryData& query= emptyQuery  );

    static TDBEdgeDocument* newEdgeDocument( const TDataBase* dbconnect, const std::string& schemaName );

    ///  Constructor collection&document
    TDBEdgeDocument( const std::string& aschemaName, const TDataBase* dbconnect );
    ///  Constructor document
    TDBEdgeDocument( const std::string& schemaName, TDBCollection* collection  );
    /// Constructor from bson data
    TDBEdgeDocument( TDBCollection* collection, const JsonDom *object );

    ///  Destructor
    virtual ~TDBEdgeDocument(){}


    /// Extract schema by id  ( no query )
    std::string  extractSchemaFromId( const std::string& id  )
    {
        return _schema->getEdgeName( extractLabelFromId( id ) );
    }

    // build functions

    /// Define new Edge document
    void setEdgeObject( const std::string& aschemaName, const std::string& outV,
                  const std::string& inV, const FieldSetMap& fldvalues );

    /// Creates a new edge document in the collection from the given fldvalues data.
    /// \param  fldvalues - data to save
    /// \param  testValues - If testValues is true, we compare the current data with the internally loaded values,
    /// and if all the values are the same, then we update the selected record instead of creating new ones.
    /// \return new key of document
    std::string CreateEdge( const std::string& aschemaName, const std::string& outV,
                            const std::string& inV, const FieldSetMap& fldvalues, bool testValues = false )
    {
        setEdgeObject( aschemaName, outV, inV, fldvalues );
        return CreateWithTestValues( testValues );
    }

protected:

    /// Build default json query std::string for collection
    DBQueryData makeDefaultQueryTemplate() const;

    /// Make Follow Outgoing Edges query - observed
    DBQueryData outEdgesQueryOld( const std::string& id ) const
    {
        return DBQueryData( std::string("{\"_type\": \"edge\", \"_from\": \"")+ id + "\" }", DBQueryData::qEdgesFrom);
    }
    /// Make Follow Outgoing Edges query - observed
    DBQueryData outEdgesQueryOld( const std::string& edgeLabel, const std::string& idVertex ) const
    {
        std::string queryJson = "{'_type': 'edge', '_label': '"
                + edgeLabel  + "', '_from': '"
                + idVertex + "' }";
        return DBQueryData( queryJson, DBQueryData::qEdgesFrom);
    }

    /// Make Follow Incoming Edges query - observed
    DBQueryData inEdgesQueryOld( const std::string& id ) const
    {
        return DBQueryData( std::string("{\"_type\": \"edge\", \"_to\": \"")+ id + "\" }", DBQueryData::qEdgesTo);
    }
    /// Make Follow Incoming Edges query - observed
    DBQueryData inEdgesQueryOld( const std::string& edgeLabel, const std::string& idVertex ) const
    {
        std::string queryJson = "{'_type': 'edge', '_label': '"
                + edgeLabel  + "', '_to': '"
                + idVertex + "' }";
        return DBQueryData( queryJson, DBQueryData::qEdgesTo);
    }

};

TDBEdgeDocument* documentAllEdges( const TDataBase* dbconnect );

} // namespace jsonio

#endif // TDBEDGE_H
