//  This is JSONIO library+API (https://bitbucket.org/gems4/jsonio)
//
/// \file dbdocument.h
/// Declarations of class TDBDocumentBase - base interface of abstract DB document
//
// JSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2017-2018 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef TDBDOCUMENT_H
#define TDBDOCUMENT_H

#include "jsonio/dbquerydef.h"
#include "jsonio/dbcollection.h"

namespace jsonio {

class TDataBase;

// some extern functions
int extractIntField( const std::string& key, const std::string& query );
std::string extractStringField( const std::string& key, const std::string& query );
void addFieldsToQueryAQL( DBQueryData& query, const FieldSetMap& fldvalues );


/// Interface for working with documents.
/// Documents are JSON objects. These objects can be nested (to any depth) and may contain lists.
/// Each document has a unique primary key which identifies it within its collection.
/// Furthermore, each document is uniquely identified by its document handle across all collections in the same database.
class TDBDocumentBase: public IsUnloadToJson
{
    friend class TDBCollection;

    /// Do it after read document from database
    virtual void afterLoad( const std::string&  ) {}
    /// Do it before remove document with key from collection.
    virtual void beforeRm( const std::string&  ) {}
    /// Do it after remove document with key from collection.
    virtual void afterRm( const std::string&  ) {}
    /// Do it before write document to database
    virtual void beforeSaveUpdate( const std::string&  ) {}
    /// Do it after write document to database
    virtual void afterSaveUpdate( const std::string&  ) {}


public:

    ///  Constructor when create/load collection
    TDBDocumentBase( const TDataBase* dbconnect, const std::string& coltype, const std::string& colname  );
    ///  Constructor used loaded collection
    TDBDocumentBase( TDBCollection* collection  );

    ///  Destructor
    virtual ~TDBDocumentBase()
    {
        _collection->removeDocument(this);
    }

    /// Constructor from configuration data
    TDBDocumentBase( TDBCollection* collection, const JsonDom *object );
    virtual void toJsonNode( JsonDom *object ) const;
    virtual void fromJsonNode( const JsonDom *object );

    /// Get type of document ( collection )
    std::string type() const
    {
        return _colltype;
    }

    /// Access to the database
    const TDataBase* database() const
    {
        return _collection->database();
    }

    /// Generate new _id or other pointer of location
    virtual std::string genOid( const std::string& _keytemplate  )
    {
        return _collection->genOid( _keytemplate );
    }
    /// Set _id to document
    virtual void setOid( const std::string&  newid ) = 0;

    /// Extract _id from current document
    virtual std::string getOid() const = 0;

    /// Test document before read
    virtual void testUpdateSchema( const std::string&  /*pkey*/ )
    { }

    /// Return curent document as json string
    virtual std::string GetJson( bool dense = false ) const = 0;
    /// Load document from json string
    virtual void SetJson( const std::string& sjson ) = 0;

    /// Extract key from current document
    virtual std::string getKeyFromCurrent() const = 0;
    /// Get document key from dom object
    virtual std::string getKeyFromDom( const JsonDom *domobj  ) const
    {
        return _collection->getKeyFromDom(domobj);
    }
    /// Load document from json string
    /// \return current document key
    virtual std::string recFromJson( const std::string& jsondata ) = 0;


    //--- Manipulation records

    /// Checks whether a document exists.
    bool Find( const std::string& key )
    {
        return _collection->Find( key );
    }

    /// Creates a new document in the collection from the given data.
    /// \param key      - key of document
    /// \return new key of document ( generate from template if key undefined )
    std::string Create( const std::string& key = "" )
    {
        // add key _id to structure
        setOid( key );
        beforeSaveUpdate( key );
        auto rid = _collection->Create( this );
        afterSaveUpdate( key );
        return rid;
    }

    /// Retrive one document from the collection
    ///  \param key      - key of document
    void Read( const std::string& key )
    {
        testUpdateSchema( key );
        _collection->Read( this, key );
        afterLoad( key );
    }

    /// Updates an existing document or creates a new document described by the key.
    ///  \param key      - key of document
    std::string Update( const std::string& key  )
    {
        // add key _id to structure
        setOid( key );
        beforeSaveUpdate( key);
        auto rid = _collection->Update( this, key );
        afterSaveUpdate( key);
        return rid;
    }

    /// Removes document from the collection
    /// \param key      - key of document
    void Delete( const std::string& key )
    {
        beforeRm( key );
        _collection->Delete(key);
        afterRm( key );
    }

    //--- Selection/query functions

    virtual FieldSetMap extractFields( const std::vector<std::string> queryFields, const JsonDom* domobj ) const =  0;
    virtual FieldSetMap extractFields( const std::vector<std::string> queryFields, const std::string& jsondata ) const = 0;

    /// Execute query
    /// Fetches all documents from a collection that match the specified condition.
    ///  \param query -    selection condition
    ///  \param setfnc -   callback function fetching document data
    void runQuery( const DBQueryData&  query, SetReadedFunction setfnc ) const
    {
        // fields deprecated (EJDB)
        _collection->selectQuery( query, setfnc );
    }

    /// Execute query
    /// Fetches all documents from a collection that match the specified condition.
    ///  \param query -    selection condition
    ///  \return list of json strings with query result
    std::vector<std::string> runQuery( const DBQueryData&  query ) const;

    /// Looks up the documents in the specified collection using the array of keys provided.
    ///  \param rkeys -      array of keys
    ///  \param setfnc -   callback function fetching document data
    void runByKeys( const std::vector<std::string>& rkeys, SetReadedFunction setfnc ) const
    {
        _collection->lookupByDocumentKeys( rkeys, setfnc );
    }

    /// Looks up the documents in the specified collection using the array of keys provided.
    ///  \param rkeys -      array of keys
    ///  \return list of json strings with query result
    std::vector<std::string> runByKeys( const std::vector<std::string>& rkeys ) const;

    /// Test existence documents by query
    ///  \param query -    selection condition
    bool existKeysByQuery( DBQueryData& query ) const;

    /// Fetches all documents keys (_id) from a collection that match the specified condition.
    ///  \param query -    selection condition
    std::vector<std::string> getKeysByQuery( DBQueryData& query ) const;

    /// Build table of fields values by query
    ///  \param query - query condition
    ///  \param  queryFields - list of fields (columns) in result table
    ///  \return table of field values
    ValuesTable downloadDocuments( const DBQueryData&  query, const std::vector<std::string>& queryFields ) const;

    /// Build table of fields values by their keys
    ///  \param rkeys - list of top level record keys
    ///  \param  queryFields - list of fields (columns) in result table
    ///  \return table of field values
    ValuesTable downloadDocuments( const std::vector<std::string>& keys, const std::vector<std::string>& queryFields ) const;

    ///  Provides 'distinct' operation over collection
    ///  \param fpath Field path to collect distinct values from.
    ///  \return Unique values by specified fpath and collection
    std::vector<std::string> fieldValues( const std::string& fpath ) const
    {
        std::vector<std::string> values;
        _collection->fpathCollect( fpath, values );
        return values;
    }

    /// Removes all documents from the collection whose keys are contained in the keys array.
    ///  \param rkeys - list of top level record keys
    ///  Possible do not remove edges for vertices
    void removeByKeys( const std::vector<std::string>& keys  )
    {
        _collection->removeByDocumentKeys( keys);
    }

    //--- Addition complex functions

    /// Creates a new document in the collection from the given data.
    /// \param  testValues - If testValues is true, we compare the current data with the internally loaded values,
    /// and if all the values are the same, then we update the selected record instead of creating new ones.
    /// \return new key of document ( generate from template if key undefined into the given data )
    std::string CreateWithTestValues( bool testValues = false );

    /// Updates an existing document or creates a new document described by the given data (key must present in data).
    /// \param  testValues - If testValues is true, we compare the current data with the internally loaded values,
    /// and if all the values are the same, then we update the selected record instead of creating new ones.
    void UpdateWithTestValues(  bool testValues = false );

    /// Creates a new document in the collection from the given json data.
    /// \param jsondata - data to save
    /// \param  testValues - If testValues is true, we compare the current data with the internally loaded values,
    /// and if all the values are the same, then we update the selected record instead of creating new ones.
    /// \return new key of document ( generate from template if key undefined )
    std::string CreateFromJson(  const std::string& jsondata, bool testValues = false )
    {
        recFromJson( jsondata );
        return CreateWithTestValues( testValues );
    }

    /// Updates an existing document.
    /// \param  jsondata - data to save ( must contain the key )
    /// \param  testValues - If testValues is true, we compare the current data with the internally loaded values,
    /// and if all the values are the same, then we update the selected record instead of creating new ones.
    void UpdateFromJson( const std::string& jsondata, bool testValues = false )
    {
        recFromJson( jsondata );
        UpdateWithTestValues( testValues );
    }

    /*
     *  Most of this functions used in import part.
     *  If  testValues is false all this function work as usual CRUD with addition setting data.
     *  If testValues is true, we compare the current data with the internally loaded values,
     *    and if all the values are the same, then we update the selected record instead of creating new ones.
     *
     *   All this functions could be call with  testValues == true only if executed before SetQuery, otherwise will be exception.
    */

    // internal selection part

    /// Set&execute query for document
    virtual void SetQuery( DBQueryData query, std::vector<std::string>  fieldsList = {} );

    /// Set&execute query for document
    virtual void SetQuery( const DBQueryDef& querydef );

    /// Run current query, rebuild internal table of values
    virtual void updateQuery() = 0;

    const DBQueryResult* lastQueryData() const
    {
      jsonioErrIf( _queryResult.get() == nullptr, "lastQueryData", "Could be execute only into selection mode.");
      return _queryResult.get();
    }

protected:

    /// Documents are grouped into collection
    TDBCollection* _collection;

    /// Internal type of collection ( "undef", "schema", "vertex", "edge" )
    /// There are two types of collections: document collection as well as edge collections.
    std::string _colltype = "undef";

    /// Prepare data to save to database
    virtual JsonDom* recToSave( time_t crtt, char* oid ) = 0;

    // internal list documents using

    /// Last query result if exists
    std::shared_ptr<DBQueryResult>  _queryResult;

    /// Add line to view table
    void addLine( const std::string& keyStr, const JsonDom* nodedata, bool isupdate )
    {
        if(_queryResult.get() != nullptr )
            _queryResult->addLine( keyStr, nodedata, isupdate );
    }

    /// Update line into view table
    void updateLine( const std::string& keyStr, const JsonDom* nodedata )
    {
        if(_queryResult.get() != nullptr )
            _queryResult->updateLine( keyStr, nodedata );
    }

    /// Delete line from view table
    void deleteLine( const std::string& keyStr )
    {
        if(_queryResult.get() != nullptr )
            _queryResult->deleteLine( keyStr );
    }

    /// Build default query for collection ( by default all documents )
    virtual DBQueryData makeDefaultQueryTemplate() const
    {
      return DBQueryData(jsonio::DBQueryData::qAll);
    }

    /// Build default query fields ( by default internal )
    virtual std::vector<std::string>  makeDefaultQueryFields() const
    {
       std::vector<std::string> keyFields = { "_id", "_key","_rev"};
       return keyFields;
    }

    /// Find key from current data
    /// Compare to data into query table
    virtual std::string getKeyFromValueNode() const = 0;

};

} // namespace jsonio

#endif // TDBDOCUMENT_H
