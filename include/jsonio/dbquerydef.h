//  This is JSONIO library+API (https://bitbucket.org/gems4/jsonio)
//
/// \file dbquerydef.h
/// Declaration of QueryLine, DBQueryDef and DBQueryResult
/// classes to work with data base queries
//
// JSONIO is a C++ library and API aimed at implementing the interfaces
// for exchanging the structured data between NoSQL database backends,
// JSON/YAML/XML files, and client-server RPC (remote procedure calls).
//
// Copyright (c) 2015-2016 Svetlana Dmytriieva (svd@ciklum.com) and
//   Dmitrii Kulik (dmitrii.kulik@psi.ch)
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU (Lesser) General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// JSONIO depends on the following open-source software products:
// Apache Thrift (https://thrift.apache.org); Pugixml (http://pugixml.org);
// YAML-CPP (https://github.com/jbeder/yaml-cpp); EJDB (http://ejdb.org).
//

#ifndef DBQUERYDEF_H
#define DBQUERYDEF_H

#include <functional>
#include "jsonio/thrift_schema.h"
#include "jsonio/jsondom.h"

namespace jsonio {

/// Map of query fields  <json name>-><field into json record>
/// Used to generate return values for AQL
using QueryFields = std::map<std::string, std::string >;
/// Table of results <key>-><extracted values>
using TableOfValues = std::map<std::string, std::vector<std::string> >;
using  CompareTemplateFunction = std::function<bool( const std::string& keypat, const std::string& akey )>;
using FieldSetMap = std::map<std::string, std::string >;   ///< map fieldpath<->value

/// \class DBQuery description query with type
class DBQueryData
{
  ///  Query style (QueryType)
  int    _type;
  /// String with query ( AQL or json template  )
  std::string _findCondition;
  /// The bind parameter values need to be passed along
  /// with the AQL query when it is executed ( json object )
  std::string _bindVars;
  /// Options: key/value object with extra options for the query.
  /// The options need to be passed along with the query when it is executed.
  std::string query_options;
  /// List of fields, if extract part of record in AQL
  QueryFields _queryFields;


public:

  enum QueryType
  { qUndef = -1, qTemplate = 0 /* most of old queries*/,
    qAll = 1,
    qEdgesFrom = 2, qEdgesTo = 3, qEdgesAll = 4,
    qAQL = 6, qEJDB = 7 };

  /// Undef or All  constructor
  DBQueryData( int atype = qAll  ):
   _type(atype), _findCondition("")
  { }

  /// String constructor
  DBQueryData( const std::string& condition, int atype ):
      _type(atype), _findCondition(condition)
  {
    if( _findCondition.empty() )
       _type = qAll;
    else
       if( _type == qTemplate )
         _findCondition = replace_all( _findCondition, "\'", "\"");
  }

  // All constructor
  DBQueryData( const DBQueryData& data):
   _type(data._type), _findCondition(data._findCondition)
  { }

  void toJsonNode( JsonDom *object ) const;
  void fromJsonNode( const JsonDom *object );

  bool empty() const
  {
    return _type == qUndef;
  }
  int getType() const
  {
    return _type;
  }

  const std::string& getQueryString() const
   {
         return _findCondition;
   }

  void  setBindVars( const std::string& jsonBindObject )
  {
         _bindVars = jsonBindObject;
  }
  void  setBindVars( const JsonDom *bindObject )
  {
        setBindVars( bindObject->toString() ) ;
  }

  const std::string& getBindVars() const
  {
         return _bindVars;
  }

  /// Set json string with the  key/value object with extra options for the query
  ///  need to be passed along with the query when it is executed.
  void  setOptions( const std::string& jsonOptionsObject )
  {
      query_options = jsonOptionsObject;
  }

  /// Get the json string with  key/value object with extra options for the query.
  const std::string& options() const
  {
      return query_options;
  }

  void  setQueryFields( const QueryFields& mapFields )
  {
        _queryFields = mapFields;
  }

  template <typename Container>
  void  setQueryFields( const Container& listFields )
  {
      _queryFields.clear();

      typename Container::const_iterator itr = listFields.begin();
      typename Container::const_iterator end = listFields.end();
      for (; itr != end; ++itr)
      {
          std::string fld = *itr;
          replaceall( fld, '.', '_');
          _queryFields[fld] = *itr;
      }

  }

  const QueryFields& getQueryFields() const
  {
         return _queryFields;
  }

  /// Generate AQL RETURN with predefined fields
  static std::string generateReturn( bool isDistinct, const QueryFields& mapFields, const std::string& collvalue = "u" );
  /// Generate AQL RETURN with predefined fields
  std::string generateReturn(const std::string& collvalue = "u" ) const
  {
     return generateReturn( false, _queryFields, collvalue );
  }
  /// Generate AQL FILTER with predefined fields
  static std::string generateFILTER(  const jsonio::FieldSetMap& fldvalues, bool asTemplate = false,
                               const std::string& collvalue = "u" );

  friend bool operator!=( const DBQueryData&,  const DBQueryData& );
};

extern DBQueryData emptyQuery;

/// \class DBQueryDef description query into Database
class DBQueryDef
{

  std::string keyName;          ///< Short name of query (used as key field)
  std::string comment;          ///< Description of query
  std::string toschema;         ///< Schema for query about
  //int    style;            ///<  Query style (QueryType)
  //std::string findCondition;          ///< String with query in json format
  std::vector<std::string> fieldsCollect;  ///< List of fieldpaths to collect
  DBQueryData condition;

public:


  /// New constructor
  DBQueryDef( const std::vector<std::string>& fields, const DBQueryData& aqury ):
    fieldsCollect(fields), condition(aqury)
  { }

  /// New constructor
  DBQueryDef( const JsonDom *object )
  {
       fromJsonNode(object );
  }


  // old constructor
  //DBQueryDef( const std::string& qschema, const std::vector<std::string>& fields ):
  // toschema(qschema), fieldsCollect(fields), condition(emptyQuery)
  //{ }

  void toJsonNode( JsonDom *object ) const;
  void fromJsonNode( const JsonDom *object );

  void setKeyName( const std::string& name )
    { keyName = name;  }
  const std::string& getKeyName() const
    { return keyName; }

  void setComment( const std::string& comm )
    { comment = comm;  }
  const std::string& getComment() const
    { return comment; }

  void setToSchema( const std::string& shname )
    { toschema = shname;  }
  const std::string& getToSchema() const
    { return toschema; }

  void setQueryCondition( const DBQueryData& query )
  {
    condition = query;
  }
  const DBQueryData& getQueryCondition() const
   {
      return condition;
   }

  void setFieldsCollect(const std::vector<std::string>& collect )
    { fieldsCollect = collect;  }
  void addFieldsCollect(const std::vector<std::string>& collect )
    { fieldsCollect.insert(fieldsCollect.end(), collect.begin(), collect.end()); }
  const std::vector<std::string>& getFieldsCollect() const
    { return fieldsCollect; }

};

/// \class  DBQueryResult used to store query definition and result
class DBQueryResult
{

  DBQueryDef query;
  TableOfValues  queryValues;    ///< Table of values were gotten from query

  /// Make line to view table
  void nodeToValues(  const JsonDom* node, std::vector<std::string>& values ) const;

 public:

  DBQueryResult( const DBQueryDef& aquery ):
      query(aquery)
  { }
  ~DBQueryResult() {}


  const DBQueryDef& getQuery() const
  {
     return query;
  }

  void setQuery( const DBQueryDef& qrdef)
  {
     query = qrdef;
  }

  void setQueryCondition( const DBQueryData& jsonquery)
  {
     query.setQueryCondition(jsonquery);
  }

  void setFieldsCollect(const std::vector<std::string>& collect )
  {
     query.setFieldsCollect(collect);
  }

  /// Define schema for query about
  void setToSchema(const std::string& schemaname )
  {
     query.setToSchema(schemaname);
  }

  /// Add line to view table
  void addLine( const std::string& keyStr,  const JsonDom* nodedata, bool isupdate );
  /// Update line into view table
  void updateLine( const std::string& keyStr,  const JsonDom* nodedata );
  /// Delete line from view table
  void deleteLine( const std::string& keyStr );

  /// Get query result table
  const TableOfValues& getQueryResult() const
  {
      return queryValues;
  }

  /// Reset data when executing query
  void clearResults()
  {
     queryValues.clear();
  }


  ///  Get all keys list for current query
  std::size_t getKeyValueList( std::vector<std::string>& aKeyList,
         std::vector<std::vector<std::string>>& aValList ) const;

  /// Get keys list for current query and defined condition function
  std::size_t getKeyValueList( std::vector<std::string>& aKeyList, std::vector<std::vector<std::string>>& aValList,
                       const char* keypart, CompareTemplateFunction compareTemplate ) const;

  /// Get keys list for current query and defined field values
  std::size_t getKeyValueList( std::vector<std::string>& aKeyList, std::vector<std::vector<std::string>>& aValList,
                       const std::vector<std::string>& fieldnames, const std::vector<std::string>& fieldvalues ) const;

  /// Extract key from data
  std::string getKeyFromValue( const JsonDom* node ) const;

  /// Extract first key from data
  std::string getFirstKeyFromList() const;

};

} // namespace jsonio


#endif // DBQUERYDEF_H
