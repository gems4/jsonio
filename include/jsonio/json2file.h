#ifndef JSON2FILE_H
#define JSON2FILE_H

#include "jsonio/jsondomfree.h"

namespace jsonio {

typedef std::ios::openmode FileStatus;

enum FileTypes {
 Undef_ = 'u', Txt_ = 't', Json_ = 'j', Yaml_ = 'y',
 XML_ = 'x', EJDB_ = 'e', Binary_ = 'b', GDS_ = 'g' // GemDataStream
};

/// This enum is used to describe the mode in which a device is opened
enum OpenModeTypes {
  NotOpen,       ///<	The device is not open.
  ReadOnly,      ///<	The device is open for reading.
  WriteOnly, 	 ///<	The device is open for writing.
  ReadWrite,     ///<	ReadOnly | WriteOnly	The device is open for reading and writing.
  Append	     ///<	The device is opened in append mode so that all data is written to the end of the file.
};

/// Base class for file manipulation
class TFileBase
{

protected:

    std::string keywd_;
    std::string dir_;
    std::string name_;
    std::string ext_;
    std::string path_;

    int type_ = FileTypes::Undef_;
    int  mode_ = OpenModeTypes::ReadWrite;
    bool  isopened_ = false;    ///<  true, if file is opened

    virtual void makeKeyword();
    void makePath()
    { path_ = u_makepath( dir_, name_, ext_ ); }

public:

    /// Constructor
    //TFileBase();
    /// Constructor
    TFileBase(const std::string& fName, const std::string& fExt, const std::string& fDir="" );
    /// Constructor from path
    TFileBase(const std::string& path);
    /// Constructor from bson data
    TFileBase( const JsonDom* object );
    virtual ~TFileBase()
    {
      //if( isopened_ )
      //  Close();
    }

    virtual void toJsonDom( JsonDom* object ) const;
    virtual void fromJsonDom( const JsonDom* object );

    //--- Selectors

    const char* getKeywd() const
    { return keywd_.c_str();  }
    bool testOpen() const
    { return isopened_; }

    const char*  Name() const
    { return name_.c_str();  }
    const char*  Ext() const
    { return ext_.c_str();    }
    const char*  Dir() const
    { return dir_.c_str();    }
    const char*  Path() const
    { return path_.c_str();  }
    int Type() const
    { return type_;  }

    //--- Manipulation

    bool Exist();

    void reOpen( const std::string& path  )
    {
      if( !path.empty() && path != path_ )
      {
        Close();
        path_ = path;
        u_splitpath(path_, dir_, name_, ext_);
        makeKeyword();
      }
      Open(mode_);
    }

    void reOpen( const std::string& fName, const std::string& fExt )
    {
      if( ( !fName.empty() && fName !=name_ ) || (!fExt.empty()  && fExt !=ext_ ) )
      {
        Close();
        name_ = fName;
        ext_ = fExt;
        makePath();
        makeKeyword();
      }
      Open(mode_);
    }

    virtual void Close() { isopened_ = false; }
    virtual void Open(int amode ) { mode_ = amode; isopened_ = true; }
};

/// Class for read/write json files
class FJson : public TFileBase
{

protected:

   /// Load data from json file to dom object
   void loadJson( JsonDom* object );

   /// Save data from dom object to  json file
   void saveJson( const JsonDom* object ) const ;

public:

   /// Constructor
    FJson( const std::string& fName, const std::string& fExt, const std::string& fDir=""):
       TFileBase( fName, fExt, fDir )
    {
        type_ =  FileTypes::Json_;
    }
    /// Constructor from path
    FJson( const std::string& path): TFileBase( path )
    {
        type_ =  FileTypes::Json_;
    }
    /// Constructor from dom data
    FJson( const JsonDom* object ):TFileBase( object ) {}
    /// Destructor
    ~FJson() { }

    /// Load data from file to dom object
    virtual void LoadJson( JsonDom* object );

    /// Save data from bson object to file
    virtual void SaveJson( const JsonDom* object ) const;

    /// Convert readed data to json string
    virtual std::string LoadtoJsonString();

};

/// Class for read/write json arrays files
class FJsonArray : public TFileBase
{
protected:

   std::size_t loadedndx_ = 0;   ///< Current loaded
   std::shared_ptr<JsonDomFree> arrObjects;

public:

    /// Constructor
    FJsonArray( const std::string& fName, const std::string& fExt, const std::string& fDir=""):
       TFileBase( fName, fExt, fDir )
    {
        type_ =  FileTypes::Json_;
    }
    /// Constructor from path
    FJsonArray( const std::string& path): TFileBase( path )
    {
        type_ =  FileTypes::Json_;
    }
    /// Constructor from bson data
    FJsonArray( const JsonDom* object ):TFileBase( object ) {}
    /// Destructor
    ~FJsonArray()
    {
        if( testOpen() )
            Close();
    }

    /// Load next object data from file to dom object
    JsonDom* LoadNext();

    /// Save next dom object to file
    void SaveNext( const JsonDom*  object );

    /// Load next object data from file to json string
    bool LoadNext( std::string& strjson );

    /// Save next json string to file
    void SaveNext( const std::string& strjson );

    virtual void Close();
    virtual void Open(int amode );
};

/// Class for read/write configuration arrays
template <class T>
class ConfigArray
{

    /// Internal Data File
    std::shared_ptr<FJson> _file;

    /// Save configuration data to file
    void array2Json( JsonDom* object , const std::vector<T>& arr )
    {
        int ii=0;
        for(auto itr = arr.begin(); itr != arr.end(); ++itr, ++ii )
        {
            //IsUnloadToBson* itt = dynamic_cast<IsUnloadToBson>(itr);
            //if(itt)
            {
                JsonDom* childobj =  object->appendObject( std::to_string(ii) );
                /*itt*/ itr->toJsonNode(childobj);
            }
        }
    }

    /// Load configuration data to array
    void json2Array( const JsonDom* object, std::vector<T>& arr )
    {
        for( std::size_t ii=0; ii<object->getChildrenCount(); ii++ )
        {
            auto childobj = object->getChild( ii);
            jsonioErrIf( childobj->getType() != JSON_OBJECT, "bson2Array", "Error reading cfg file..." );
            arr.push_back( T(childobj) );
        }
    }

    /// Save configuration data to file
    void array2Json( JsonDom* object, const std::vector<std::unique_ptr<T>>& arr )
    {
        int ii=0;
        for(auto itr = arr.begin(); itr != arr.end(); ++itr, ++ii )
        {
            //IsUnloadToBson* itt = dynamic_cast<IsUnloadToBson>(itr);
            //if(itt)
            {
                JsonDom* childobj =  object->appendObject( std::to_string(ii) );
                /*itt*/  itr->get()->toJsonNode(childobj);
            }
        }
    }

    /// Load configuration data to array
    void json2Array( const JsonDom* object,std:: vector<std::unique_ptr<T>>& arr )
    {
        for( std::size_t ii=0; ii<object->getChildrenCount(); ii++ )
        {
            auto childobj = object->getChild( ii);
            jsonioErrIf( childobj->getType() != JSON_OBJECT, "json2Array", "Error reading cfg file..." );
            arr.push_back( std::unique_ptr<T>(new T(childobj)) );
        }
    }

 public:

    ConfigArray( const std::string& fname ):
        _file( new FJson(fname) )
    {}

    ///  Constructor - reading configurator from json/yaml/xml file
    ConfigArray( std::shared_ptr<FJson> otherfile ):
        _file( otherfile )
    {}
    virtual ~ConfigArray() {}

    /// Save configuration data to file
    void saveArray2Cfg( const std::vector<T>& arr )
    {
        std::shared_ptr<JsonDomFree> node(JsonDomFree::newArray());
        array2Json( node.get(),  arr );
        _file->SaveJson( node.get() );
    }

    /// Load configuration data to array
    void loadCfg2Array( std::vector<T>& arr )
    {
        std::shared_ptr<JsonDomFree> node(JsonDomFree::newArray());
        _file->LoadJson( node.get() );
        json2Array( node.get(), arr );
    }

    /// Save configuration data to file
    void saveArray2Cfg( const std::vector<std::unique_ptr<T>>& arr )
    {
        std::shared_ptr<JsonDomFree> node(JsonDomFree::newArray());
        array2Json( node.get(),  arr );
        _file->SaveJson( node.get() );
    }

    /// Load configuration data to array
    void loadCfg2Array( std::vector<std::unique_ptr<T>>& arr )
    {
        std::shared_ptr<JsonDomFree> node(JsonDomFree::newArray());
        _file->LoadJson( node.get() );
        json2Array( node.get(), arr );
    }

};

} // namespace jsonio


#endif // JSON2FILE_H
