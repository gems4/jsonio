JSONIO
=========

JSONIO is a library and API providing a generic interface for exchanging the structured data between JSON files and ArangoDB multi-model database back-ends. 
The data munging is based on JSON schemas connected with the internal JSONDOM object. A JSON schema can be easily generated from a Thrift data structure definition (.thrift file) 
for a given structured data type.   

## What JSONIO does? ##

* JSONDOM is an internal structure for JSON documents. JSONDOM is used for processing JSON files and for storing JSON data in NoSQL database such as ArangoDB. 
* JSONIO implements the JSONDOM rendering to/from file or text stream into ArangoDB and/or JSON format. Public API methods allow the following conversions:
    - JSONDOM to/from JSON
* JSONIO can also be used for connecting any user-defined C++ data structures to a GUI editor widget and graphics (using the JSONUI widget API).
* JSONIO is written in C/C++ using open-source library Velocypack from ArangoDB.
* Version: currently 0.1.
* Will be distributed as is (no liability) under the terms of Lesser GPL v.3 license. 

## How to get JSONIO source code ##

* In your home directory, make a folder named e.g. ~/jsonio.
* cd ~/jsonio and clone this repository from https://bitbucket.org/gems4/jsonio.git  using a preinstalled free git client SourceTree or SmartGit (the best way on Windows). 
* Alternatively on Mac OS X or linux, open a terminal and type in the command line (do not forget a period):

```
git clone https://bitbucket.org/gems4/jsonio.git . 

```

## How to install the JSONIO library ##

 Building JSONIO requires g++, [CMake](http://www.cmake.org/), Velocypack.  

* Make sure you have g++, cmake and git installed. If not, install them. 

  On Ubuntu linux terminal, this can be done using the following commands:

```
#!bash
sudo apt-get install g++ cmake git libssl-dev libtool byacc flex
```

  For Mac OSX, make sure you have Homebrew installed (see [Homebrew web site](http://brew.sh) and [Homebrew on Mac OSX El Capitan](http://digitizor.com/install-homebrew-osx-el-capitan/) ).


* Install Dependencies

In order to build the JSONIO library on (k)ubuntu linux 16.04/18.04 or MacOS, first execute the following: 

```
#!bash
cd ~/jsonio
sudo ./install-dependencies.sh
```

* Install the JSONIO library

Then navigate to the directory where this README.md file is located and type in terminal:

```
cd ~/jsonio
sudo ./install.sh
```

* After that, headers, library  and the third-party libraries can be found in /usr/local/{include,lib}.  

### Install current version of ArangoDB server locally

On (K)Ubuntu linux, install the current version of ArangoDB server locally [from here](https://www.arangodb.com/download-major/ubuntu/):

~~~
curl -OL https://download.arangodb.com/arangodb34/DEBIAN/Release.key
sudo apt-key add - < Release.key
echo 'deb https://download.arangodb.com/arangodb34/DEBIAN/ /' | sudo tee /etc/apt/sources.list.d/arangodb.list
sudo apt-get install apt-transport-https
sudo apt-get update
sudo apt-get install arangodb3=3.4.4-1
~~~

The updates will come together with other ubuntu packages that you have installed.

On MacOS Sierra or higher, [navigate here](https://docs.arangodb.com/3.4/Manual/Installation/MacOSX.html) and follow the instructions on how to install ArangoDB with homebrew. Basically, you have to open a terminal and run two commands:

~~~
brew update
brew install arangodb
~~~

### How to use JSONIO (use cases) ###

#### [Doxygen](http://www.stack.nl/~dimitri/doxygen/index.html) documentation #####

Install doxygen

~~~
sudo apt install doxygen
sudo apt install graphviz
~~~

Generate html help

~~~
doxygen  jsonio-doxygen.conf
~~~

### How to generate JSON schemas using thrift ###

Use the following command:

~~~
thrift -r --gen json schema.thrift
~~~

The option -r (recursively) is necessary for generating json files from potential includes in the schema.thrift file. 

* TBD